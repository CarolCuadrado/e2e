const globalProperties = {
    httpUsername: "megarush",
    httpPassword: "megarush_gig",
    megalottoMasterURL: "https://master-megarush-stg.cpa.gigmagic.io/",
    localURL: "https://master-megarush-stg.cpa.gigmagic.io/",
    megalottoEnvironmentURLPrefix: "https://",
    megalottoEnvironmentURLSuffix: "-megarush-stg.cpa.gigmagic.io/",
    viewProfileURL: "/profile/view",
    isUserLoggedInAPIURL: "/isLoggedIn",
    loginAPIURL: '/loginV2',
    signUpAPIURL: '/quickSignUpV2',
    resetPasswordAPIURL: '/resetPassword',
    changePasswordAPIURL: '/changePassword',
    logOutUserAPIURL: '/logout',
    gamingAccountsAPIURL: '/getGamingAccounts',
    createBetAPIURL: '/createBet',
    getOpenDrawsAPIURL:'/getOpenDraws',
    resetPasswordURL: '/account/password-reset',
    userPreferencesURL: '/setPreferences',
    editLuckyNumberURL: '/profile/lucky-number',
    editProfileURL: '/profile/edit',
    expiredForgottenPasswordToken: '/0d0bef5a-d482-4e57-b8be-8638b59cd732',
    xApiKey: '5c7e3e9c1c62609cee83f960',
    magicAPIURL: 'https://megalotto-stg-api.gigmagic.io/igc/megalotto',
    walletAPIURL: 'https://test-api.paymentiq.io/paymentiq/api',
    paymentMethodWalletAPIURL: '/user/payment/method/**',
    cardDepositWalletAPIURL: '/creditcard/deposit/process**',
    accountWalletAPIURL: '/user/account/**',
    magicUsersAPIURL: '/users',
    lotteriesAPIURL:'/lottery',
    confirmationAPIURL: '/lottery-purchased/**',
    magicWalletAPIURL: '/wallet',
    walletURL: 'https://master.payments.staging.betitgroup.com',
    walletUsername: 'thrills',
    walletPassword: 'flappybird',
    iphoneUserAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1',
    iphoneViewportWidht: 375,
    iphoneViewportHeight: 812
};

module.exports = globalProperties;
