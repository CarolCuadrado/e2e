Feature: Account closure

    @desk @mobile @regression
    Scenario: Account closure does not displayed
        Given a logged "in" user in megalotto platform
        # And the user clicks on profile overlay icon
        # And the user clicks "account" on the profile overlay menu
        Then the closure account section is not displayed

    # @manual @desktop @mobile @regression
    # Scenario: Successful account closure
    #     Given a logged in user in Megalotto platform
    #     And the user does not have any money in his wallet
    #     And the user does not have any active lottery ticket
    #     And the user does not have any active bonus
    #     When the user clicks on the user profile menu
    #     And clicks on settings
    #     And clicks on account closure
    #     Then account closure menu is shown
    #     And a compliance text with all the information and terms is shown
    #     And links to Responsible Gaming section, Live Help and Privacy notice are displayed
    #     When the user clicks on close account button
    #     Then a verification window requesting the birth of date is displayed
    #     When the user enters the right birth of date
    #     And clicks on close account
    #     Then the user account is closed
    #     And a confirmation message will be shown for 5 seconds
    #     And the user will be automatically logged out and redirected to the home page.

    # @manual @desktop @mobile
    # Scenario: Redirection of don't close account button
    #     Given a logged in user in Megalotto platform
    #     And the user does not have any money in his wallet
    #     And the user does not have any active lottery ticket
    #     And the user does not have any active bonus
    #     When the user goes to account closure menu
    #     And clicks on close account button
    #     Then a verification window requesting the birth of date is displayed
    #     When the user clicks on Don't close button
    #     Then the user is redirected to lobby page

    # @manual @desktop @mobile
    # Scenario: Unsuccessful account closure entering wrong birth date
    #     Given a logged in user in Megalotto platform
    #     And the user does not have any money in his wallet
    #     And the user does not have any active lottery ticket
    #     And the user does not have any active bonus
    #     When the user goes to account closure menu
    #     And clicks on close account button
    #     Then a verification window requesting the birth of date is displayed
    #     When the user enters a wrong birth date
    #     And clicks on close account button
    #     Then an error message "The date of birth is incorrect" is displayed

    # @manual @desktop @mobile
    # Scenario: Trying to close the account with money in the wallet
    #     Given a logged in user in Megalotto platform
    #     And the user has money in his wallet
    #     When the user goes to account closure menu
    #     And clicks on close account button
    #     Then an error message "You cannot close your account because you have money in your account"
    #     And a button to withdraw the money
    #     When the user clicks on the withdraw button
    #     Then the user is redirected to withdraw section in the wallet

    # @manual @desktop @mobile
    # Scenario: Trying to close the account with an active lottery ticket
    #     Given a logged in user in Megalotto platform
    #     And the user has an active lottery ticket
    #     When the user goes to account closure menu
    #     And clicks on close account button
    #     Then an error message "You cannot close your account because you have active lottery tickets. Please contact Customer Support"

    # @manual @desktop @mobile
    # Scenario: Trying to close the account with an active bonus
    #     Given a logged in user in Megalotto platform
    #     And the user has an active bonus
    #     When the user goes to account closure menu
    #     And clicks on close account button
    #     Then an error message "You cannot close your account because first you need to forfeit your active bonuses in your account" is displayed
    #     And a button to Forfeit bonuses is displayed
    #     When the user clicks on Forfeit bonuses button
    #     Then the user is redirected to bonus section in the wallet
