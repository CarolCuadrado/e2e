Feature: Footer

@desk @mobile @regression
Scenario Outline: Checking footer elements
    Given a logged "<statusOfTheUser>" user in megalotto platform
    When the user scrolls down to the footer
    Then footer elements are displayed
    Examples:
        | statusOfTheUser |
        | in              |
        | out             |

@desk @mobile @regression
    Scenario Outline: Checking footer payment methods
    Given a logged "<statusOfTheUser>" user in megalotto platform
    When the user scrolls down to the footer
    Then Payment elements footer are displayed
    Examples:
        | statusOfTheUser |
        | in              |
        | out             |

@desk @mobile @regression
Scenario Outline: Clicking footer elements
    Given a logged "<statusOfTheUser>" user in megalotto platform
    When the user scrolls down to the footer
    And the user clicks "<footerOption>" option in the footer
    Then the user was properly redirected to "<footerOption>" page
    Examples:
        | statusOfTheUser | footerOption              |
        | in              | about-us                  |
        | in              | faq                       |
        | in              | contact-us                |
        | in              | responsible-gaming        |
        | in              | tnc                       |
        | in              | privacy-and-cookie-policy |
        | out             | about-us                  |
        | out             | faq                       |
        | out             | contact-us                |
        | out             | responsible-gaming        |
        | out             | tnc                       |
        | out             | privacy-and-cookie-policy |


