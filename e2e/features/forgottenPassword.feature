Feature: Forgotten password feature

@desk
Scenario: Change password using a registered mail on desktop
    Given a logged 'out' user in megalotto platform
    And the user is using a desktop device
    When the user opens the login window on 'desktop' from top navigation
    And the user clicks on the forgot password link
    Then the reset password window is displayed
    When the user enters a valid registered email "validemail@mail.com" in the field
    Then the send button is enabled
    When the user clicks on the send button
    Then an email is sent to the account entered
    And a success window with a link to home page is displayed
    When the user clicks on the link received in the email
    Then the user is redirected to the platform
    And the change password window is displayed
    When the user enters a new password "NewPassword1!" twice in the fields
    Then the change password button is enabled
    When the user clicks on change password button
    Then the new password is properly set
    And a success window with a link to login page is displayed
    And there is a button redirecting to login modal

@desk @mobile
Scenario: Try to change the password with the link sent after 24 hours
    Given a user who already received the reset password link but has not been used
    When the user opens the link after 24 hours
    Then the user is redirected to the platform
    And the change password window is displayed
    When the user enters a new password "NewPassword1!" twice in the fields
    And the user clicks on change password button
    Then an error message is shown "Reset password link expired"

# @mobile @regression
# Scenario: Change password using a registered mail on mobile
#     Given a logged 'out' user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the login window on 'mobile' from top navigation
#     And the user clicks on the forgot password link
#     Then the reset password window is displayed
#     When the user enters a valid registered email "validemail@mail.com" in the field
#     Then the send button is enabled
#     When the user clicks on the send button
#     Then an email is sent to the account entered
#     And a success window with a link to home page is displayed
#     When the user clicks on the link received in the email
#     Then the user is redirected to the platform
#     And the change password window is displayed
#     When the user enters a new password "NewPassword1!" twice in the fields
#     Then the change password button is enabled
#     When the user clicks on change password button
#     Then the new password is properly set
#     And a success window with a link to login page is displayed
#     And there is a button redirecting to login modal

# @manual @desktop @mobile
# Scenario: Enter a new invalid password and click the button to reveal the entered passwords
#     Given a logged out user in the login page
#     When the user clicks on the forgot password link
#     Then the reset password window is displayed
#     When the user enters a valid registered email in the field
#     Then the send button is enabled
#     When the user clicks on the send button
#     Then an email is sent to the account entered
#     And a success window with a link to home page is displayed
#     When the user clicks on the link received in the email
#     Then the user is redirected to the platform
#     And the change password window is displayed
#     When the user enters two different passwords in the fields
#     Then the button change password is not enabled
#     And an error message is displayed "The passwords entered do not match"
#     When the user clicks on both buttons in the password fields to reveal the passwords
#     Then both passwords are shown in plain text

# @manual @desktop @mobile
# Scenario: Change password using a unregistered mail
#     Given a logged out user in the login page
#     When the user clicks on the forgot password link
#     Then the reset password window is displayed
#     When the user enters a valid but unregistered email in the field
#     And clicks on the send button
#     Then a success window with a link to home page is displayed
#     And the email is never sent as it was not registered in the platform

# @manual @desktop @mobile
# Scenario: Enter an invalid email
#     Given a logged out user in the login page
#     When the user clicks on the forgot password link
#     Then the reset password window is displayed
#     When the user enters an invalid email in the field
#     Then an error message is displayed "The email entered is not valid"
#     And the send button is not enabled

# @manual @desktop @mobile
# Scenario: Try to use the link sent a second time after changing the password properly
#     Given a logged out user who already changed the password using the reset password link
#     When the user opens again the reset password link already sent
#     Then the user is redirected to the platform
#     And the change password window is displayed
#     When the user enters a new valid password in both fields
#     And clicks on change password button
#     Then an error message is shown "Reset password link expired"


