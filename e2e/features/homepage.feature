Feature: Homepage for logged in and out users

    @desktop @regression
    Scenario: Homepage content for logged out user on desktop
        Given a logged "out" user in megalotto platform
        When the user accesses the homepage
        Then the homepage is displayed
        And the top navigation menu is present
        And the homepage contains the "carousel"
        # And the homepage does not contain the "accumulated lottery jackpots"
        # And the homepage does not contain the "active and past lottery tickets section"
        # And the homepage contains the "biggest jackpots section"
        # And the homepage contains the "drawing soon section"
        # And the homepage contains the "casino games section"
        # And the homepage contains the "casino promo box section"
        # And the homepage contains the "popular lotteries section"
        # And the homepage does not contain the "ending soon widget"
        # And the homepage contains the "scratchcards promo box section"
        # And the homepage does not contain the "recently played section"
        # And the homepage contains the "selling points"
        # And the homepage contains the "suggestion box"

    # @mobile @regression
    # Scenario: Homepage content for logged out user on mobile
    #     Given the user is using a mobile device
    #     And a logged "out" user in megalotto platform
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the top navigation menu is present
    #     And the homepage contains the "carousel"
    #     And the homepage does not contain the "accumulated lottery jackpots"
    #     And the homepage does not contain the "active and past lottery tickets section"
    #     And the homepage contains the "biggest jackpots section"
    #     And the homepage contains the "drawing soon section"
    #     And the homepage contains the "casino games section"
    #     And the homepage contains the "casino promo box section"
    #     And the homepage contains the "popular lotteries section"
    #     And the homepage contains the "ending soon widget"
    #     And the homepage contains the "scratchcards promo box section"
    #     And the homepage does not contain the "recently played section"
    #     And the homepage contains the "selling points"
    #     And the homepage contains the "suggestion box"

    # @desktop @regression
    # Scenario: Homepage content for logged in user with purchased lottery tickets on desktop
    #     Given a logged in user in megalotto platform who has "purchased" a lottery ticket
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the top navigation menu is present
    #     And the homepage contains the "carousel"
    #     And the homepage contains the "accumulated lottery jackpots"
    #     And the homepage contains the "active and past lottery tickets section"
    #     And the homepage contains the "biggest jackpots section"
    #     And the homepage contains the "drawing soon section"
    #     And the homepage contains the "casino games section"
    #     And the homepage contains the "casino promo box section"
    #     And the homepage contains the "popular lotteries section"
    #     And the homepage does not contain the "ending soon widget"
    #     And the homepage contains the "scratchcards promo box section"
    #     And the homepage contains the "recently played section"
    #     And the homepage does not contain the "selling points"
    #     And the homepage contains the "suggestion box"

    # @mobile @regression
    # Scenario: Homepage content for logged in user with purchased lottery tickets on mobile
    #     Given a logged in user in megalotto platform who has "purchased" a lottery ticket
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the top navigation menu is present
    #     And the homepage contains the "carousel"
    #     And the homepage contains the "accumulated lottery jackpots"
    #     And the homepage contains the "active and past lottery tickets section"
    #     And the homepage contains the "biggest jackpots section"
    #     And the homepage contains the "drawing soon section"
    #     And the homepage contains the "casino games section"
    #     And the homepage contains the "casino promo box section"
    #     And the homepage contains the "popular lotteries section"
    #     And the homepage contains the "ending soon widget"
    #     And the homepage contains the "scratchcards promo box section"
    #     And the homepage contains the "recently played section"
    #     And the homepage does not contain the "selling points"
    #     And the homepage contains the "suggestion box"

    # @desktop @regression
    # Scenario: Homepage content for logged in user without purchased lottery tickets on desktop
    #     Given a logged in user in megalotto platform who has "not purchased" a lottery ticket
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the top navigation menu is present
    #     And the homepage contains the "carousel"
    #     And the homepage does not contain the "accumulated lottery jackpots"
    #     And the homepage does not contain the "active and past lottery tickets section"
    #     And the homepage contains the "biggest jackpots section"
    #     And the homepage contains the "drawing soon section"
    #     And the homepage contains the "casino games section"
    #     And the homepage contains the "casino promo box section"
    #     And the homepage contains the "popular lotteries section"
    #     And the homepage does not contain the "ending soon widget"
    #     And the homepage contains the "scratchcards promo box section"
    #     And the homepage does not contain the "recently played section"
    #     And the homepage does not contain the "selling points"
    #     And the homepage contains the "suggestion box"

    # @mobile @regression
    # Scenario: Homepage content for logged in user without purchased lottery tickets on mobile
    #     Given a logged in user in megalotto platform who has "not purchased" a lottery ticket
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the homepage contains the "carousel"
    #     And the homepage does not contain the "accumulated lottery jackpots"
    #     And the homepage does not contain the "active and past lottery tickets section"
    #     And the homepage contains the "biggest jackpots section"
    #     And the homepage contains the "drawing soon section"
    #     And the homepage contains the "casino games section"
    #     And the homepage contains the "casino promo box section"
    #     And the homepage contains the "popular lotteries section"
    #     And the homepage contains the "ending soon widget"
    #     And the homepage contains the "scratchcards promo box section"
    #     And the homepage does not contain the "recently played section"
    #     And the homepage does not contain the "selling points"
    #     And the homepage contains the "suggestion box"

    # @desktop @mobile @regression
    # Scenario: Homepage content game providers icon
    #     Given a logged "out" user in megalotto platform
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the game providers icons are displayed for "EU"

    # @desktop @mobile @regression
    # Scenario Outline: Checking game providers according to language
    #     Given a logged "out" user in megalotto platform
    #     And the user enter as "<language>"
    #     When the user accesses the homepage
    #     Then the homepage is displayed
    #     And the game providers icons are displayed for "<language>"

    #     Examples:
    #         | language  |
    #         | Germany   |
    #         | Canada    |
    #         | Finland   |
    #         | NewZeland |