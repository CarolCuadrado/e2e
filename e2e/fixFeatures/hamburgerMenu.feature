# Feature: Hamburger menu

#     @mobile @regression
#     Scenario Outline: Opening hamburger menu and checking elements
#         Given the user is using a mobile device
#         And a logged "<statusOfTheUser>" user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         And the hamburger menu constains all the correct links
#         Examples:
#             | statusOfTheUser |
#             | in              |
#             | out             |

#     @mobile @regression
#     Scenario: Opening profile overlay menu after hamburger menu
#         Given the user is using a mobile device
#         And a logged 'in' user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         When the user clicks on profile overlay icon
#         Then the hamburger menu is no longer visible
#         And the profile overlay menu is open

#     @mobile @regression
#     Scenario Outline: Clicking hamburger menu elements
#         Given the user is using a mobile device
#         And a logged "<statusOfTheUser>" user in megalotto platform
#         When the user opens the hamburger menu
#         And the user clicks "<hamburgerMenuOption>" on the hamburger menu
#         Then the user was properly redirected to "<hamburgerMenuOption>" page
#         Examples:
#             | statusOfTheUser | hamburgerMenuOption |
#             | in              | promo               |
#             | in              | faq                 |
#             | in              | about-us            |
#             | in              | contact-us          |
#             | out             | promo               |
#             | out             | faq                 |
#             | out             | about-us            |
#             | out             | contact-us          |

#     @mobile @regression
#     Scenario: Opening hamburger menu to check login button
#         Given the user is using a mobile device
#         And a logged "out" user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         And the top navigation contains login button
#         And  the user clicks in login button
#         And it should open a modal to sign-in

#     @mobile @regression
#     Scenario: Opening hamburger menu to check deposit button
#         Given the user is using a mobile device
#         And a logged "in" user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         And the top navigation contains deposit button

#     @mobile @regression
#     Scenario: Opening hamburger menu to check login button
#         Given the user is using a mobile device
#         And a logged "out" user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         And the top navigation contains login button
#         And  the user clicks in login button
#         And it should open a modal to sign-in
#         And the user enters his credentials
#         And the user clicks on login button
#         Then the user is logged in properly
#         And the top navigation contains deposit button

#     @mobile @regression
#     Scenario: Opening hamburger menu to check register button
#         Given the user is using a mobile device
#         And a logged "out" user in megalotto platform
#         When the user opens the hamburger menu
#         Then the hamburger menu is open
#         And the top navigation contains login button
#         And the user clicks on registration button
#         Then User navigates to sign-up site
