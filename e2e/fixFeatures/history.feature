# Feature: History

# @manual @desktop @mobile
# Scenario: Correct components on payment transactions history
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Payment Transactions' from transactions dropdown
#     Then Range of dates for payment transaction history is displayed
#     And Transaction dropdown menu is displayed
#     And Payment balances are displayed
#     And Table with payment transactions is displayed
#     And Transactions search button is displayed

# @manual @desktop @mobile
# Scenario: Correct components on gaming transactions history
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Gaming Transactions' from transactions dropdown
#     Then Range of dates for gaming transaction history is displayed
#     And Transaction dropdown menu is displayed
#     And Gaming balances are displayed
#     And Table with gaming transactions is displayed
#     And Transactions search button is displayed

# @manual @desktop @mobile
# Scenario: Transaction dropdown has correct options
#     Given User logs in
#     When User navigates to history site
#     Then Transaction dropdown has correct options

# @manual @desktop @mobile
# Scenario: Default time range in payment transaction history
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Payment Transactions' from transactions dropdown
#     Then Range of dates for 'payment' transaction history is 1 year until today

# @manual @desktop @mobile
# Scenario: Default time range in gaming transaction history
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Gaming Transactions' from transactions dropdown
#     Then Range of dates for 'gaming' transaction history is 1 year until today

# @manual @desktop @mobile
# Scenario: Correct header for payment transactions table
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Payment Transactions' from transactions dropdown
#     Then Correct header is displayed form payment transactions table

# @manual @desktop @mobile
# Scenario: Correct header for gaming transactions table
#     Given User logs in
#     And User navigates to history site
#     When User selects 'Gaming Transactions' from transactions dropdown
#     Then Correct header is displayed form gaming transactions table

# @manual @desktop @mobile
# Scenario: Correct details for deposit transactions
#     Given Expected payment transactions are available
#     And User logs in
#     And User navigates to history site
#     When User clicks on transactions search button
#     And User selects 'Payment Transactions' from transactions dropdown
#     And User sets starting date for '7' days in the past in transactions search
#     And User selects 'Deposits' as payment type
#     And User submits transactions search
#     Then Correct deposit transactions are displayed

# @manual @desktop @mobile
# Scenario: Correct details for withdrawal transactions
#     Given Expected payment transactions are available
#     And User logs in
#     And User navigates to history site
#     When User clicks on transactions search button
#     And User selects 'Payment Transactions' from transactions dropdown
#     And User sets starting date for '7' days in the past in transactions search
#     And User selects 'Withdrawals' as payment type
#     And User submits transactions search
#     Then Correct withdrawal transactions are displayed

# @manual @desktop @mobile @regression
# Scenario: Correct details for gaming transactions
#     Given Expected gaming transactions are available
#     And User logs in
#     And User navigates to history site
#     When User clicks on transactions search button
#     And User selects 'Gaming Transactions' from transactions dropdown
#     And User sets starting date for '7' days in the past in transactions search
#     And User submits transactions search
#     Then Correct gaming transactions are displayed

# @manual @desktop @mobile
# Scenario: Transactions search form
#     Given User logs in
#     And User navigates to history site
#     When User clicks on transactions search button
#     Then Transactions search form is displayed

# @manual @desktop @mobile
# Scenario: Correct components on payment transactions filter
#     Given User logs in
#     And User navigates to history site
#     And User clicks on transactions search button
#     When User selects 'Payment Transactions' from transactions dropdown
#     Then Transaction dropdown menu is displayed
#     And Input fields to enter starting date are displayed
#     And Input fields to enter ending date are displayed
#     And Deposits and Withrawals buttons 'are' displayed
#     And Submit search transactions button is displayed

# @manual @desktop @mobile
# Scenario: Correct components on gaming transactions filter
#     Given User logs in
#     And User navigates to history site
#     And User clicks on transactions search button
#     When User selects 'Gaming Transactions' from transactions dropdown
#     Then Transaction dropdown menu is displayed
#     And Input fields to enter starting date are displayed
#     And Input fields to enter ending date are displayed
#     And Deposits and Withrawals buttons 'are not' displayed
#     And Submit search transactions button is displayed

# @manual @desktop @mobile
# Scenario: Transaction search is correctly set up for payment transactions
#     Given User logs in
#     And User navigates to history site
#     And User clicks on transactions search button
#     And User selects 'Payment Transactions' from transactions dropdown
#     When User sets starting date for '7' days in the past in transactions search
#     Then Ending date in transactions search is 'today'
#     And User submits transactions search
