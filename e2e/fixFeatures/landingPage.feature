# Feature: Landing page

#     @mobile @regression
#     Scenario Outline: Access to landing page for logged <kind> user
#         Given a logged <kind> user in megalotto platform
#         And the user is using a mobile device
#         When user navigates to landing offer page
#         Then user is redirected to <page> page
#         Examples:
#             | kind  | page      |
#             | 'in'  | 'home'    |
#             | 'out' | 'landing' |

#     @desktop @regression
#     Scenario Outline: Access to landing page for logged <kind> user
#         Given a logged <kind> user in megalotto platform
#         And the user is using a desktop device
#         When user navigates to landing offer page
#         Then user is redirected to <page> page
#         Examples:
#             | kind  | page      |
#             | 'in'  | 'home'    |
#             | 'out' | 'landing' |

#     @mobile @regression
#     Scenario: Landing page content mobile
#         Given a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         When user navigates to landing offer page
#         Then user is redirected to 'landing' page
#         And hamburger menu icon is not displayed
#         And landing page banner is displayed
#         And landing page help steps are displayed
#         And landing page description is displayed
#         And landing page second CTA is displayed
#         And landing page TnC is displayed
#         And Payment elements are displayed

#     @desktop @regression
#     Scenario: Landing page content desktop
#         Given a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         When user navigates to landing offer page
#         Then user is redirected to 'landing' page
#         And navigation options are not displayed
#         And landing page offer is displayed
#         And landing page help steps are displayed
#         And landing page description is displayed
#         And landing page second CTA is displayed
#         And landing page TnC is displayed
#         And Payment elements are displayed

#     @desktop @regression
#     Scenario: Landing page banner
#         Given a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         When user navigates to landing offer page
#         Then user is redirected to 'landing' page
#         And landing page banner is displayed
#         And title of landing page banner is displayed
#         And description of landing page banner is displayed
#         And image of landing page banner is displayed
#         And first CTA of landing page banner is displayed

#     @mobile @regression
#     Scenario: Landing page banner
#         Given a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         When user navigates to landing offer page
#         Then user is redirected to 'landing' page
#         And landing page banner is displayed
#         And title of landing page banner is displayed
#         And description of landing page banner is displayed
#         And image of landing page banner is displayed
#         And first CTA of landing page banner is displayed

