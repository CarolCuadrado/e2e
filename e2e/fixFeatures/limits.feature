# Feature: Limits

# @manual @desktop @mobile @regression
# Scenario: Accessing to Create limits site
#     Given User logs in
#     And User navigates to Responsible gaming site
#     When User clicks on "Set a limit" button on View limits site
#     Then User is on Create limits page

# @manual @desktop @mobile
# Scenario: Correct options in dropdown to select limit
#     Given User logs in
#     When User navigates to Create limits site
#     Then Limits dropdown has correct options

# @manual @desktop @mobile @regression
# Scenario Outline: Correct radio options per limit
#     Given User logs in
#     And User navigates to Create limits site
#     When User selects '<option>' from limits dropdown
#     Then Correct radio options are displayed for '<option>'
#     Examples:
#         | option |
#         | Deposit Limit |
#         | Wager Limit |

# @manual @desktop @mobile
# Scenario: Set Limit button is enabled with correct limit settings
#     Given User logs in
#     And User navigates to Create limits site
#     And Set Limit button is 'disabled'
#     When User enters '100' into the limit amount input field
#     Then Set Limit button is 'enabled'

# @manual @desktop @mobile
# Scenario: Correct message in View Limits when user doesn't have limits
#     Given User logs in with account without limits
#     When User navigates to Responsible gaming site
#     Then No-limits message is displayed

# @manual @desktop @mobile
# Scenario: Table with limits in View Limits when user has some limits
#     Given User logs in with account with limits
#     When User navigates to Responsible gaming site
#     Then Limits table is displayed

# @manual @desktop @mobile @regression
# Scenario: User is notified about limit correctly set
#     Given User logs in with account with limits
#     And User navigates to Create limits site
#     When User enters '100000' into the limit amount input field
#     And User clicks on "Set limit" button on Create limits site
#     Then Notification with sucessful limit set is displayed

# @manual @desktop @mobile
# Scenario Outline: User can set deposit limit for all the periods
#     Given User logs in
#     And User navigates to Create limits site
#     When User selects 'Deposit Limit' from limits dropdown
#     And User selects '<period>' radio for Limits
#     And User enters '1000' into the limit amount input field
#     And User clicks on "Set limit" button on Create limits site
#     Then 'Deposit limit' of '1000' for the '<period>' period is set
#     Examples:
#         | period |
#         | day |
#         | week |
#         | month |

# @manual @desktop @mobile
# Scenario Outline: User can set wager limit for all the periods
#     Given User logs in
#     And User navigates to Create limits site
#     When User selects 'Wager Limit' from limits dropdown
#     And User selects '<period>' radio for Limits
#     And User enters '1000' into the limit amount input field
#     And User clicks on "Set limit" button on Create limits site
#     Then 'Wager limit' of '1000' for the '<period>' period is set
#     Examples:
#         | period |
#         | day |
#         | week |
#         | month |

# @manual @desktop @mobile
# Scenario Outline: User can set loss limit for all the periods
#     Given User logs in
#     And User navigates to Create limits site
#     When User selects 'Loss limit' from limits dropdown
#     And User selects '<period>' radio for Limits
#     And User enters '1000' into the limit amount input field
#     And User clicks on "Set limit" button on Create limits site
#     Then 'Loss limit' of '1000' for the '<period>' period is set
#     Examples:
#         | period |
#         | day |
#         | week |
#         | month |

# @manual @desktop @mobile
# Scenario: User can set session limit for all the periods
#     Given User logs in
#     And User navigates to Create limits site
#     When User selects 'Session Limit' from limits dropdown
#     And User enters '1000' into the limit amount input field
#     And User clicks on "Set limit" button on Create limits site
#     Then Session limit of '1000' minutes is set
