# Feature: Login

# @desktop @smoke @regression
# Scenario: Standard successful login on desktop
#     Given a logged 'out' user in megalotto platform
#     And the user is using a desktop device
#     When the user opens the login window on 'desktop' from top navigation
#     And the user enters his credentials
#     And the user clicks on login button
#     Then the user is logged in properly

# @mobile @smoke @regression
# Scenario: Standard successful login on mobile
#     Given a logged 'out' user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the login window on 'mobile' from top navigation
#     And the user enters his credentials
#     And the user clicks on login button
#     Then the user is logged in properly

# @desktop @regression
# Scenario: Standard unsuccessful login on desktop
#     Given a logged 'out' user in megalotto platform
#     And the user is using a desktop device
#     When the user opens the login window on 'desktop' from top navigation
#     And the user enters invalid credentials
#     And the user clicks on login button
#     Then the login failed

# @mobile @regression
# Scenario: Standard unsuccessful login on mobile
#     Given a logged 'out' user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the login window on 'mobile' from top navigation
#     And the user enters invalid credentials
#     And the user clicks on login button
#     Then the login failed

# @desktop @mobile
# Scenario: Login with API call
#     Given a logged 'in' user in megalotto platform
#     When the user visits his profile
#     Then the profile page is displayed properly

# @desktop @regression
# Scenario Outline: Standard unsuccessful login on desktop
#     Given a logged 'out' user in megalotto platform
#     And the user is using a desktop device
#     When the user opens the login window on 'desktop' from top navigation
#     And the user who is '<statusOfTheUser>' enters his credentials
#     And the user clicks on login button
#     Then the login failed
#     Examples:
#         | statusOfTheUser |
#         | Blocked         |
#         | Closed          |

# @mobile @regression
# Scenario Outline: Standard unsuccessful login on mobile
#     Given a logged 'out' user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the login window on 'mobile' from top navigation
#     And the user who is '<statusOfTheUser>' enters his credentials
#     And the user clicks on login button
#     Then the login failed
#     Examples:
#         | statusOfTheUser |
#         | Blocked         |
#         | Closed          |

# @manual @desktop @mobile
# Scenario Outline: Contain of the login page
#     Given a logged 'out' user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     When the user opens the login window on '<kindOfDevice>' from top navigation
#     Then the login page is displayed
#     And the login page has two fields, one for the email and other for the password
#     And a link to forgot password feature is displayed
#     And a link to create new account is displayed
#     Examples:
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Button disabled until a valid email and password have been entered
#     Given a logged 'out' user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     When the user opens the login window on '<kindOfDevice>' from top navigation
#     Then the login page is displayed
#     And the log in button is disabled as the user did not enter neither email nor password
#     When the user enters a bad email format
#     Then an error message "Enter a valid email address" below the email field is displayed
#     And the log in button is still disabled
#     When the user enters a password with less than 8 chars
#     Then an error message "Please enter a password with at least 8 chars" below the password field is displayed
#     And the log in button is still disabled
#     When the user enters a valid email and a password with at least 8 chars length
#     Then the log in button is enabled
#     Examples:
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Login with a restricted/self-excluded/timed-out user
#     Given a logged 'out' user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And the user is '<kindOfExclusion>'
#     When the user opens the login window on '<kindOfDevice>' from top navigation
#     And user completes log in with the right credentials
#         | email address    | password      |
#         | anon@example.com | rightPassword |
#     Then the log in button is enabled
#     When the user clicks on the log in button
#     Then the user is not authenticated
#     And an error message "Access to your account is restricted. Please contact support" is displayed
#     Examples:
#         | kindOfExclusion | kindOfDevice |
#         | restricted      | mobile       |
#         | self-excluded   | mobile       |
#         | timed-out       | mobile       |
#         | restricted      | desktop      |
#         | self-excluded   | desktop      |
#         | timed-out       | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Login with the right credentials after entering a wrong password 3 times
#     Given a logged 'out' user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     When the user opens the login window on '<kindOfDevice>' from top navigation
#     And user tries to log in with the wrong credentials 3 times
#         | email address    | password      |
#         | anon@example.com | wrongPassword |
#     Then user is not authenticated
#     And an error message "Your username or password is incorrect" is displayed
#     When user enters the right credentials to log in the platform
#         | email address    | password      |
#         | anon@example.com | rightPassword |
#     Then an error message "Access to your account is restricted. Please contact support" is displayed
#     And the user is not able to log in the platform until 60 minutes after
#     Examples:
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Button to reveal the entered password
#     Given a logged 'out' user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     When the user opens the login window on '<kindOfDevice>' from top navigation
#     And enters his credentials
#     And clicks on the button in the password field to reveal the entered password
#     Then the password entered will be visible as plain text
#     Examples:
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |
