# Feature: Lotteries page

# @manual @desktop
# Scenario Outline: Content of lotteries page on desktop
#     Given a <statusOfTheUser> user in megalotto platform using a desktop device
#     When the user clicks on lotteries option in the header menu
#     Then the user is redirected to lotteries page
#     And Matrix of lotteries is displayed
#     And Section with promotions is displayed
#     And Sorting lottery component is displayed
#     And Lotteries are sorted by 'Jackpot'
#     And There is no button to load more lotteries
#     And the row of promotional boxes is horizontally scrollable
#     And Ending soon lottery widget is not displayed
#     Examples:
#         | statusOfTheUser |
#         | logged in       |
#         | logged out      |

# @manual @mobile
# Scenario Outline: Content of lotteries page on mobile
#     Given a <statusOfTheUser> user in megalotto platform using a mobile device
#     When the user clicks on lotteries option in the hamburger menu
#     Then the user is redirected to lotteries page
#     And lotteries page is displayed with the following elements: Two rows of vertical single lottery tickets with 2 lottery tickets per row, One row of promotional boxes with 1 item and three more rows of vertical single lottery tickets with 2 lottery tickets per row
#     And lotteries will be displayed by draw date by default
#     And there is a button to load more lotteries
#     And the row of promotional boxes is not horizontally scrollable
#     When the user clicks on load more button
#     And the remaining amount of lotteries are loaded
#     And Ending soon lottery widget is displayed
#     Examples:
#         | statusOfTheUser |
#         | logged in       |
#         | logged out      |

# @mobile
# Scenario: Clicking on load more lotteries button 
#     Given the user is using a mobile device
#     And a logged 'out' user in megalotto platform
#     When the user opens the hamburger menu
#     And the user clicks 'lotteries' on the hamburger menu
#     Then lotteries page is displayed
#     When the user clicks on load more lotteries button
#     Then the remaining amount of lotteries are loaded

# @manual @desktop @mobile
# Scenario Outline: Checking currency for unauthenticated and authenticated players
#     Given a <statusOfTheUser> user in megalotto platform
#     And the user is <isReturningVisitor>
#     When the user goes to lotteries page
#     And the user checks one lottery details
#     Then the jackpot is shown in <currency> and converted using exchange rate
#     And ticket prices are shown in <currency> and converted using simplified multiplier
#     Examples:
#         | statusOfTheUser | isReturningVisitor      | currency                     |
#         | logged in       | a returning visitor     | player's registered currency |
#         | logged in       | not a returning visitor | player's registered currency |
#         | logged out      | a returning visitor     | player's registered currency |
#         | logged out      | not a returning visitor | locations's default currency |

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when less than 24 hours in lotteries page
#     Given a <statusOfTheUser> user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is less than 24 hours
#     When the user checks the draw time of that lottery in <componentToCheck>
#     Then the remaining time until draw is displayed with the format XX hours XX minutes
#         | statusOfTheUser | kindOfDevice | componentToCheck     |
#         | logged in       | mobile       | lotteries page       |
#         | logged in       | desktop      | lottery details page |
#         | logged out      | desktop      | lotteries page       |
#         | logged out      | mobile       | lottery details page |

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when between 24 and 48 hours in lotteries page
#     Given a <statusOfTheUser> user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is more than 24 hours but less than 48
#     When the user checks the draw time of that lottery in <componentToCheck>
#     Then the remaining time until draw is displayed with the format 1 day XX hours
#         | statusOfTheUser | kindOfDevice | componentToCheck     |
#         | logged in       | mobile       | lotteries page       |
#         | logged in       | desktop      | lottery details page |
#         | logged out      | desktop      | lotteries page       |
#         | logged out      | mobile       | lottery details page |

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when more than 48 hours in lotteries page
#     Given a <statusOfTheUser> user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is more than 48 hours
#     When the user checks the draw time of that lottery in <componentToCheck>
#     Then the remaining time until draw is displayed with the format XX days XX hours
#         | statusOfTheUser | kindOfDevice | componentToCheck     |
#         | logged in       | mobile       | lotteries page       |
#         | logged in       | desktop      | lottery details page |
#         | logged out      | desktop      | lotteries page       |
#         | logged out      | mobile       | lottery details page |
