# Feature: Lottery details page

# @desktop @regression
# Scenario Outline: Accessing lotteriy details page using a desktop device
#     Given a logged '<statusOfTheUser>' user in megalotto platform
#     When the user clicks 'lotteries' on top navigation menu
#     Then lotteries page is displayed
#     When the user clicks on 'powerball' lottery
#     Then the user is redirected to 'powerball' lottery details
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @mobile @regression
# Scenario Outline: Accessing lottery details page using a mobile device
#     Given the user is using a mobile device
#     And a logged '<statusOfTheUser>' user in megalotto platform
#     When the user opens the hamburger menu
#     And the user clicks 'lotteries' on the hamburger menu
#     Then lotteries page is displayed
#     When the user clicks on 'powerball' lottery
#     Then the user is redirected to 'powerball' lottery details
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @desktop @mobile @regression
# Scenario: Content of lottery details page
#     Given a logged 'in' user in megalotto platform
#     When the user clicks 'lotteries' on top navigation menu
#     Then lotteries page is displayed
#     When the user clicks 'lotteries' on top navigation menu
#     Then lotteries page is displayed
#     When the user clicks on 'powerball' lottery
#     Then the user is redirected to 'powerball' lottery details
#     And Tabs are displayed
#     And User clicks on information tab
#     And Information section is displayed
#     And Payment methods section 'is' displayed
#     And User clicks on results tab
#     And Results section is displayed
#     And Payment methods section 'is not' displayed



# @desktop @mobile @regression
# Scenario: Content of payment method in lottery details page
#     Given a logged 'out' user in megalotto platform
#     And the user clicks 'lotteries' on top navigation menu
#     And lotteries page is displayed
#     And the user clicks 'lotteries' on top navigation menu
#     And lotteries page is displayed
#     When the user clicks on 'powerball' lottery
#     Then the user is redirected to 'powerball' lottery details
#     And Tabs are displayed
#     And User clicks on information tab
#     When Information section is displayed
#     And Payment methods section 'is' displayed
#     And Payment elements are displayed

 
