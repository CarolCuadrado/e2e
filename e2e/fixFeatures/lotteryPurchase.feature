# Feature: Purchase lottery ticket feature

#     Background:
#         Given Use mocked APIs

#     @desktop @mobile @regression
#     Scenario Outline: Purchase lottery modal with a logged in user without enough money to buy a ticket and no card
#         Given a user who does not have enough money on wallet to buy a lottery ticket
#         And the user does not have any card registered
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "<pageToAccessModal>"
#         Then the purchase modal is displayed
#         And user wallet balance is displayed
#         And the purchase button is "enabled" with the "Deposit" option
#         Examples:
#             | pageToAccessModal  |
#             | lotteries page     |
#             | ending soon widget |

#     @desktop @mobile @regression
#     Scenario: Purchase lottery modal with a logged in user without enough money to buy a ticket and no card
#         Given a user who does not have enough money on wallet to buy a lottery ticket
#         And the user does not have any card registered
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then user wallet balance is displayed
#         And the purchase button is "enabled" with the "Deposit" option


#     @desktop @mobile @regression
#     Scenario Outline: Accessing purchase lottery modal with a logged in user with enough money to buy the ticket
#         Given a user who has enough money on wallet to buy a lottery ticket
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "<pageToAccessModal>"
#         Then user wallet balance is displayed
#         And the purchase button is "enabled" with the "Purchase" option
#         When the user clicks on the purchase button
#         Then lottery ticket is purchased
#         Examples:
#             | pageToAccessModal  |
#             | lotteries page     |
#             | ending soon widget |
#             | lottery details    |

#     @desktop @mobile @regression
#     Scenario: Accessing purchase lottery details, purchase and confirmation page (mockup) test
#         Given a user who has enough money on wallet to buy a lottery ticket
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then user wallet balance is displayed
#         And the purchase button is "enabled" with the "Purchase" option
#         When the user clicks on the purchase button
#         Then lottery ticket is purchased
#         Then confirmation page appears

#     @desktop @regression
#     Scenario:  Accessing confirmation page check elements
#         Given confirmation page appears
#         And it displays the next jackpot on the left
#         And it displays the link to see the tickets
#         And it displays lottery lines and a button to expand and see all
#         And it shows the remaining time until next draw on the right
#         And it shows the back button
#         When click on back home
#         Then the user is redirect to home

#     @desktop @regression
#     Scenario Outline: Accessing purchase lottery modal with a logged out user and clicking the register button
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "<pageToAccessModal>"
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the register button
#         Then User navigates to sign-up site
#         Examples:
#             | pageToAccessModal  |
#             | lotteries page     |
#             | ending soon widget |

#     @mobile @regression
#     Scenario Outline: Accessing purchase lottery modal with a logged out user and clicking the register button
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "<pageToAccessModal>"
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the register button
#         Then User navigates to sign-up site
#         Examples:
#             | pageToAccessModal  |
#             | lotteries page     |
#             | ending soon widget |


#     @desktop @regression
#     Scenario: Accessing purchase lottery modal with a logged out user and clicking the register button
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then the ticket has 3 randomly generated lines in lottery details
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the register button
#         Then User navigates to sign-up site

#     @mobile @regression
#     Scenario: Accessing purchase lottery modal with a logged out user and clicking the register button
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then the ticket has 3 randomly generated lines in lottery details
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the register button
#         Then User navigates to sign-up site


#     @desktop @mobile @regression
#     Scenario: Accessing purchase lottery modal with a logged out user and clicking the login link
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then the ticket has 3 randomly generated lines in lottery details
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the login link
#         Then the login modal is displayed
#         And  the user enters his credentials
#         And the user clicks on login button

#     @desktop @mobile @regression
#     Scenario: Accessing purchase lottery modal with a logged out user and clicking the login link
#         Given a logged "out" user in megalotto platform
#         When the user clicks on a play button on "lotteries page"
#         And payment method section is not displayed
#         And the purchase button is "enabled" with the "Register" option
#         And there is a login link
#         When the user clicks on the login link
#         Then the login modal is displayed
#         When the user enters his credentials
#         And the user clicks on login button
#         Then the purchase modal for the lottery the user was purchasing is shown again

#     @desktop @mobile @regression
#     Scenario: Purchase lottery modal with a logged in user without enough money to buy a ticket and a card registered
#         Given a user who does not have enough money on wallet to buy a lottery ticket
#         And the user has registered previously a credit or debit card
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "lottery details"
#         Then user wallet balance is displayed
#         And last active card is displayed and selected on payment options
#         And the purchase button is "disabled" with the "Purchase" option
#         When the user enters the correct CVC code
#         And the user clicks on the purchase button
#         Then lottery ticket is purchased

#     @manual @desktop @mobile
#     Scenario Outline: Opening the purchase lottery modal a lottery whose cut off date has taken place but the draw date has not
#         Given a <statusOfTheUser> user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user opens the purchase modal for a lottery whose cut off time has taken place
#         But the draw date for that lottery has not taken place
#         Then the draw date displayed at the top of the modal belongs to the next draw for that lottery
#         When the user purchases the lottery ticket
#         Then the ticket purchased is for the next draw date
#         Examples:
#             | statusOfTheUser | kindOfDevice |
#             | logged in       | mobile       |
#             | logged out      | desktop      |
#             | logged in       | desktop      |
#             | logged out      | mobile       |

#     @manual @desktop @mobile
#     Scenario Outline: Trying to purchase a lottery ticket when the cut off time has taken place
#         Given a <statusOfTheUser> user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user opens the purchase modal for a lottery just before the cutt off time has taken place
#         And the user tries to purchase the lottery ticket
#         Then there is an error displaying that the lottery ticket could not be purchased for that date
#         Examples:
#             | statusOfTheUser | kindOfDevice |
#             | logged in       | mobile       |
#             | logged out      | desktop      |
#             | logged in       | desktop      |
#             | logged out      | mobile       |

#     @manual @desktop @mobile
#     Scenario Outline: Check default number of lines when it is smaller than max amount of lines
#         Given a <statusOfTheUser> user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user opens the purchase modal for a lottery
#         And the lottery has set 4 as default number of lines
#         And the lottery has set 6 has max amount of lines
#         Then purchase modal is open with 4 lottery lines
#         Examples:
#             | statusOfTheUser | kindOfDevice |
#             | logged in       | mobile       |
#             | logged in       | desktop      |
#             | logged out      | mobile       |
#             | logged out      | desktop      |

#     @manual @desktop @mobile
#     Scenario Outline: Check default number of lines when it is bigger than max amount of lines
#         Given a <statusOfTheUser> user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user opens the purchase modal for a lottery
#         And the lottery has set 7 as default number of lines
#         And the lottery has set 5 has max amount of lines
#         Then purchase modal is open with 5 lottery lines
#         Examples:
#             | statusOfTheUser | kindOfDevice |
#             | logged in       | mobile       |
#             | logged in       | desktop      |
#             | logged out      | mobile       |
#             | logged out      | desktop      |

#     @manual @desktop @mobile
#     Scenario Outline: Opening purchase modal from autoplay and play again these numbers buttons
#         Given a logged in user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user clicks on <buttonToClick> from a ticket details
#         Then the purchase modal is open for that specific lottery
#         And the purchase modal has as many lottery lines as the previous ticket has
#         Examples:
#             | kindOfDevice | buttonToClick |
#             | mobile       | Autoplay      |
#             | desktop      | Play again    |
#             | mobile       | Play again    |
#             | desktop      | Autoplay      |

#     @manual @desktop @mobile
#     Scenario Outline: Opening information tooltip
#         Given a <statusOfTheUser> user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         And the user is on purchase lottery ticket modal
#         When the user clicks on the icon on the top right
#         Then a tooltip with information is displayed
#         Examples:
#             | statusOfTheUser | kindOfDevice |
#             | logged in       | mobile       |
#             | logged out      | desktop      |
#             | logged in       | desktop      |
#             | logged out      | mobile       |

#     @manual @desktop @mobile
#     Scenario Outline: Checking that purchase modal is not open after close it and make a deposit
#         Given a logged in user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         And the user does not have enough money on wallet to buy a lottery ticket
#         And the user does not have any card registered
#         When the user clicks on a play button on <pageToAccessModal>
#         Then the purchase modal for that specific lottery is displayed
#         When the user clicks on close button
#         Then the purchase modal is closed
#         When the user makes a deposit through wallet
#         Then the purchase modal is not open
#         Examples:
#             | kindOfDevice | pageToAccessModal                 |
#             | mobile       | lotteries page                    |
#             | mobile       | lottery details                   |
#             | mobile       | suggested lottery on user profile |
#             | mobile       | drawing soon section              |
#             | mobile       | recently played section           |
#             | mobile       | ending soon widget                |
#             | desktop      | lotteries page                    |
#             | desktop      | lottery details                   |
#             | desktop      | drawing soon section              |
#             | desktop      | recently played section           |
#             | desktop      | ending soon widget                |

#     @manual @desktop @mobile
#     Scenario Outline: Checking purchase modal is not open again after the user closes the modal and logs in
#         Given a logged out user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         And the user is on purchase modal
#         When the user closes the modal
#         And the user logs in the application
#         Then login is performed properly
#         And purchase modal is not displayed
#         Examples:
#             | kindOfDevice |
#             | mobile       |
#             | desktop      |
#     @manual @desktop @mobile
#     Scenario Outline: Purchase lottery modal with a logged in user after making a deposit
#         Given a user who does not have enough money on wallet to buy a lottery ticket
#         And the user does not have any card registered
#         And a logged "in" user in megalotto platform
#         When the user clicks on a play button on "<pageToAccessModal>"
#         Then the purchase modal is displayed
#         And user wallet balance is displayed
#         And the purchase button is "enabled" with the "Deposit" option
#         When the user clicks on deposit button
#         Then the user is redirected to wallet
#         When the user deposits money
#         Then the user is redirected back to the previous lottery ticket he was purchasing with the same numbers and lines
