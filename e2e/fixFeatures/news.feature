# Feature: News

#     News feature for logged in and out users

#     @desktop @regression
#     Scenario Outline: Accessing news section
#         Given a logged <kind> user in megalotto platform
#         And the user is using a desktop device
#         When the user clicks 'news' on top navigation menu
#         Then news section is displayed
#         And there is '6' news displayed
#         And each new contains image
#         And each new contains description
#         And there is a load more button at the bottom of the page
#         When the user clicks on the load more button
#         Then there is '8' news displayed
#         Examples:
#             | kind  |
#             | 'in'  |
#             | 'out' |

#     @mobile @regression
#     Scenario Outline: Accessing news section
#         Given a logged <kind> user in megalotto platform
#         And the user is using a mobile device
#         And the user opens the hamburger menu
#         When the user clicks 'news' on the hamburger menu
#         Then news section is displayed
#         And there is '3' news displayed
#         And each new contains image
#         And each new contains description
#         And there is a load more button at the bottom of the page
#         When the user clicks on the load more button
#         Then there is '6' news displayed
#         Examples:
#             | kind  |
#             | 'in'  |
#             | 'out' |

#     @desktop @regression
#     Scenario Outline: Clicking news
#         Given a logged <kind> user in megalotto platform
#         And the user is using a desktop device
#         And the user clicks 'news' on top navigation menu
#         And news section is displayed
#         When the user clicks the image of a new
#         Then the user was properly redirected to <News> page
#         Examples:
#             | kind  | News      |
#             | 'in'  | 'profile' |
#             | 'out' | 'profile' |

#  @mobile @regression
#     Scenario Outline: Clicking news
#         Given a logged <kind> user in megalotto platform
#         And the user is using a mobile device
#         And the user opens the hamburger menu
#         When the user clicks 'news' on the hamburger menu
#         And news section is displayed
#         When the user clicks the image of a new
#         Then the user was properly redirected to <News> page
#         Examples:
#             | kind  | News      |
#             | 'in'  | 'profile' |
#             | 'out' | 'profile' |
