# Feature: Panic bar

# Background:
#     Given current time is stopped

# @smoke
# Scenario: Panic bar on session with Swedish account
#     Given User logs in with a 'Swedish' account
#     When User navigates to main site
#     Then Panic bar 'is' displayed

# @smoke
# Scenario: Panic bar on session with non-Swedish account
#     Given User logs in
#     When User navigates to main site
#     Then Panic bar 'is not' displayed

# @manual
# Scenario: Panic bar shows current session timer
#     Given User logs in with a 'Swedish' account
#     When User navigates to main site
#     Then Panic bar shows the time since login
