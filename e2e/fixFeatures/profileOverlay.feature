# Feature: Profile overlay menu

# @desktop @mobile @regression
# Scenario: Checking all the elements in profile overlay menu
#     Given a logged 'in' user in megalotto platform
#     When the user clicks on profile overlay icon
#     Then the profile overlay menu is open
#     And the profile overlay menu contains all the correct links


# @desktop @mobile @regression
# Scenario: Opening wallet from profile overlay menu
#     Given a logged 'in' user in megalotto platform
#     When the user clicks on profile overlay icon
#     Then the profile overlay menu is open
#     When the user clicks "deposit" on the profile overlay menu
#     Then the wallet is open

# @desktop @mobile @regression
# Scenario: Logging out a user
#     Given a logged 'in' user in megalotto platform
#     When the user clicks on profile overlay icon
#     When the user clicks "logout" on the profile overlay menu
#     Then the profile overlay menu is open
#     Then the user is logged out

# @desktop @mobile @regression
# Scenario Outline: Clicking profile overlay menu elements
#     Given a logged 'in' user in megalotto platform
#     When the user clicks on profile overlay icon
#     Then the profile overlay menu is open
#     When the user clicks "<profileOverlayOption>" on the profile overlay menu
#     Then the user was properly redirected to "<profileOverlayOption>" page
#     Examples:
#         | profileOverlayOption |
#         | wallet               |
#         | profile              |
#         | messages             |
#         | promo                |
#         | account              |

# @desktop @mobile @regression
# Scenario Outline: Notification for unread messages on profile overlay
#     Given User logs in with account '<statusOfMessages>' unread messages
#     When the user clicks on profile overlay icon
#     Then the profile overlay menu is open
#     And Notification about unread messages '<statusOfNotification>' displayed in profile overlay
#     Examples:
#         | statusOfMessages | statusOfNotification |
#         | with | is |
#         | without | is not |
