# Feature: Reality Check

# Scenario: Correct components in Reality check site
#     Given User logs in
#     When User navigates to Reality check site
#     Then Reality check section of Responsible gaming is displayed
#     And Reality check description is displayed
#     And Correct radio options are displayed for reality check
#     And Submit button is displayed on Reality check site

# Scenario Outline: Submit new interval for Reality check
#     Given User logs in
#     And User navigates to Reality check site
#     When User selects '<interval>' radio for reality check
#     And User clicks on "Submit" button on Reality check site
#     And Value for radio group for reality check is '<interval>'
#     And User refreshes the page
#     Then Value for radio group for reality check is '<interval>'
#     Examples:
#         | interval |
#         | none |
#         | 15 |
#         | 30 |
#         | 60 |
