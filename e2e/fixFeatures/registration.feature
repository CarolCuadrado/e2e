# Feature: Registration

#     @desktop
#     Scenario: Registration window is displayed from login modal on desktop
#         Given a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         When the user opens the login window on 'desktop' from top navigation
#         And the user clicks on registration link
#         Then User navigates to sign-up site

#     @mobile @regression
#     Scenario: Registration window is displayed from login modal on mobile
#         Given a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         When the user opens the login window on 'mobile' from top navigation
#         And the user clicks on registration link
#         Then User navigates to sign-up site

#     @mobile @regression
#     Scenario: Registration window is displayed from login modal on mobile from landing page
#         Given a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         And user navigates to landing offer page
#         When the user opens the login window on 'mobile' from top navigation
#         And the user clicks on registration link
#         Then User navigates to sign-up site

#     @deskop @regression
#     Scenario: Registration window is displayed from on desktop from landing page
#         Given a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         And user navigates to landing offer page
#         When the user opens the login window on 'desktop' from top navigation
#         And the user clicks on registration link
#         Then User navigates to sign-up site

#     @desktop @regression
#     Scenario: Registration window is displayed from top navigation on desktop
#         Given a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         When the user opens the registration window on 'desktop' from top navigation
#         Then User navigates to sign-up site

#     @mobile @regression
#     Scenario: Registration window is displayed from top navigation on mobile
#         Given a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         When the user opens the registration window on 'mobile' from top navigation
#         Then User navigates to sign-up site

#     @desktop @regression
#     Scenario: Create a new user calling directly the API on deskop
#         Given a user who just completed the registration flow
#         And a logged 'out' user in megalotto platform
#         And the user is using a desktop device
#         When the user opens the login window on 'desktop' from top navigation
#         And enters his credentias for the just created account
#         And the user clicks on login button
#         Then the user is logged in properly

#     @mobile @regression
#     Scenario: Create a new user calling directly the API on mobile
#         Given a user who just completed the registration flow
#         And a logged 'out' user in megalotto platform
#         And the user is using a mobile device
#         When the user opens the login window on 'mobile' from top navigation
#         And enters his credentias for the just created account
#         And the user clicks on login button
#         Then the user is logged in properly

#     @manual @desktop @mobile
#     Scenario Outline: Registering an non existing account and checking scroll
#         Given a logged 'out' user in megalotto platform
#         And the user is using a <kindOfDevice> device
#         And the user opens the login window on 'mobile' from top navigation
#         And the user clicks on registration link
#         And is not possible to scroll outside the modal
#         When the user fills all the required fields with an account that is not registered in the system
#         Then the account is registered properly
#         And the user is logged in the platform
#         Examples:
#             | kindOfDevice |
#             | desktop      |
#             | mobile       |

#     @mobile @regression
#     Scenario: Trying to register an existing account
#         Given a logged 'out' user in megalotto platform
#         And the user opens the login window on 'mobile' from top navigation
#         And the user clicks on registration link
#         And User navigates to sign-up site
#         When User is on step '1' of sign-up
#         Then Message 'is' displayed on sign-up modal
#         And Complete step 1 of sign-up with invalid data
#         Then an error message is displayed

#     # with new reg works in the last step Verify and it shows the error without reason.Error :55b24e5a699bb5e0f9247932
#     @manual @desktop @mobile
#     Scenario: Trying to register a similar account
#         Given a logged out user in Megalotto platform
#         When the user clicks on login window
#         And clicks on create a new account link
#         Then registration windows is displayed
#         When the user fills all the required fields with similar data that an account already registered on the system
#         Then an error message is displayed "A similar account is already registered on the system"
#         And the user is not able to continue the sign up process

#     @mobile @regression
#     Scenario: Trying to register a similar combination Mobile and prefix
#         Given a logged 'out' user in megalotto platform
#         And the user opens the login window on 'mobile' from top navigation
#         And the user clicks on registration link
#         And User navigates to sign-up site
#         And Message 'is' displayed on sign-up modal
#         And Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         When Complete step 2 of sign-up with invalid data in Prefix and mobile
#         And Submit step 2 of sign-up
#         Then error notification is displayed


#     @manual @desktop @mobile
#     Scenario Outline: Opening Terms and Conditions link
#         Given a logged out user in Megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the accesses to sign up window
#         And clicks on terms and conditions link
#         Then terms and conditions modal will be opened on top of sign up modal
#         Examples:
#             | kindOfDevice |
#             | desktop      |
#             | mobile       |

#     @manual @desktop @mobile
#     Scenario Outline: Registration from an affiliate site
#         Given a logged out user in Megalotto platform
#         And the user is using a <kindOfDevice> device
#         When the user accesses the sign up window from an affiliate site
#         Then the btag code is part of registation data in the URL
#         Examples:
#             | kindOfDevice |
#             | desktop      |
#             | mobile       |

#     @desktop @mobile @regression
#     Scenario: Sucess new account creation
#         Given User navigates to sign-up site
#         When Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         Then User is on step '3' of sign-up

#     @desktop @mobile @regression
#     Scenario: Error message
#         Given User navigates to sign-up site
#         When Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And Complete step 2 of sign-up with invalid data
#         And Submit step 2 of sign-up
#         Then error notification is displayed

#     @desktop @mobile @regression
#     Scenario: Progress bar on sign-up process shows error SMS using the same phone
#         Given User navigates to sign-up site
#         Then Sign-up progress bar is displayed
#         And 'Step 1' is highlighted on sign-up progress bar
#         And Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And Sign-up progress bar is displayed
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         Then error notification is displayed

#     @desktop @mobile @regression
#     Scenario: Close button on Step 1 of sign-up process
#         Given User navigates to sign-up site
#         And User is on step '1' of sign-up
#         When User clicks on close button of modal
#         Then Close confirmation is displayed

#     @desktop @mobile @regression
#     Scenario: Close button on Step 2 of sign-up process
#         Given User navigates to sign-up site
#         When Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And User is on step '2' of sign-up
#         And User clicks on close button of modal
#         Then Close confirmation is displayed

#     @desktop @mobile @regression
#     Scenario: Close button on Verification of sign-up process
#         Given User navigates to sign-up site
#         When Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         And User is on step '3' of sign-up
#         And User clicks on close button of modal
#         Then Close confirmation is displayed

#     @desktop @mobile @regression
#     Scenario: Content of close confirmation window for sign-up
#         Given User navigates to sign-up site
#         And User is on step '1' of sign-up
#         When User clicks on close button of modal
#         And Close confirmation is displayed
#         Then A message is displayed in close confirmation modal
#         And Continue button is displayed in close confirmation modal
#         And Cancel button is displayed in close confirmation modal


#     @desktop @mobile
#     Scenario Outline: Actions in close confirmation window for sign-up
#         Given User navigates to sign-up site
#         And User is on step '1' of sign-up
#         And User clicks on close button of modal
#         When User clicks on <actionButton> button in close confirmation modal
#         Then User is on <site>
#         Examples:
#             | actionButton | site                |
#             | Continue     | step '1' of sign-up |
#             | Cancel       | main page           |

#     @desktop @mobile
#     Scenario: Message during sign-up process
#         Given User navigates to sign-up site
#         When User is on step '1' of sign-up
#         Then Message 'is' displayed on sign-up modal
#         And Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And User is on step '2' of sign-up
#         And Message 'is' displayed on sign-up modal
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         And User is on step '3' of sign-up
#         And Message 'is not' displayed on sign-up modal

#     @desktop
#     Scenario: Image during sign-up process
#         Given User navigates to sign-up site
#         When User is on step '1' of sign-up
#         Then Image 'is' displayed on sign-up modal
#         And Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And User is on step '2' of sign-up
#         And Image 'is' displayed on sign-up modal
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         And User is on step '3' of sign-up
#         And Image 'is' displayed on sign-up modal

#     @mobile
#     Scenario: Image during sign-up process
#         Given User navigates to sign-up site
#         When User is on step '1' of sign-up
#         Then Image 'is not' displayed on sign-up modal
#         And Complete step 1 of sign-up with valid data
#         And Entered e-mail in step 1 is validated by backend
#         And Submit step 1 of sign-up
#         And User is on step '2' of sign-up
#         And Image 'is not' displayed on sign-up modal
#         And Complete step 2 of sign-up with valid data
#         And Submit step 2 of sign-up
#         And User is on step '3' of sign-up
#         And Image 'is not' displayed on sign-up modal

#     @desktop @mobile @regression
#     Scenario: Password requirements when nothing has been introduced yet
#         Given User navigates to sign-up site
#         When Password input field of sign-up is empty
#         Then Password requirements are displayed
#         And 'Characters long' password requirement is 'inactive'
#         And 'DigitOrSymbol' password requirement is 'inactive'
#         And 'Uppercase' password requirement is 'inactive'
#         And 'Lowercase' password requirement is 'inactive'

#     @desktop @mobile @regression
#     Scenario: Password requirements when a lowercase letter is introduced
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '1' lowercase letters in password input field of sign-up
#         And 'Characters long' password requirement is 'inactive'
#         And 'DigitOrSymbol' password requirement is 'inactive'
#         And 'Uppercase' password requirement is 'inactive'
#         Then 'Lowercase' password requirement is 'successful'

#     @desktop @mobile @regression
#     Scenario: Password requirements when 8 lowercase letters are introduced
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         And User types '8' lowercase letters in password input field of sign-up
#         Then 'Characters long' password requirement is 'successful'
#         And 'DigitOrSymbol' password requirement is 'inactive'
#         And 'Uppercase' password requirement is 'inactive'
#         And 'Lowercase' password requirement is 'successful'

#     @desktop @mobile @regression
#     Scenario: Password requirements when a digit is introduced
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '1' digits in password input field of sign-up
#         Then 'Characters long' password requirement is 'inactive'
#         And 'DigitOrSymbol' password requirement is 'successful'
#         And 'Uppercase' password requirement is 'inactive'
#         And 'Lowercase' password requirement is 'inactive'

#     @desktop @mobile @regression
#     Scenario: Password requirements when a symbol is introduced
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '1' symbols in password input field of sign-up
#         Then 'Characters long' password requirement is 'inactive'
#         And 'DigitOrSymbol' password requirement is 'successful'
#         And 'Uppercase' password requirement is 'inactive'
#         And 'Lowercase' password requirement is 'inactive'

#     @desktop @mobile @regression
#     Scenario: Password requirements when an uppercase letter is introduced
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '1' uppercase letters in password input field of sign-up
#         Then 'Characters long' password requirement is 'inactive'
#         And 'DigitOrSymbol' password requirement is 'inactive'
#         And 'Uppercase' password requirement is 'successful'
#         And 'Lowercase' password requirement is 'inactive'

#     @desktop @mobile @regression
#     Scenario: Password requirements when all of the are met using one digit
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '6' lowercase letters in password input field of sign-up
#         And User types '1' digits in password input field of sign-up
#         And User types '1' uppercase letters in password input field of sign-up
#         Then 'Characters long' password requirement is 'successful'
#         And 'DigitOrSymbol' password requirement is 'successful'
#         And 'Uppercase' password requirement is 'successful'
#         And 'Lowercase' password requirement is 'successful'

#     @desktop @mobile @regression
#     Scenario: Password requirements when all of the are met using one symbol
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '6' lowercase letters in password input field of sign-up
#         And User types '1' symbols in password input field of sign-up
#         And User types '1' uppercase letters in password input field of sign-up
#         Then 'Characters long' password requirement is 'successful'
#         And 'DigitOrSymbol' password requirement is 'successful'
#         And 'Uppercase' password requirement is 'successful'
#         And 'Lowercase' password requirement is 'successful'

#     @desktop @mobile @regression
#     Scenario: Password requirements when all of the are met using both
#         Given User navigates to sign-up site
#         And Password input field of sign-up is empty
#         When User types '5' lowercase letters in password input field of sign-up
#         And User types '1' digits in password input field of sign-up
#         And User types '1' symbols in password input field of sign-up
#         And User types '1' uppercase letters in password input field of sign-up
#         Then 'Characters long' password requirement is 'successful'
#         And 'DigitOrSymbol' password requirement is 'successful'
#         And 'Uppercase' password requirement is 'successful'
#         And 'Lowercase' password requirement is 'successful'
