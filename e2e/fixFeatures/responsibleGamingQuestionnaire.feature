# Feature: Responsible Gaming

# @desktop @mobile @regression
# Scenario: Access to responsible gaming questionnaire
#    Given User is logged out
#    And User navigates to Responsible Gaming site
#    Then Responsible gaming questionnarie modal is displayed

# @desktop  @regression
# Scenario: Content of question window
#     Given User is logged out
#     And User navigates to Responsible Gaming questionnaire
#     Then Responsible gaming questionnarie modal is displayed
#     And Close button in the modal is displayed
#     And Question title is displayed in Responsible Gaming questionnaire
#     And Question text is displayed in Responsible Gaming questionnaire
#     And Set of answers are displayed in Responsible Gaming questionnaire
#     @desktop  @regression
# Scenario Outline: Complete the responsable gaming questionanaire
#     Given User is logged out
#     And User navigates to Responsible Gaming questionnaire
#     When User answers all the questions with '<answer>'
#     Then Responsible gaming questionnarie modal is displayed
#     And Close button in the modal is displayed
#     And Result title is displayed in Responsible Gaming questionnaire
#     And Responsible gaming result for '<level>' level is displayed
#     And Info is displayed in Responsible Gaming questionnaire
#     Examples:
#     | answer           | level    |
#     | Sometimes        | low      |
#     | Most of the time | moderate |
#     | Almost always    | serious  |


#  @mobile @regression
# Scenario: Content of question window
#     Given User is logged out
#     And User navigates to Responsible Gaming questionnaire
#     Then Responsible gaming questionnarie modal is displayed
#     And Question title is displayed in Responsible Gaming questionnaire
#     And Question text is displayed in Responsible Gaming questionnaire
#     And Set of answers are displayed in Responsible Gaming questionnaire
#  @mobile @regression
# Scenario Outline: Complete the responsable gaming questionanaire
#     Given User is logged out
#     And User navigates to Responsible Gaming questionnaire
#     When User answers all the questions with '<answer>'
#     Then Responsible gaming questionnarie modal is displayed
#     And Result title is displayed in Responsible Gaming questionnaire
#     And Responsible gaming result for '<level>' level is displayed
#     And Info is displayed in Responsible Gaming questionnaire
#     Examples:
#     | answer           | level    |
#     | Sometimes        | low      |
#    # | Most of the time | moderate |
#     | Almost always    | serious  |