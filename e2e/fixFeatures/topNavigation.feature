# Feature: Top navigation on desktop & mobile devices

#     @desktop @regression
#     Scenario Outline: Checking all the elements in top navigation menu
#         Given a logged "<statusOfTheUser>" user in megalotto platform
#         When the user is using a desktop device
#         Then the top navigation menu is present
#         And the top navigation menu contains all the correct links
#         Examples:
#             | statusOfTheUser |
#             | in              |
#             | out             |

#     @desktop @regression
#     Scenario Outline: Clicking top navigation menu elements
#         Given a logged "<statusOfTheUser>" user in megalotto platform
#         When the user is using a desktop device
#         And the user clicks "<topNavigationOption>" on top navigation menu
#         Then the user was properly redirected to "<topNavigationOption>" page
#         Examples:
#             | statusOfTheUser | topNavigationOption |
#             | in              | lotteries           |
#             | in              | casino              |
#             | in              | scratchcards        |
#             | in              | live-casino         |
#             | in              | promo               |
#             | out             | lotteries           |
#             | out             | casino              |
#             | out             | scratchcards        |
#             | out             | live-casino         |
#             | out             | promo               |

#     @desktop @mobile
#     Scenario Outline: Notification for unread messages on top navigation
#         Given User logs in with account '<statusOfMessages>' unread messages
#         Then Notification about unread messages '<statusOfNotification>' displayed in top navigation
#         Examples:
#             | statusOfMessages | statusOfNotification |
#             | with             | is                   |
#             | without          | is not               |

# @desktop @regression @mobile
#     Scenario: Checking login button on top navigation
#         Given a logged "out" user in megalotto platform
#         When the top navigation menu is present
#         Then the top navigation contains login button
#         And  the user clicks in login button
#         And it should open a modal to sign-in
          
# @desktop @regression @mobile
#     Scenario: Checking deposit button on top navigation
#         Given a logged "in" user in megalotto platform
#         When the top navigation menu is present
#         Then the top navigation contains deposit button
       
     