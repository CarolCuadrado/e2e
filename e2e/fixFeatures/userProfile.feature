# Feature: User profile feature

# @desktop @regression
# Scenario: Fields with user information are displayed when using a desktop device
#     Given a logged "in" user in megalotto platform
#     And the user is using a desktop device
#     When the user clicks on profile overlay icon
#     And the user clicks "profile" on the profile overlay menu
#     Then user profile page is displayed
#     And the user profile contains the "lucky number"
#     And the user profile contains the "user name"
#     And the user profile contains the "user address"
#     And the user profile contains the "user mail"
#     And the user profile contains the "user phone number"

# @mobile @regression
# Scenario: Fields with user information are displayed when using a mobile device
#     Given a logged "in" user in megalotto platform
#     And the user is using a mobile device
#     When the user clicks on profile overlay icon
#     And the user clicks "profile" on the profile overlay menu
#     Then user profile page is displayed
#     And user information is collapsed
#     And the user profile contains the "lucky number"
#     And the user profile contains the "user name"
#     And the user profile does not contains the "user address"
#     And the user profile does not contains the "user mail"
#     And the user profile does not contains the "user phone number"
#     When the user clicks on the arrow to expand the details
#     Then the user profile contains the "user address"
#     And the user profile contains the "user mail"
#     And the user profile contains the "user phone number"

# @desktop @mobile @regression
# Scenario Outline: Setting lucky number for the first time
#     Given a logged in user in megalotto platform who has "not set" the lucky number previously
#     When the user accesses the profile page
#     Then the lucky number field is empty
#     When the user clicks on edit profile
#     And the user clicks on edit lucky number
#     Then the page to lucky number is displayed
#     When the user enters "<luckyNumber>" as lucky number
#     And the user clicks on Wish me luck button
#     Then the user is redirected back to account details page
#     And the new lucky number "<luckyNumber>" is set
#     Examples:
#         | luckyNumber |
#         | 12          |

# @manual @desktop @mobile 
# Scenario Outline: Trying to set a wrong lucky number
#     Given a logged in user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     When the user goes to edit lucky number page
#     And the user tries to set a number from 100 onwards
#     Then the lucky number cannot be set
#     Examples:
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @desktop @mobile @regression
# Scenario Outline: Changing lucky number
#     Given a logged in user in megalotto platform who has "set" the lucky number previously
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     And the user clicks on edit lucky number
#     Then the page to lucky number is displayed
#     When the user enters "<luckyNumber>" as lucky number
#     And the user clicks on Wish me luck button
#     And the new lucky number "<luckyNumber>" is set
#     Examples:
#         | luckyNumber |
#         | 33          |

# @desktop @mobile @regression
# Scenario: Leaving empty the lucky number
#     Given a logged in user in megalotto platform who has "set" the lucky number previously
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     And the user clicks on edit lucky number
#     Then the page to lucky number is displayed
#     When the user leaves empty the lucky number field
#     And the user clicks on Wish me luck button
#     Then the lucky number field is empty

# @manual @desktop @mobile 
# Scenario: Checking content and link redirection
#     Given a logged "in" user in megalotto platform
#     When the user goes to edit lucky number page
#     Then the lucky number page is displayed
#     When the user clicks on the arrow to go back
#     Then the user is taken back to account details page

# @manual @desktop @mobile 
# Scenario: Check the editable fields
#     Given a logged "in" user in megalotto platform
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     Then edit user profile page is displayed
#     And email and password are editable

# @desktop @mobile @regression
# Scenario: Change email properly
#     Given a logged "in" user in megalotto platform
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     Then edit user profile page is displayed
#     When the user clicks on the edit email button
#     Then edit email page is displayed
#     When the user enters a new email "validemail@gig.com"
#     And the user enters the password "validPassword"
#     And the user clicks on the save email button
#     Then the email is properly changed
#     And the user is logged out from the platform

# @manual @desktop @mobile 
# Scenario: Change email error message when entering wrong password confirmation
#     Given a logged user in Megalotto platform
#     And the user is in the editable user profile page
#     When he clicks on the edit email button
#     Then another page with the following two fields is displayed: New email and password
#     When the user enters a wrong current password
#     And a right new email
#     And clicks on the save email button
#     Then an error message "The password you entered is not correct" is shown
#     And the email is not changed

# @manual @desktop @mobile 
# Scenario: Change email error message when entering wrong new email
#     Given a logged user in Megalotto platform
#     And the user is in the editable user profile page
#     When he clicks on the edit email button
#     Then another page with the following two fields is displayed: New email and password
#     When the user enters the right current password
#     And a wrong new email
#     Then an error message "The email is not valid" is shown
#     And the Save email button is not enabled

# @manual @desktop @mobile 
# Scenario: Change email error message when entering an existing email
#     Given a logged user in Megalotto platform
#     And the user is in the editable user profile page
#     When he clicks on the edit email button
#     Then another page with the following two fields is displayed: New email and password
#     When the user enters the right current password
#     And an existing email
#     And clicks on the save email button
#     Then an error message "The email is already used" is shown

# @desktop @mobile @regression
# Scenario: Change password properly
#     Given a logged "in" user in megalotto platform
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     Then edit user profile page is displayed
#     When the user clicks on the edit password button
#     Then edit password page is displayed
#     When the user enters the "old" password "Megalotto2019!"
#     And the user enters the "new" password "Megalotto2020!"
#     And the user enters the "repeated new" password "Megalotto2020!"
#     And the user clicks on save password button
#     Then the password is properly changed
#     And the user is logged out from the platform

# @manual @desktop @mobile 
# Scenario: Change password error message when entering wrong old password
#     Given a logged user in Megalotto platform
#     And the user is in the editable user profile page
#     When he clicks on the edit password button
#     Then another page with the following three fields is displayed: Old password, New password and Repeat new password
#     When the user enters a right new password twice
#     And a wrong password in the old password field
#     And clicks on change password button
#     Then an error message "The old password you entered is not correct" is shown
#     And the password is not changed

# @desktop @mobile @regression
# Scenario: Change password error message when both entered new passwords do not match
#     Given a logged "in" user in megalotto platform
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     Then edit user profile page is displayed
#     When the user clicks on the edit password button
#     Then edit password page is displayed
#     When the user enters the "old" password "Megalotto2019!"
#     And the user enters the "new" password "Megalotto2020!"
#     And the user enters the "repeated new" password "Megalotto2021!"
#     Then the save password button is not enabled
#     And an error message "The passwords must be the same" is shown
        
# @desktop @mobile @regression
# Scenario: Change password error message when entering a new password that does not meet security requirements
#     Given a logged "in" user in megalotto platform
#     When the user accesses the profile page
#     And the user clicks on edit profile
#     Then edit user profile page is displayed
#     When the user clicks on the edit password button
#     Then edit password page is displayed
#     When the user enters the "old" password "Megalotto2019!"
#     And the user enters the "new" password "Megalotto"
#     And the user enters the "repeated new" password "Megalotto"
#     Then the save password button is not enabled
#     And an error message "Must contain min" is shown

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when less than 24 hours in user profile
#     Given a logged in user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is less than 24 hours
#     When the user checks the draw time of that lottery in the active ticket in user profile
#     Then the remaining time until draw is displayed with the format XX hours XX minutes
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when between 24 and 48 hours in user profile
#     Given a logged in user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is more than 24 hours but less than 48
#     When the user checks the draw time of that lottery in the active ticket in user profile
#     Then the remaining time until draw is displayed with the format 1 day XX hours
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |

# @manual @desktop @mobile
# Scenario Outline: Checking draw time when more than 48 hours in user profile
#     Given a logged in user in megalotto platform
#     And the user is using a <kindOfDevice> device
#     And there is one lottery whose draw time is more than 48 hours
#     When the user checks the draw time of that lottery in the active ticket in user profile
#     Then the remaining time until draw is displayed with the format XX days XX hours
#         | kindOfDevice |
#         | mobile       |
#         | desktop      |
