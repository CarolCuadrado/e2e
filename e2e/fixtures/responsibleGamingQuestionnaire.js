export const questionnaireResults = [
    {
        level: 'low',
        text:
            'You have a low level of problems with few or no identified negative consequences.',
    },
    {
        level: 'moderate',
        text:
            'You are showing signs of a moderate level of problems leading to some negative consequences.',
    },
    {
        level: 'serious',
        text:
            'You are showing signs of problem gambling with negative consequences and a possible loss of control.',
    },
];
export const questionnaireQuestions = [
    'Have you bet more than you could really afford to lose?',
    'Within the last 12 months, have you needed to gamble with larger amounts of money to get the same feeling of excitement?',
    'When you gambled, did you go back another day to try to win back the money you lost?',
    'Have you borrowed money or sold anything to get money to gamble?',
    'Have you felt that you might have a problem with gambling?',
    'Has gambling caused you any health problems, including stress or anxiety?',
    'Have people criticized your betting or told you that you had a gambling problem, regardless of whether or not you thought it was true?',
    'Has your gambling caused any financial problems for you or your household?',
    'Have you felt guilty about the way you gamble or what happens when you gamble?',
];
export const questionnaireAnswers = [
    'Never',
    'Sometimes',
    'Most of the time',
    'Almost always',
];