Feature: Page not found (404 apage)

@manual @desktop @mobile
Scenario Outline: Accesing a page that does not exist and checking content of 404 page
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user visits a URL inside megalotto domain that does not exist
    Then the 404 page is displayed with the following items: 404 icon, descriptive text, ending soon lottery ticket and explore other lotteries link
    Examples:
        | statusOfTheUser | kindOfDevice |
        | logged in       | mobile       |
        | logged in       | desktop      |
        | logged out      | mobile       |
        | logged out      | desktop      |

@manual @desktop @mobile
Scenario Outline: Clicking elements on the 404 page
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user visits a URL inside megalotto domain that does not exist
    Then the 404 page is displayed
    When the user clicks on <pageElement>
    Then the user is redirected to <redirectedOption>
    Examples:
        | statusOfTheUser | kindOfDevice | pageElement                                       | redirectedOption                                     |
        | logged in       | mobile       | play button inside the ending soon lottery ticket | purchase confirmation page for that specific lottery |
        | logged in       | desktop      | elsewhere on the ending soon ticket               | specific lottery details                             |
        | logged in       | mobile       | explore other lotteries link                      | browse lotteries page                                |
        | logged out      | desktop      | play button inside the ending soon lottery ticket | purchase confirmation page for that specific lottery |
        | logged out      | mobile       | elsewhere on the ending soon ticket               | specific lottery details                             |
        | logged out      | desktop      | explore other lotteries link                      | browse lotteries page                                |

@manual @desktop @mobile
Scenario Outline: Location and content of the ending soon lottery widget
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user accesses to 404 page
    Then the ending soon lottery widget is displayed with the following elements: Lottery image, Countdown until draw, Jackpot amount, Descriptive text and play button
    Examples:
        | statusOfTheUser | kindOfDevice |
        | logged in       | mobile       |
        | logged in       | desktop      |
        | logged out      | mobile       |
        | logged out      | desktop      |

@manual @desktop @mobile
Scenario Outline: Clicking on ending soon lottery ticket
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user accesses to 404 page
    And clicks on <element>
    Then the user is redirected to <redirectedPage>
    Examples:
        | statusOfTheUser | kindOfDevice | element                           | redirectedPage                                       |
        | logged in       | mobile       | play button in the lottery ticket | purchase confirmation page for that specific lottery |
        | logged in       | desktop      | elsewhere on the ticket           | lottery details                                      |
        | logged out      | mobile       | elsewhere on the ticket           | lottery details                                      |
        | logged out      | desktop      | play button in the lottery ticket | purchase confirmation page for that specific lottery |
