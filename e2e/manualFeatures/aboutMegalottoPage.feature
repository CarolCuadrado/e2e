Feature: About Megalotto static page

@manual @desktop @mobile
Scenario: Accessing about megalotto static page
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user clicks on about Megalotto option in <element>
    Then the about Megalotto static page is displayed
    Examples:
        | statusOfTheUser | kindOfDevice | element               |
        | logged in       | desktop      | top navigation header |
        | logged out      | desktop      | top navigation header |
        | logged in       | mobile       | hamburger menu        |
        | logged out      | mobile       | hamburger menu        |
