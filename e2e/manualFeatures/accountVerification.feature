Feature: Account Verification

@manual @desktop @mobile @regression
Scenario: Different sections in the page
    Given a user logged in the Megalotto platform who has not uploaded any document to verify his account
    When the user clicks on the user profile menu
    And clicks on settings
    And clicks on verification
    Then user account verification is shown with the following sections: Proof of ID, Proof of address and proof of payment
    And the proof of ID section has a dropdown with the following options: ID card, Passport, Driving license and Other
    And the status for the three sections is No document uploaded

@manual @desktop @mobile
Scenario: Upload a document in the Proof of ID section
    Given a user logged in the Megalotto platform who has not uploaded any document to verify his account
    When the user clicks on the user profile menu
    And clicks on settings
    And clicks on verification
    Then user account verification is shown
    And the upload button for Proof of ID section is not displayed
    When the user selects in the dropdown the kind of document to be uploaded to Proof of ID section
    Then the upload button is displayed
    When the user uploads a document for Proof of ID section
    And the document is uploaded properly
    Then a success message is displayed on the top of the page
    And the document with its uploaded date is attached to the Proof of ID section
    And the global status of the section is changed to pending verification
    And the status of the just uploaded document is pending verification
    And the upload button is disabled as the maximum number of documents allowed is one

@manual @desktop @mobile
Scenario Outline: Upload a document in the Proof of address and Proof of payment sections
    Given a user logged in the Megalotto platform who has not uploaded any document to verify his account
    And the user is in the account verification menu
    When the user uploads a document to <verificationSection> section
    And the document is uploaded properly
    Then a success message is displayed on the top of the page
    And the document with its uploaded date is attached to the <verificationSection> section
    And the global status of the section is changed to pending verification
    And the status of the just uploaded document is pending verification
    And the upload button is disabled as the maximum number of documents allowed is one
    Examples:
        | verificationSection |
        | Proof of address    |
        | Proof of payment    |

@manual @desktop @mobile
Scenario Outline: Section is verified
    Given a user logged in the Megalotto platform who has uploaded a document to verify his account to <verificationSection>
    And the document uploaded and the <verificationSection> section have been verified
    When the user goes to user account verification menu
    Then the <verificationSection> section is in verified status
    And the document uploaded to the <verificationSection> section is not shown
    And the <elementToBeDisabled> is not displayed
    Examples:
        | verificationSection | elementToBeDisabled |
        | Proof of ID         | dropdown list       |
        | Proof of address    | upload button       |
        | Proof of payment    | upload button       |

@manual @desktop @mobile
Scenario Outline: Section is rejected
    Given a user logged in the Megalotto platform who has uploaded a document to verify his account to <verificationSection>
    And the document uploaded and the <verificationSection> section have been rejected
    When the user goes to user account verification menu
    Then the <verificationSection> section is in rejected status
    And the document uploaded to the <verificationSection> section is also displayed with rejected status
    And the <elementToBeEnabled> is enabled again
    Examples:
        | verificationSection | elementToBeEnabled |
        | Proof of ID         | dropdown list      |
        | Proof of address    | upload button      |
        | Proof of payment    | upload button      |

@manual @desktop @mobile
Scenario Outline: Document upload fails
    Given a user logged in the Megalotto platform who has not uploaded any document to verify his account
    And the user is in the account verification menu
    When the user uploads a document for <verificationSection> section
    And the document upload fails
    Then an error message is displayed on the top of the page
    And the document is not attached to the <verificationSection> section
    And the <elementToBeEnabled> is enabled again
    Examples:
        | verificationSection | elementToBeEnabled |
        | Proof of ID         | dropdown list      |
        | Proof of address    | upload button      |
        | Proof of payment    | upload button      |
