Feature: Active lottery subscription and prepaid tickets on the homepage

    Active lottery subscription and prepaid tickets on the homepage

    Scenario Outline: User who has active lottery subscription tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has active subscription tickets
        When the user accesses home page
        Then active lottery tickets section is displayed
        And lottery tickets will be sorted by first upcoming draw from the left
        And the following elements are displayed inside each active susbscription ticket: Lottery icon, number of lines and two arrows icon
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |



    Scenario Outline: User who has active prepaid autoplay lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has active prepaid autoplay tickets
        When the user accesses home page
        Then active lottery tickets section is displayed
        And lottery tickets will be sorted by first upcoming draw from the left
        And the following elements are displayed inside each active prepaid autoplay ticket: Lottery icon, number of lines and two arrows icon
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |