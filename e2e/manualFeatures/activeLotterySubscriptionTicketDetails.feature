Feature: Active lottery subscription tickets details

    Active lottery subscription tickets details

    Scenario Outline: Accessing active subscription lottery ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one active subscription lottery ticket
        When the user is on the <platformSection>
        And the user clicks on <elementToClick>
        Then the lottery details are displayed as a modal with the following elements: Lottery icon and jackpot, draw date, number of lines, price per line, ticket's lines with selected numbers, autoplay icon and text, purchase date, autoplay duration, ticket price, autoplay cost, winnings, ticket Number and a button to cancel the subscription
        Examples:
            | kindOfDevice | platformSection     | elementToClick                                 |
            | mobile       | homepage            | the active subscription ticket                 |
            | desktop      | homepage            | the active subscription ticket                 |
            | mobile       | user's profile page | the active subscription ticket                 |
            | desktop      | user's profile page | the active subscription ticket                 |
            | mobile       | user's profile page | active subscription > three dots > View ticket |
