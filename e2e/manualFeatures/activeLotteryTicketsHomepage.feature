Feature: Active lottery tickets in the homepage

    Active lottery tickets in the homepage

    Scenario Outline: Accesing active lottery tickets section in the homepage
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses home page
        Then active lottery tickets section <statusOfTheSection>
        Examples:
            | statusOfTheUser | kindOfDevice | statusOfTheSection |
            | logged in       | mobile       | is displayed       |
            | logged in       | desktop      | is displayed       |
            | logged out      | mobile       | is not displayed   |
            | logged out      | desktop      | is not displayed   |

    Scenario Outline: User who does not have any active lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any active ticket
        When the user accesses home page
        Then active lottery tickets section is displayed by default
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                           |
            | mobile       | descriptive text, 0 active lines and a powerball lottery ticket |
            | desktop      | descriptive text, 0 active lines                                |

    Scenario Outline: User who has active lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user <numberOfActiveTickets> active tickets
        When the user accesses home page
        Then active lottery tickets section is displayed
        And only lottery tickets whose draw date has not taken place are shown
        And the following elements are displayed: Number of active lines and time until next draw
        When the user tries to <wayToMoveTickets> horizontally the tickets
        Then the active lottery tickets are <wayToMoveTickets>
        When the user clicks on an active ticket
        Then ticket details are shown
        Examples:
            | kindOfDevice | numberOfActiveTickets | wayToMoveTickets |
            | mobile       | 4 or more             | scroll           |
            | desktop      | 10 or more            | slide            |

    Scenario Outline: User who has two active lottery tickets for the same lottery
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has purchased two tickets for the same kind of lottery
        When the user accesses to home page
        Then active lottery tickets section is displayed
        And there are two different tickets for the lottery
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: User who has at least one active lottery subscription
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one active lottery subscription
        When the user accesses to home page
        Then active lottery tickets section is displayed
        And subscription tickets will contain the two arrows icon