Feature: Active lottery tickets in the profile page

    Active lottery tickets in the profile page

    Scenario Outline: Accesing active lottery tickets section in the profile page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses profile page
        Then active lottery tickets section <statusOfTheSection>
        Examples:
            | statusOfTheUser | kindOfDevice | statusOfTheSection                                                     |
            | logged in       | mobile       | is displayed and selected by default                                   |
            | logged in       | desktop      | is displayed and selected by default                                   |
            | logged out      | mobile       | is not displayed as profile page is only available for logged in users |
            | logged out      | desktop      | is not displayed as profile page is only available for logged in users |

    Scenario Outline: User who does not have any active lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any active ticket
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                                              |
            | mobile       | descriptive text, a powerball lottery ticket and a link to explore other lotteries |
            | desktop      | descriptive text and a link to explore other lotteries                             |

    Scenario Outline: User who has active single lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has 3 or less single active tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has not taken place are shown
        And the following elements are displayed inside each ticket: <ticketElements>
        Examples:
            | kindOfDevice | ticketOrientation | ticketElements                                                                                                                   |
            | mobile       | vertically        | Lottery icon, number of lines, time until draw, three dots on the right with two options: view ticket and autoplay these numbers |
            | desktop      | horizontally      | Lottery icon, number of lines, time until draw and an autoplay icon on the right                                                 |

    Scenario Outline: User who has active subscription lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has 3 or less active subscription tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has not taken place are shown
        And the following elements are displayed inside each ticket: <ticketElements>
        Examples:
            | kindOfDevice | ticketOrientation | ticketElements                                                                                                                                       |
            | mobile       | vertically        | Lottery icon, number of lines, time until draw, autplay text and icon, three dots on the right with two options: view ticket and cancel subscription |
            | desktop      | horizontally      | Lottery icon, number of lines, time until draw and an cancel subscription icon on the right                                                          |

    Scenario Outline: User who has active prepaid autoplay tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has 3 or less active prepaid autoplay tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has not taken place are shown
        And the following elements are displayed inside each prepaid autoplay ticket: <ticketElements>
        Examples:
            | kindOfDevice | ticketOrientation | ticketElements                                                                                                                          |
            | mobile       | vertically        | Lottery icon, number of lines, time until draw, autoplay text and icon, three dots on the right with two options: view ticket and close |
            | desktop      | horizontally      | Lottery icon, number of lines, time until draw and no icons                                                                             |

    Scenario Outline: Loading/sliding more tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has 3 or more active tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And <elementToLoadMoreTickets> is displayed
        When the user clicks <elementToLoadMoreTickets>
        Then <numberOfTickets> are displayed
        Examples:
            | kindOfDevice | elementToLoadMoreTickets          | numberOfTickets                      |
            | mobile       | a button to load more tickets     | the remaining active lottery tickets |
            | desktop      | side arrows to slide more tickets | another 3 active lottery tickets     |



    -------------------------------Suggested lottery when empty status-----------------------

    Scenario Outline: User who does not have any active lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any active ticket
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                                              |
            | mobile       | descriptive text, a powerball lottery ticket and a link to explore other lotteries |
            | desktop      | descriptive text and a link to explore other lotteries                             |


    -------------------------------Suggested lottery when empty status-----------------------

    Scenario Outline: User who has active single lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has 3 or less single active tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has not taken place are shown
        And the following elements are displayed inside each ticket: <ticketElements>
        Examples:
            | kindOfDevice | ticketOrientation | ticketElements                                                                                                                   |
            | mobile       | vertically        | Lottery icon, number of lines, time until draw, three dots on the right with two options: view ticket and autoplay these numbers |
            | desktop      | horizontally      | Lottery icon, number of lines, time until draw and an autoplay icon on the right                                                 |

    Scenario: Clicking the autoplay icon
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has 3 or less single active tickets
        When the user accesses profile page
        Then active lottery tickets section is displayed by default
        When the user clicks on the autoplay icon on a ticket
        Then the user is redirected to purchase confirmation page for that specific lottery with the same lines and numbers the active ticket has, autoplay option will be enabled with 8 weeks option selected





