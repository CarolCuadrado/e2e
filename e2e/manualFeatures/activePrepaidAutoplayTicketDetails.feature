Feature: Active prepaid autoplay ticket details

    Active prepaid autoplay ticket details


    Scenario Outline: Accessing active prepaid autoplay lottery ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one active prepaid autoplay lottery ticket
        When the user is on the <platformSection>
        And the user clicks on <elementToClick>
        Then the lottery details are displayed as a modal with the following elements: Lottery icon and jackpot, Draw date, Number of lines, Price per line, Ticket's lines with selected numbers, Autoplay icon and text, Purchase date, Autoplay duration, Ticket price, Autoplay cost, Winnings, Ticket Number and last draw in this autoplay.
        Examples:
            | kindOfDevice | platformSection     | elementToClick                                     |
            | mobile       | homepage            | the active prepaid autoplay ticket                 |
            | desktop      | homepage            | the active prepaid autoplay ticket                 |
            | mobile       | user's profile page | the active prepaid autoplay ticket                 |
            | desktop      | user's profile page | the active prepaid autoplay ticket                 |
            | mobile       | user's profile page | active prepaid autoplay > three dots > View ticket |
