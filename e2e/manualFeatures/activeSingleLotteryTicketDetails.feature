Feature: Active single lottery tickets details

    Active single lottery tickets details

    Scenario Outline: Accessing single active lottery ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one single active lottery ticket
        When the user <pageToBeAccessed>
        And the user clicks on <elementToClick>
        Then the lottery details are displayed as a modal with the following elements: Lottery icon and jackpot, Draw date, Number of lines, Price per line, Purchase date, Total price, Winnings, Ticket Number and a button to autplay these numbers
        Examples:
            | kindOfDevice | pageToBeAccessed    | elementToClick           |
            | mobile       | homepage            | the active ticket        |
            | desktop      | homepage            | the active ticket        |
            | mobile       | user's profile page | the active ticket        |
            | desktop      | user's profile page | the active ticket        |
            | mobile       | user's profile page | three dots > View ticket |


    Scenario Outline: Clicking the autoplay button from details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a active ticket on <platformSection>
        Then the active ticket details are displayed
        When the user clicks on autoplay numbers button
        Then the user is redirected to purchase confirmation page for that specific lottery with the same lines and numbers the active ticket has, autoplay option will be enabled with 8 weeks option selected
        Examples:
            | platformSection | kindOfDevice |
            | homepage        | mobile       |
            | homepage        | desktop      |
            | user profile    | mobile       |
            | user profile    | desktop      |
