# Feature: Casino lobby page

# @desktop @regression
# Scenario Outline: Accesing scratchcards page with a desktop device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     When the user clicks "scratchcards" on top navigation menu
#     Then scratchcards page on casino lobby is displayed
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @mobile @regression
# Scenario Outline: Accesing scratchcards page with a mobile device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the hamburger menu
#     And the user clicks "scratchcards" on the hamburger menu
#     Then User is on Scratchcards page on mobile
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @desktop @regression
# Scenario Outline: Accesing live casino page with a desktop device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     And the user is using a desktop device
#     When the user clicks "live-casino" on top navigation menu
#     Then live casino page on casino lobby is displayed
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @mobile @regression
# Scenario Outline: Accesing live page with a mobile device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the hamburger menu
#     And the user clicks "live-casino" on the hamburger menu
#     Then User is on live casino page on mobile
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @desktop @regression
# Scenario Outline: Accesing casino games page with a desktop device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     And the user is using a desktop device
#     When the user clicks "casino" on top navigation menu
#     Then User is on Casino page
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @mobile @regression
# Scenario Outline: Accesing casino games page with a mobile device
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     And the user is using a mobile device
#     When the user opens the hamburger menu
#     And the user clicks "casino" on the hamburger menu
#     Then User is on Casino page
#     Examples:
#         | statusOfTheUser |
#         | in              |
#         | out             |

# @desktop @regression
# Scenario Outline: Correct components on Casino lobby on desktop
#     Given a logged "<statusOfTheUser>" user in megalotto platform
#     When User navigates to Casino lobby site
#     Then Casino lobby header is displayed
#     And Games search field is displayed
#     And Link to promotions is displayed
#     And Game categories menu is displayed on desktop
#     And Last played games are '<lastPlayedGamesStatus>'
#     And Games are displayed
#     Examples:
#         | statusOfTheUser | lastPlayedGamesStatus |
#         | in              | displayed             |
#         | out             | not displayed         |

@mobile @regression
Scenario Outline: Correct components on Casino lobby on mobile
    Given a logged "<statusOfTheUser>" user in megalotto platform
    When User navigates to Casino lobby site
    Then Casino lobby header is displayed
    And Games search field is displayed
    And Link to promotions is displayed
    And Game categories menu is displayed on mobile
    And Last played games are '<lastPlayedGamesStatus>'
    And Games are displayed
    Examples:
        | statusOfTheUser | lastPlayedGamesStatus |
        | in              | displayed             |
        | out             | not displayed         |

@manual @desktop @mobile
Scenario: Game category with subcategories
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    When User selects game category in position '1'
    Then Game category in position '1' is highlighted
    And Game subcategories are 'displayed'
    And Games are displayed

@manual @desktop @mobile
Scenario: Game category without subcategories
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    When User selects game category in position '2'
    Then Game category in position '2' is highlighted
    And Game subcategories are 'not displayed'
    And Games are displayed

@manual @desktop @mobile
Scenario: Content for game subcategories
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    When User selects game category in position '1'
    Then Game subcategories are 'displayed'
    And Game subcategories have correct name
    And Game subcategories have correct amount of games

@manual @desktop
Scenario: Expand game subcategory on desktop
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    When User selects game category in position '1'
    Then Game subcategory in position '2' 'has' See all link
    And User clicks on See all link of game subcategory in position '2'
    And All the games of the game subcategory in position '2' are displayed
    And Game subcategory in position '2' 'does not have' See all link

@manual @mobile
Scenario: Scrolling on game subcategory on mobile
    Given User is logged out
    And User navigates to Casino lobby site
    And User selects game category in position '1'
    When There is a game subcategory with few games
    Then Game subcategory doesn't have See all link
    And All the games in the game category are accesible by horizontally scrolling

@manual @desktop
Scenario: Navigate to game subcategory
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    And User selects game category in position '1'
    Then Game subcategory in position '1' 'has' See all link
    And User clicks on See all link of game subcategory in position '1'
    And User is on game subcategory Live Casino site

@manual @mobile
Scenario: Navigate to game subcategory
    Given User is logged out
    And User navigates to Casino lobby site
    And User selects game category in position '1'
    When There is a game subcategory with many games
    Then Game subcategory has See all link
    And All the games in the game category are accesible by horizontally scrolling
    And User clicks on See all link
    And User is sent to game subcategory site

@manual @mobile @desktop
Scenario: Correct components on game subcategory site for logged out user
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    And User selects game category in position '1'
    When User clicks on See all link of game subcategory in position '1'
    Then Games header is displayed
    And Games search field is displayed
    And Game categories menu is displayed
    And None of the game categories is highlighted
    And Last played games are 'not displayed'
    And Games are displayed

@manual @mobile @desktop
Scenario: Search functionality on Casino lobby
    Given Expected games and categories are available
    And User is logged out
    And User navigates to Casino lobby site
    When User enters 'star' on games search input field
    Then Expected games are displayed on 'star' search result

@manual @mobile @desktop
Scenario Outline: Searching a specific game on Casino lobby
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When User navigates to Casino lobby page
    And the user uses the search box in order to search for a <kindOfGame>
    Then the game <searchResult>
    Examples:
        | statusOfTheUser | kindOfDevice | kindOfGame                                    | searchResult           |
        | logged in       | mobile       | existing game                                 | is found and displayed |
        | logged in       | desktop      | non existing game                             | is not found           |
        | logged out      | mobile       | existing game that not require authentication | is found and displayed |
        | logged out      | desktop      | existing game that require authentication     | is not found           |

@desktop
Scenario: Game cards on Scratchcard site on desktop
    Given User is logged out
    When User navigates to Scratchcard site on desktop
    Then Scratchcard games are displayed

@mobile
Scenario: Game cards on Scratchcard site on mobile
    Given User is logged out
    When User navigates to Scratchcard site on mobile
    Then Scratchcard games are displayed

@manual @desktop
Scenario: Scratchcard content on desktop
    Given User is logged out
    When User navigates to Scratchcard site on desktop
    Then Image displayed in scratchcard game in position '1' is correct
    And Provider displayed in scratchcard game in position '1' is correct
    And Title displayed in scratchcard game in position '1' is correct
    And Subtitle displayed in scratchcard game in position '1' is correct
    And Play button is displayed in scratchcard game in position '1'
    And Text of play button displayed in scratchard game in position '1' is correct

@manual @mobile
Scenario: Scratchcard content on mobile
    Given User is logged out
    When User navigates to Scratchcard site on mobile
    Then Image displayed in scratchcard game in position '1' is correct
    And Provider displayed in scratchcard game in position '1' is correct
    And Title displayed in scratchcard game in position '1' is correct
    And Subtitle displayed in scratchcard game in position '1' is correct
    And Play button is displayed in scratchcard game in position '1'
    And Text of play button displayed in scratchard game in position '1' is correct

@manual @desktop
Scenario: Game-play in scratchcard game on desktop
    Given User logs in
    When User navigates to Scratchcard site on desktop
    When User clicks on play button of game in position '1'
    Then Game is launched

@manual @desktop
Scenario: Game-play in scratchcard game on mobile
    Given User logs in
    When User navigates to Scratchcard site on mobile
    When User clicks on play button of game in position '1'
    Then Game is launched
