Feature: Contact us page

@manual @desktop @mobile
Scenario Outline: Accessing contact us page and checking content
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user clicks on <elementToBeClicked>
    Then contact us page is displayed as a <wayToBeDisplayed>
    And the contact us page have the following elements: Title and content, Link to live chat, Link to email, contact form and help box component
    Examples:
        | statusOfTheUser | kindOfDevice | elementToBeClicked              | wayToBeDisplayed |
        | logged in       | mobile       | contact us link in the footer   | normal page      |
        | logged in       | desktop      | contact us link in the footer   | modal            |
        | logged out      | mobile       | contact us link in the help box | normal page      |
        | logged out      | desktop      | contact us link in the help box | modal            |
