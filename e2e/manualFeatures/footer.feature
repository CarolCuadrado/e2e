Feature: Footer and its elements

    Footer

    Scenario Outline: Checking footer elements
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user navigates to homepage
        And the user scrolls down in order to see the footer
        Then the footer is displayed with the following elements: Switch language dropdown, Payments options (Visa, MasterCard and Trustly), Certificates section (MGA logo and 18+ logo), Megalotto description and logo, Spel inspektionen logo, Social Section(Facebook and instagram logos)
        And links to the following pages are displayed: About us, Help, Contact us, Responsible gaming, terms and conditions and privacy and policy
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Checking redirections
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user navigates to homepage
        And the user scrolls down in order to see the footer
        Then the footer is displayed
        When the user clicks on <footerElement>
        Then the user is redirected to <redirectedPage>
        Examples:
            | statusOfTheUser | kindOfDevice | footerElement             | redirectedPage                                                                                                        |
            | logged in       | mobile       | About us link             | About us page                                                                                                         |
            | logged in       | mobile       | Help link                 | Help page                                                                                                             |
            | logged in       | mobile       | Contact us link           | Contact us page                                                                                                       |
            | logged in       | mobile       | Responsible gaming link   | Responsible gaming static page                                                                                        |
            | logged in       | desktop      | Terms and conditions link | Terms and conditions page                                                                                             |
            | logged in       | desktop      | Privacy policy link       | Privacy policy page                                                                                                   |
            | logged in       | desktop      | MGA logo                  | https://www.authorisation.mga.org.mt/verification.aspx?lang=EN&company=4341ce53-7839-4b86-8dd9-e48c730ba363&details=1 |
            | logged out      | desktop      | About us link             | About us page                                                                                                         |
            | logged out      | desktop      | Help link                 | Help page                                                                                                             |
            | logged out      | desktop      | Contact us link           | Contact us page                                                                                                       |
            | logged out      | desktop      | Responsible gaming link   | Responsible gaming static page                                                                                        |
            | logged out      | mobile       | Terms and conditions link | Terms and conditions page                                                                                             |
            | logged out      | mobile       | Privacy policy link       | Privacy policy page                                                                                                   |
            | logged out      | mobile       | MGA logo                  | https://www.authorisation.mga.org.mt/verification.aspx?lang=EN&company=4341ce53-7839-4b86-8dd9-e48c730ba363&details=1 |

