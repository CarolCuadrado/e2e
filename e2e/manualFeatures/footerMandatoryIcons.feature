Feature: Footer mandatory icons

    Footer mandatory icons

    Scenario Outline: Checking footer mandatory icons
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user navigates to homepage
        And the user scrolls down in order to see the footer
        Then the footer is displayed with the following icons: Gamcare Icon, Gamblers Anonymous Icon, ODR Icon, IBAS Icon and BeGambleAware icon
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Checking redirections
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user navigates to homepage
        And the user scrolls down in order to see the footer
        Then the footer is displayed
        When the user clicks on <footerElement>
        Then the user is redirected to <redirectedPage>
        Examples:
            | statusOfTheUser | kindOfDevice | footerElement           | redirectedPage                        |
            | logged in       | mobile       | Gamcare Icon            | http://www.gamcare.org.uk             |
            | logged in       | mobile       | Gamblers Anonymous Icon | https://www.gamblersanonymous.org.uk/ |
            | logged in       | desktop      | ODR Icon                | https://ec.europa.eu/consumers/odr/   |
            | logged in       | desktop      | IBAS Icon               | http://www.ibas-uk.com/               |
            | logged in       | desktop      | BeGambleAware icon      | https://www.begambleaware.org/        |
            | logged out      | desktop      | Gamcare Icon            | http://www.gamcare.org.uk             |
            | logged out      | desktop      | Gamblers Anonymous Icon | https://www.gamblersanonymous.org.uk/ |
            | logged out      | mobile       | ODR Icon                | https://ec.europa.eu/consumers/odr/   |
            | logged out      | mobile       | IBAS Icon               | http://www.ibas-uk.com/               |
            | logged out      | mobile       | BeGambleAware icon      | https://www.begambleaware.org/        |
