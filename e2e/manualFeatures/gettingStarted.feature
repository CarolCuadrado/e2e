Feature: Getting started - Help page

    Getting started - Help page
    
    Scenario Outline: Accesing getting started page and checking content
        Given a <statusOfTheUser> user in megalotto platform
        When the user goes to help page
        And the user clicks on getting started button in help page
        Then the getting started page is displayed with the following elements: Static content and how to play steps
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |

    Scenario Outline: Horizontal scroll in mobile devices
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user goes to help page
        And the user clicks on getting started button in help page
        Then the getting started page is displayed
        And the how to play steps <itemConfiguration>
        Examples:
            | statusOfTheUser | kindOfDevice | itemConfiguration               |
            | logged in       | mobile       | are horizontally scrollable     |
            | logged in       | desktop      | are not horizontally scrollable |
            | logged out      | mobile       | are horizontally scrollable     |
            | logged out      | desktop      | are not horizontally scrollable |