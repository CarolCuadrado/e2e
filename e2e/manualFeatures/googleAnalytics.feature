Feature: Google analytics

    Google Analytics Feature

    Scenario Outline: Check google analytics messages are sent
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a desktop device
        When the user <action>
        Then a message with the action just performed is sent to google analytics
        Examples:
            | statusOfTheUser | action          |
            | logged in       | deposits money  |
            | logged in       | withdraws money |
            | logged in       | leaves a game   |
            | logged out      | logs in         |
            | logged out      | fails to login  |

    Scenario: Deposits events when user purchases a lottery ticket with a card
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user does not have sufficient balance to purchase a lottery ticket
        And the user has a card registered
        And the user is on purchase lottery modal
        When the user enters the card CVC
        And the user clicks on purchase
        Then the ticket is purchased
        And a deposit money event is sent to google analytics