Feature: Hamburger menu

    Hamburger menu in mobile devices

    Scenario: Accesing hamburger menu, displaying the elements and closing the menu
        Given a <statusOfTheUser> user in megalotto platform using a mobile device
        When the user clicks on the hamburger icon on the top navigation (header menu) icon
        Then the hamburger menu is opened with the following elements: Home option, Lotteries option, Scratch cards option, Instant wins option, Games option, Promotions option, News option, Help option, About Megalotto option, Contact us option, Switch language option, Instagram icon and Facebook icon.
        And the hamburger icon changes to X (close icon)
        When the user clicks on the X (close icon)
        Then the menu is closed
        And the X (close icon) changes again to the hamburger icon
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |

    Scenario: Hamburger menu not available for users using desktop device
        Given a <statusOfTheUser> user in megalotto platform using a desktop device
        When the user checks the top navigation header
        Then the hamburger icon is not displayed
        And the hamburger menu cannot be opened as the options are already included in the top navigation header
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |

    Scenario: Clicking elements of the hamburger menu
        Given a <statusOfTheUser> user in megalotto platform using a mobile device
        When the user clicks on the hamburger icon on the top navigation (header menu) icon
        Then the hamburger menu is displayed
        When the user clicks on <hamburgerMenuOption>
        Then the user is redirected to <redirectedOption>
        Examples:
            | statusOfTheUser | hamburgerMenuOption  | redirectedOption    |
            | logged in       | Home option          | homepage            |
            | logged in       | Lotteries option     | /lotteries URL      |
            | logged in       | Scratch cards option | /scratchcards URL   |
            | logged in       | Instant wins option  | /instantwins URL    |
            | logged in       | Games option         | /casino URL          |
            | logged in       | Promotions option    | /promotions URL     |
            | logged in       | News option          | /news URL           |
            | logged in       | Help option          | /help URL           |
            | logged in       | About megalotto      | /about URL          |
            | logged in       | Contact Us           | Live help           |
            | logged in       | Instagram icon       | Megalotto instagram |
            | logged in       | Facebook icon        | Megalotto facebook  |
            | logged out      | Home option          | homepage            |
            | logged out      | Lotteries option     | /lotteries URL      |
            | logged out      | Scratch cards option | /scratchcards URL   |
            | logged out      | Instant wins option  | /instantwins URL    |
            | logged out      | Games option         | /casino URL          |
            | logged out      | Promotions option    | /promotions URL     |
            | logged out      | News option          | /news URL           |
            | logged out      | Help option          | /help URL           |
            | logged out      | About megalotto      | /about URL          |
            | logged out      | Contact Us           | Live help           |
            | logged out      | Instagram icon       | Megalotto instagram |
            | logged out      | Facebook icon        | Megalotto facebook  |
