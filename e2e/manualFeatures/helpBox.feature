Feature: Help box component

    Help box component

    Scenario Outline: Checking the pages where this help box is present
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits the <platformSection> section
        Then the help box is shown with the following items: Descriptive text, link to help page, link to live chat and link to contact us page
        When the user clicks on <helpBoxLink> in the help box
        Then the user is redirected to <redirectedPage>
        Examples:
            | statusOfTheUser | kindOfDevice | platformSection      | helpBoxLink     | redirectedPage  |
            | logged in       | mobile       | Help                 | Help link       | Help page       |
            | logged in       | mobile       | Contact us           | Live chat link  | Live chat       |
            | logged in       | mobile       | Getting started      | Contact us link | Contact us page |
            | logged in       | mobile       | Terms and conditions | Help link       | Help page       |
            | logged in       | mobile       | Lotteries            | Live chat link  | Live chat       |
            | logged out      | mobile       | Help                 | Contact us link | Contact us page |
            | logged out      | mobile       | Getting started      | Help link       | Help page       |
            | logged out      | mobile       | Contact us           | Live chat link  | Live chat       |
            | logged out      | mobile       | Terms and conditions | Contact us link | Contact us page |
            | logged out      | mobile       | Lotteries            | Help link       | Help page       |
            | logged in       | Desktop      | Help                 | Live chat link  | Live chat       |
            | logged in       | Desktop      | Contact us           | Contact us link | Contact us page |
            | logged in       | Desktop      | Getting started      | Help link       | Help page       |
            | logged in       | Desktop      | Terms and conditions | Live chat link  | Live chat       |
            | logged in       | Desktop      | Lotteries            | Contact us link | Contact us page |
            | logged out      | Desktop      | Help                 | Help link       | Help page       |
            | logged out      | Desktop      | Getting started      | Live chat link  | Live chat       |
            | logged out      | Desktop      | Contact us           | Contact us link | Contact us page |
            | logged out      | Desktop      | Terms and conditions | Help link       | Help page       |
            | logged out      | Desktop      | Lotteries            | Live chat link  | Live chat       |