Feature: Help page

    Getting started - Help page

    Scenario Outline: Accesing help page and checking content
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on help link in the <element>
        Then the help page is displayed with the following items: Search box, getting started button, categories, options per each category (collapsed by default) and the help box
        And categories and subcategories are displayed in the same order as they are on the CMS
        When the user clicks on one collapsed option inside a category
        Then the option is expanded and information is shown
        And the url follows the structure: /faq/category#expanded-subcategory
        Examples:
            | statusOfTheUser | kindOfDevice | element        |
            | logged in       | mobile       | footer         |
            | logged in       | mobile       | hamburger menu |
            | logged in       | desktop      | help box       |
            | logged in       | desktop      | footer         |
            | logged out      | mobile       | footer         |
            | logged out      | mobile       | hamburger menu |
            | logged out      | desktop      | help box       |
            | logged out      | desktop      | footer         |

    Scenario Outline: Horizontal scroll on mobile devices
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a mobile device
        When the user accesses to help page
        And the user tries to scroll horizontally the categories
        Then the caregories are scrolled
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |

    Scenario Outline: Search for a specific topic
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses help page
        And the user searches for a existing category
        Then the category is displayed
        When the user searches for an unexisting category
        Then no results message is displayed
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Clicking the getting started link
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses help page
        And the user clicks on getting started button
        Then the user is redirected to getting started page
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |