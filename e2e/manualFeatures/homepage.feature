Feature: Homepage

@manual @desktop @mobile
Scenario Outline: Location and content of the drawing soon lottery widget
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user accesses to homepage
    Then the biggest jackpots section is displayed above drawing soon section
    And there is <numberOfLotteries> displayed on this section
    And there are two rows at both sides of the tickets
    And the lotteries are sorted by jackpot using a descending order
    When the user click the right arrow
    Then another <numberOfLotteries> are loaded
    Examples:
        | statusOfTheUser | kindOfDevice | numberOfLotteries |
        | logged in       | mobile       | 2                 |
        | logged in       | desktop      | 5                 |
        | logged out      | mobile       | 2                 |
        | logged out      | desktop      | 5                 |

@manual @desktop @mobile
Scenario Outline: Location and content of the drawing soon lottery widget
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user accesses to homepage
    Then the ending soon lottery widget is displayed with the following elements: Lottery image, Countdown until draw, Jackpot amount, Descriptive text and play button
    Examples:
        | statusOfTheUser | kindOfDevice |
        | logged in       | mobile       |
        | logged in       | desktop      |
        | logged out      | mobile       |
        | logged out      | desktop      |

@manual @desktop @mobile
Scenario Outline: Clicking on drawing soon lottery ticket
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    When the user accesses to homepage
    And clicks on <element>
    Then the user is redirected to <redirectedPage>
    Examples:
        | statusOfTheUser | kindOfDevice | element                           | redirectedPage                                       |
        | logged in       | mobile       | play button in the lottery ticket | purchase confirmation page for that specific lottery |
        | logged in       | desktop      | elsewhere on the ticket           | lottery details                                      |
        | logged out      | mobile       | elsewhere on the ticket           | lottery details                                      |
        | logged out      | desktop      | play button in the lottery ticket | purchase confirmation page for that specific lottery |

@manual @desktop @mobile
Scenario Outline: Change of drawing soon lottery ticket
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    And the user is visiting a page where the drawing soon widget is displayed
    When the countdown reaches zero
    Then the lottery ticket disappears and the next <nextTicketPosition> ticket takes the place
    And a new ticket on the <newTicketPosition> after the rest of the tickets appears
    Examples:
        | statusOfTheUser | kindOfDevice | nextTicketPosition | newTicketPosition |
        | logged in       | mobile       | below              | bottom            |
        | logged in       | desktop      | right              | right             |
        | logged out      | mobile       | below              | bottom            |
        | logged out      | desktop      | right              | right             |

@manual @desktop @mobile
Scenario Outline: Loading more tickets on desktop
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a desktop device
    When the user accesses to homepage
    Then and clicks on the side arrow to load more tickets
    Then another three new ticktes are displayed
    Examples:
        | statusOfTheUser |
        | logged in       |
        | logged out      |

@manual @desktop @manual
Scenario Outline: Checking draw time when less than 24 hours in homepage
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    And there is one lottery whose draw time is less than 24 hours
    When the user checks the draw time of that lottery in <componentToCheck>
    Then the remaining time until draw is displayed with the format XX hours XX minutes
    Examples:
        | statusOfTheUser | kindOfDevice | componentToCheck                                     |
        | logged in       | desktop      | drawing soon section                                 |
        | logged in       | mobile       | recently played section                              |
        | logged in       | desktop      | recently played section                              |
        | logged in       | mobile       | the homepage the remaining time until user next draw |
        | logged in       | desktop      | the homepage the remaining time until user next draw |
        | logged out      | mobile       | drawing soon section                                 |

@manual @desktop @mobile
Scenario Outline: Checking draw time when between 24 and 48 hours in homepage
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    And there is one lottery whose draw time is more than 24 hours but less than 48
    When the user checks the draw time of that lottery in <componentToCheck>
    Then the remaining time until draw is displayed with the format 1 day XX hours
    Examples:
        | statusOfTheUser | kindOfDevice | componentToCheck                                     |
        | logged in       | desktop      | drawing soon section                                 |
        | logged in       | mobile       | recently played section                              |
        | logged in       | desktop      | recently played section                              |
        | logged in       | mobile       | the homepage the remaining time until user next draw |
        | logged in       | desktop      | the homepage the remaining time until user next draw |
        | logged out      | mobile       | drawing soon section                                 |

@manual @desktop @mobile
Scenario Outline: Checking draw time when more than 48 hours in homepage
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    And there is one lottery whose draw time is more than 48 hours
    When the user checks the draw time of that lottery in <componentToCheck>
    Then the remaining time until draw is displayed with the format XX days XX hours
    Examples:
        | statusOfTheUser | kindOfDevice | componentToCheck                                     |
        | logged in       | desktop      | drawing soon section                                 |
        | logged in       | mobile       | recently played section                              |
        | logged in       | desktop      | recently played section                              |
        | logged in       | mobile       | the homepage the remaining time until user next draw |
        | logged in       | desktop      | the homepage the remaining time until user next draw |
        | logged out      | mobile       | drawing soon section                                 |

@manual @desktop @mobile
Scenario: Change of ending soon lottery ticket
    Given a <statusOfTheUser> user in megalotto platform
    And the user is using a <kindOfDevice> device
    And the user accesses to homepage
    When the countdown reaches zero
    Then the lottery ticket disappears and another ending soon lottery ticket appears
