Feature: How to play in lottery detail page

    How to play in lottery detail page

    Scenario Outline: Checking how to play component
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is in lotteries page
        When the user clicks on a specific lottery which has <numberOfSteps>
        Then the user is redirected to lottery details page
        And steps in how to play component are <statusOfTheComponent>
        Examples:
            | statusOfTheUser | kindOfDevice | numberOfSteps     | statusOfTheComponent |
            | logged in       | mobile       | more than 3 steps | scrollable           |
            | logged in       | desktop      | more than 3 steps | slid                 |
            | logged in       | mobile       | 0 steps           | not shown            |
            | logged out      | mobile       | only 1 step       | not scrollable       |
            | logged out      | desktop      | 2 steps           | not scrollable       |
            | logged out      | desktop      | 0 steps           | not shown            |
