Feature: Accumulated jackpots and Active Tickets update on purchase

    Accumulated jackpots and Active Tickets update on purchase

    Scenario Outline: Purchase lottery ticket successfully
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is purchasing a <kindOfLotteryTicket> lottery ticket
        When the user clicks on purchase button
        Then the ticket is purchased successfully
        And the purchase modal is closed
        And the jackpot amount is updated
        And the active ticket section is updated including now the just purchased ticket/s
        Examples:
            | kindOfDevice | kindOfLotteryTicket |
            | mobile       | single              |
            | desktop      | prepaid autoplay    |
            | mobile       | prepaid autoplay    |
            | desktop      | single              |

    Scenario Outline: Purchase lottery ticket unsuccessfully
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is purchasing a <kindOfLotteryTicket> lottery ticket
        When the user clicks on purchase button
        Then the purchase fails
        And the lottery ticket is not purchased
        And the purchase modal is not closed
        And the jackpot amount is not updated
        And the active ticket section is not updated
        Examples:
            | kindOfDevice | kindOfLotteryTicket |
            | mobile       | single              |
            | desktop      | prepaid autoplay    |
            | mobile       | prepaid autoplay    |
            | desktop      | single              |

    Scenario Outline: Abort lottery purchase
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is purchasing a <kindOfLotteryTicket> lottery ticket
        When the user closes the purchase modal
        Then the lottery ticket is not purchased
        And the purchase modal is closed
        And the jackpot amount is not updated
        And the active ticket section is not updated
        Examples:
            | kindOfDevice | kindOfLotteryTicket |
            | mobile       | single              |
            | desktop      | prepaid autoplay    |
            | mobile       | prepaid autoplay    |
            | desktop      | single              |