Feature: Landing page

@manual @mobile @desktop
Scenario Outline: Access to landing page for logged in user
    Given logged in user in megalotto platform
    And user is using a <kindOfDevice> device
    When user navigates to landing page
    Then user is redirected to 404 page
    Examples:
        | kindOfDevice |
        | mobile       |
        | desktop      |

@manual @mobile @desktop
Scenario: Landing page content
    Given logged out user in megalotto platform
    And user is using a <kindOfDevice> device
    When user navigates to landing page
    Then landing page is displayed
    And hamburger menu icon is not displayed
    And navigation options are not displayed
    And landing page banner is displayed
    And landing page help steps are displayed
    And landing page description is displayed
    And landing page second CTA is displayed
    And landing page TnC is displayed
    And secure payment methods are displayed
    And responsible gaming section is not displayed
    Examples:
        | kindOfDevice |
        | mobile       |
        | desktop      |

@manual @mobile @desktop
Scenario: Landing page banner
    Given logged out user in megalotto platform
    And user is using a <kindOfDevice> device
    When user navigates to landing page
    Then landing page banner is displayed
    And title of landing page banner is displayed
    And description of landing page banner is displayed
    And image of landing page banner is displayed
    And first CTA of landing page banner is displayed
    Examples:
        | kindOfDevice |
        | mobile       |
        | desktop      |
