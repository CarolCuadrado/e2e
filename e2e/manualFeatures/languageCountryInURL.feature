Feature: ISO language-country code in URL

    ISO language-country code in URL

    Scenario Outline: Accessing the platform from differents locations
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits megalotto from <location>
        Then megalotto is displayed in <language> language
        And the language on the URL is <urlCode>
        Examples:
            | kindOfDevice | country     | language  | urlCode |
            | mobile       | New Zealand | English   | en-nz   |
            | desktop      | Canada      | English   | en-ca   |
            | mobile       | EU Other    | English   | en      |
            | desktop      | Germany     | German    | de-de   |
            | mobile       | Austria     | German    | de-at   |
            | desktop      | Finland     | Finnish   | fi-fi   |
            | mobile       | Norway      | Norwegian | nb-no   |

    Scenario Outline: Changing country through country selector on the footer
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits megalotto
        And selects <country> on the country selector on the footer
        Then megalotto is displayed in <language> language
        And the language on the URL is <urlCode>
        Examples:
            | kindOfDevice | country     | language  | urlCode |
            | mobile       | New Zealand | English   | en-nz   |
            | desktop      | Canada      | English   | en-ca   |
            | mobile       | EU Other    | English   | en      |
            | desktop      | Germany     | German    | de-de   |
            | mobile       | Austria     | German    | de-at   |
            | desktop      | Finland     | Finnish   | fi-fi   |
            | mobile       | Norway      | Norwegian | nb-no   |

    Scenario Outline: Changing country through country selector on the hamburger menu
        Given a logged out user in megalotto platform
        And the user is using a mobile device
        When the user visits megalotto
        And selects <country> on the country selector on the hamburger menu
        Then megalotto is displayed in <language> language
        And the language on the URL is <urlCode>
        Examples:
            | country     | language  | urlCode |
            | New Zealand | English   | en-nz   |
            | Canada      | English   | en-ca   |
            | EU Other    | English   | en      |
            | Germany     | German    | de-de   |
            | Austria     | German    | de-at   |
            | Finland     | Finnish   | fi-fi   |
            | Norway      | Norwegian | nb-no   |

    Scenario Outline: Checking is not possible to change language nor region once the user is logged in
        Given a logged in user in megalotto platform
        And the account the user is using an account from <country>
        And the user is using a <kindOfDevice> device
        When the user tries to change the country using <locationOfRegionSelector>
        Then the user is not able to change the country as the region selector is not available
        When the user changes the language/country through the url to a different value
        Then the site is reloaded and show again with the <urlCode> in the URL
        Examples:
            | country     | kindOfDevice | locationOfRegionSelector | urlCode |
            | New Zealand | mobile       | hamburger menu           | en-nz   |
            | Canada      | desktop      | footer                   | en-ca   |
            | EU Other    | mobile       | hamburger menu           | en      |
            | Germany     | desktop      | footer                   | de-de   |
            | Austria     | mobile       | hamburger menu           | de-at   |
            | Finland     | desktop      | footer                   | fi-fi   |
            | Norway      | mobile       | footer                   | nb-no   |

    Scenario Outline: Language remains the same after logs in with an account from another country
        Given a logged in user in megalotto platform
        And the account the user is using an account from <accountCountry>
        And the user is using a <kindOfDevice> device
        And the user is accessing from <countryOfAccess>
        When the user logs out
        Then the site is shown in <language> and with <urlCode> in the url
        Examples:
            | accountCountry | kindOfDevice | countryOfAccess | language  | urlCode |
            | New Zealand    | mobile       | Norway          | English   | en-nz   |
            | Canada         | desktop      | Finland         | English   | en-ca   |
            | EU Other       | mobile       | Austria         | English   | en      |
            | Germany        | desktop      | New Zealand     | German    | de-de   |
            | Austria        | mobile       | Canada          | German    | de-at   |
            | Finland        | desktop      | EU Other        | Finnish   | fi-fi   |
            | Norway         | mobile       | Germany         | Norwegian | nb-no   |