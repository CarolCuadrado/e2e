Feature: Last played casino games feature

    Last played casino games

    Scenario Outline: Accessing last played casino games feature
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits casino lobby
        Then the last played games section <sectionStatus>
        Examples:
            | statusOfTheUser | kindOfDevice | sectionStatus    |
            | logged in       | desktop      | is displayed     |
            | logged in       | mobile       | is displayed     |
            | logged out      | desktop      | is not displayed |
            | logged out      | mobile       | is not displayed |

    Scenario Outline: Checking the number of last played games shown in casino lobby
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has played at least one casino game before
        When the user visits casino lobby
        Then last played games section is displayed with room up to <numberOfLastGamesPlayedInTheLobby> games
        When the user clicks on a game in the last played games section
        Then the game is open
        Examples:
            | kindOfDevice | numberOfLastGamesPlayed |
            | desktop      | 9                       |
            | mobile       | 4                       |

    Scenario Outline: Checking the number of last played games shown in gameplay
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits casino lobby
        And clicks on a game
        Then gameplay section is displayed
        And the last played games section <sectionStatus>
        Examples:
            | kindOfDevice | sectionStatus                                    |
            | desktop      | is displayed with room up to 6 last played games |
            | mobile       | is not displayed                                 |

