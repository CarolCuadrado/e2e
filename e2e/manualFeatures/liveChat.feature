Feature: Live chat

    Live chat

    Scenario: Trying to access old wand live chat
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks try to access directly the url /live-chat
        Then the user is redirected to 404 page
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |