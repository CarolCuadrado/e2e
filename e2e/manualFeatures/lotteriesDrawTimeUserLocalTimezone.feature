Feature: Lotteries draw time in user local timezone

    Lotteries draw time in user local timezone

    Scenario Outline: Check lottery draw time is displayed in user local timezone
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is in a <timezone> timezone
        When the user opens <placeToCheck>
        Then lottery draw date and time are displayed in user local time
        Examples:
            | statusOfTheUser | kindOfDevice | timezone | placeToCheck                     |
            | logged in       | mobile       | UTC-5    | lottery details page             |
            | logged in       | desktop      | UTC+3    | the purchase modal for a lottery |
            | logged out      | mobile       | UTC-10   | the purchase modal for a lottery |
            | logged out      | desktop      | UTC      | lottery details page             |