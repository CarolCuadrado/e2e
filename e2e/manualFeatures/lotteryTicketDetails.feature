Feature: Lottery ticket details

    Lottery ticket details

    Scenario: Lottery ticket details using desktop browser
        Given a logged in user in Megalotto platform using a desktop browser
        When the user clicks on lotteries option in the navigation bar
        Then lotteries page is displayed
        When the user clicks on a lottery ticket in the page
        Then the clicked lottery ticket details page is displayed
        And the following information is shown: Lottery logo, Jackpot amount, time until draw, play now button, price per ticket
        And there will be two tabs in the page: Information (selected by default) and Results
        And the results tab contains the following information: Draw date, how to play (steps) and odds and prizes table
        And odds and prizes tables contains the following information: Hits column, odds column and prize column
        When the user clicks on the play button
        Then the user is redirected to buy lottery ticket page

    Scenario: Lottery ticket details using mobile device
        Given a logged in user in Megalotto platform using a mobile device
        When the user clicks on the hamburger menu
        And in lotteries option
        Then lotteries page is displayed
        When the user clicks on a lottery ticket in the page
        Then the clicked lottery ticket details page is displayed
        And the following information is shown: Lottery logo, Jackpot amount, time until draw, play now button, price per ticket
        And there will be two tabs in the page: Information (selected by default) and Results
        And the results tab will contain the following information: Draw date, how to play (steps which can be scrolled horizontally) and odds and prizes table section (collapsed)
        When the user clicks on odds and prizes section
        Then odds and prizes table is expanded
        And odds and prizes tables contains the following information: Hits column, odds column and prize column
        When the user clicks on the play button
        Then the user is redirected to buy lottery ticket page