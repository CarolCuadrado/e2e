Feature: Lottery titles on homepage

    Lottery titles on homepage

    Scenario Outline: Lottery titles on homepage
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        And the user checks <lotterySection>
        Then lottery titles are displayed inside each lottery component
        Examples:
            | statusOfTheUser | kindOfDevice | lotterySection              |
            | logged in       | mobile       | drawing soon section        |
            | logged in       | desktop      | recently played section     |
            | logged in       | desktop      | suggested lottery component |
            | logged out      | mobile       | ending soon widget          |
            | logged out      | desktop      | drawing soon section        |

    Scenario Outline: Lottery titles on user profile
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses user profile
        And the user checks <lotterySection>
        Then lottery titles are displayed inside each lottery component
        Examples:
            | statusOfTheUser | kindOfDevice | lotterySection              |
            | logged in       | mobile       | active, past and winning    |
            | logged in       | desktop      | active, past and winning    |
            | logged in       | mobile       | suggested lottery component |
            | logged in       | desktop      | suggested lottery component |
