Feature: Maintenance mode

    Maintenance mode

    Scenario Outline: Check games are not playable nor clickable when maintenance mode is on
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there is a maintenance mode configured and enabled for the game provider "netEnt"
        When the user goes to Megalotto platform
        And the user goes to the games lobby
        Then games whose game provider is "netEnt" are displayed in disabled mode
        And the games are not clickable
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Check games are automatically enabled when the schedule on maintenance mode is over
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there is a maintenance mode configured and enabled for the game provider "netEnt"
        When the schedule configured for the game provider "netEnt" is over
        Then games for that game provider are automatically enabled
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |