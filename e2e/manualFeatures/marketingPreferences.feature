Feature: Marketing Preferences

    Marketing Preferences Feature

    Scenario: Accessing and changing marketing preferences menu
        Given a logged in user in Megalotto platform
        When the user clicks on the user profile menu 
        And clicks on settings
        And clicks on marketing preferences
        Then user marketing preferences are shown: Email, Text Message, Telephone, Direct Mail and Personalized Marketing
        When the user clicks on the toggle button to activate Email option
        And the user clicks on the toogle button to deactivate Telephone option
        Then marketing preferences are saved properly
        When the user goes to another menu
        And comes back to marketing preferences menu
        Then marketing preferences are shown in the status that the user set before