Feature: Mega number

    Accumulated lottery jackpots in homepage


    Scenario: Mega number for a user who has active lottery tickets
        Given a logged in user in Megalotto platform who has at least one active lottery ticket
        When the user accesses to the homepage
        Then the user can see the mega number below the navigation bar, displaying the total sum of al the lotteries the player has a stake in

    Scenario: Mega number for a user who does not have any active lottery ticket
        Given a logged in user in Megalotto platform who does not have any active lottery ticket
        When the user accesses to the homepage
        Then the user will see zero as the mega number below the navigation bar

    Scenario: Mega number tooltip
        Given a logged in user in Megalotto platform
        When the user accesses to the homepage
        Then the mega number and a help icon are displayed below the navigation bar
        When the user clicks on the help icon
        Then a modal window with a descriptive text and a learn more link is displayed
        When the user clicks on the learn more link
        Then the user is redirected to Megalotto help section


    ---------------------------ML-332-------------------------------

    Scenario Outline: Mega number format
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the player's registered currency is <playerCurrency>
        When the user accesses the homepage
        Then the mega number is <megaNumber>
        And the number of decimals of the mega number is <megaNumberDecimals>
        Examples:
            | kindOfDevice | playerCurrency | megaNumber                | megaNumberDecimals |
            | mobile       | EUR            | double digits in millions | one decimal        |
            | desktop      | CAD            | three digits in millions  | zero decimals      |
            | mobile       | NOK            | single digit in billions  | one decimal        |
            | desktop      | USD            | double digits in billions | one decimal        |
            | mobile       | SEK            | less than a million       | zero decimals      |
            | desktop      | EUR            | zero                      | zero decimals      |
