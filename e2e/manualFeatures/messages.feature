Feature: Messages

    Messages feature

    Scenario Outline: Checking empty inbox
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has not received any messages
        When the user opens the profile overlay menu
        And the user clicks on inbox option
        Then messages inbox is displayed with the follwing elements: title, <elements>, descriptive text and a button to go to games
        When the user clicks on the go to games button
        Then the user is redirected to casino games lobby
        Examples:
            | kindOfDevice | elements                  |
            | mobile       | arrow to go back          |
            | desktop      | button to close the inbox |

    Scenario: Checking inbox with messages in mobile devices
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        And the user has received at least one message
        When the user opens the profile overlay menu
        And the user clicks on inbox option
        Then messages inbox is displayed with the following elements: title and an arrow to go back
        And the messages are displayed in descending chronological order
        And each message item contains the following elements: icon, title of the message and the reception date
        When the user clicks on any message
        Then the message is open and contains the following items: arrow to go back to messages, title, reception date, body of the message, button and terms and conditions collapsed
        When the user clicks on terms and conditions
        Then terms and conditions are expanded
        When the user clicks on the arrow to go back
        Then the user is redirected to messages inbox

    Scenario: Checking inbox with messages in desktop devices
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has received at least one message
        When the user opens the profile overlay menu
        And the user clicks on inbox option
        Then messages inbox is displayed with the following elements: title and button to close the inbox
        And the messages are displayed in descending chronological order
        And each message item contains the following elements: icon, title of the message, the reception date and a button to read the message
        When the user clicks on any read button
        Then the message is open and contains the following items: title, reception date, body of the message, button, terms and conditions collapsed and a button to go back to messages
        When the user clicks on terms and conditions
        Then terms and conditions are expanded
        When the user clicks on go back button
        Then the user is redirected to messages inbox


    Scenario Outline: Messages will be kept for 48 hours
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has received at least one message in the last 48 hours
        And the user has opened one of the messages received in the last 48 hours
        When the user accesses messages inbox
        And the open message is still present
        When the user checks the messages inbox after the remaining time until 48 hours after the reception date of the message
        Then the message is no longer present
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Closed messages will not be deleted after 48 hours
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has received at least one message in the last 48 hours
        And the user has not opened any of the messages received in the last 48 hours
        When the user hecks the messages inbox after 48 hours
        And the closed message is still present
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |