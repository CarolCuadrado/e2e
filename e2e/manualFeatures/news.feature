Feature: News

    News feature for logged in and out users

    Scenario Outline: Accessing news section
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are more than 6 news configured on the CMS
        When the user clicks on news option in the <elementToClick>
        Then news section is displayed
        And there is <numberOfNewsPerRow> new/s per row
        And there is three rows of news
        And each new contains: image, title and description of the new
        And there is a load more button at the bottom of the page
        Examples:
            | statusOfTheUser | kindOfDevice | elementToClick | numberOfNewsPerRow |
            | logged in       | mobile       | hamburger menu | one                |
            | logged in       | desktop      | top navigation | two                |
            | logged out      | mobile       | hamburger menu | one                |
            | logged out      | desktop      | top navigation | two                |

    Scenario Outline: Loading more news and checking sorting
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are more than 6 news configured on the cms
        When the user accesses the news section
        Then news section is displayed
        And news are sorted using a descending order based on the field position on the CMS
        When the user clicks on the load more button
        Then another 3 rows of news are displayed
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Clicking news
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on news section
        When the user clicks the image of a new
        Then the user is redirected to the URL configured in the CMS
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |