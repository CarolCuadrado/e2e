Feature: Opt in offers

    Opt in offers

    Scenario Outline: Opt in offers for logged in users
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses a opt in offer
        Then the opt in button configured as primary is displayed
        And the secondary button is not displayed
        When the user clicks on the opt in button
        Then a notification appears "You have successfully opted in"
        And the roles are added to the user
        And the opt in button is no longer visible
        And the secondary button appears
        When the user clicks on the secondary button
        Then the user is redirected to the URL configured in that button
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Opt in offers for logged out users
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses a opt in offer
        Then the opt in button configured as primary is displayed
        And the secondary button is not displayed
        When the user clicks on the opt in button
        Then the login modal is open
        When the user logs in
        Then the user is redirected back to the opt in offer
        When the user clicks again on the opt in button
        Then a notification appears "You have successfully opted in"
        And the roles are added to the user
        And the opt in button is no longer visible
        And the secondary button appears
        When the user clicks on the secondary button
        Then the user is redirected to the URL configured in that button
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Check that normals offers remain the same
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses a normal offer
        Then the primary button is displayed
        When the user clicks on the button
        Then the user is redirected the the URL configured in that button
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged out      | desktop      |
            | logged in       | desktop      |
            | logged out      | mobile       |

    Scenario Outline: User who has already opted in an offer
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has already opted in the offer
        When the user accesses the opt in offer
        Then the opt in button configured as primary is displayed
        And the secondary button is not displayed
        When the user clicks on the opt in button
        Then a notification appears "You have already opted in this offer"
        And the opt in button is no longer visible
        And the secondary button appears
        When the user clicks on the secondary button
        Then the user is redirected to the URL configured in that button
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |