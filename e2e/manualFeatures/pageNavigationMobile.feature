Feature: Page navigation for mobile devices

    Page navigation for mobile devices


    Scenario Outline: Checking the pages where this page navigation is present
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user visits the <platformSection> section
        Then the page navigation below the top navigation header <statusOfPageNavigation>
        Examples:
            | kindOfDevice | platformSection                   | statusOfPageNavigation                                                      |
            | mobile       | Settings and its internal options | is displayed showing the title of the section and an arrow to navigate back |
            | mobile       | Messages                          | is displayed showing the title of the section and an arrow to navigate back |
            | mobile       | Wallet and its internal options   | is displayed showing the title of the section and an arrow to navigate back |
            | mobile       | Profile                           | is displayed showing the title of the section and an arrow to navigate back |
            | desktop      | Settings and its internal options | is not displayed                                                            |
            | desktop      | Messages                          | is not displayed                                                            |
            | desktop      | Wallet and its internal options   | is not displayed                                                            |
            | desktop      | Profile                           | is not displayed                                                            |

    Scenario Outline: Clicking on the back arrow and checking the user is redirected properly
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        When the user visits the <platformSection> section
        Then the page navigation with the section title and an arrow to navigate back is displayed
        When the user clicks on the arrow
        Then the user is redirected to the previous page he was
        Examples:
            | platformSection                   |
            | Settings and its internal options |
            | Messages                          |
            | Wallet and its internal options   |
            | Profile                           |