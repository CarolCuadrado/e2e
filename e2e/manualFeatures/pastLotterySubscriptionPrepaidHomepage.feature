Feature: Past lottery subscription and prepaid tickets on the homepage

    Past lottery subscription and prepaid tickets on the homepage

    Scenario Outline: User who has unchecked past lottery subscription tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has unchecked past subscription tickets
        When the user accesses home page
        Then active lottery tickets section is displayed
        When the user clicks on past tab
        Then unchecked past lottery subscription tickets are displayed
        And the following elements are displayed inside each unchecked past susbscription ticket: Lottery icon, number of lines and two arrows icon
        When the user clicks on one unchecked past subscription ticket
        Then the ticket dissapears from this section
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: User who has past prepaid autoplay lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has unchecked past prepaid autoplay lottery tickets
        When the user accesses home page
        Then active lottery tickets section is displayed
        When the user clicks on past tab
        Then unchecked past prepaid autoplay lottery tickets are displayed
        And the following elements are displayed inside each unchecked past prepaid autoplay ticket: Lottery icon, number of lines and two arrows icon
        When the user clicks on one unchecked past prepaid autoplay ticket
        Then the ticket dissapears from this section
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |