Feature: Past lottery subscription and prepaid tickets on the user profile

    Past lottery subscription and prepaid tickets on the user profile

    Scenario Outline: User who has past subscription lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning subscription ticket and one no winning subscription ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the following elements are displayed inside each winning subscription ticket: <winningSubscriptionTicketElements>
        And the following elements are displayed inside each no winning subscription ticket: <noWinningSubscriptionTicketElements>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketElements                                                                   | noWinningTicketElements                                                               |
            | mobile       | vertically        | Golden styles, lottery icon, draw date, amount of money won and three dots on the right | Lottery icon, draw date, number of lines, two arrows icon and three dots on the right |
            | desktop      | horizontally      | Lottery icon, draw date and amount of money won                                         | Lottery icon, draw date, two arrows icon and number of lines                          |

    Scenario Outline: User who has past prepaid autoplay lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning prepaid autoplay ticket and one no winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the following elements are displayed inside each winning prepaid autoplay ticket: <winningSubscriptionTicketElements>
        And the following elements are displayed inside each no winning prepaid autoplay ticket: <noWinningSubscriptionTicketElements>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketElements                                                                                   | noWinningTicketElements                                                               |
            | mobile       | vertically        | Golden styles, lottery icon, draw date,two arrows icon, amount of money won and three dots on the right | Lottery icon, draw date, number of lines, two arrows icon and three dots on the right |
            | desktop      | horizontally      | Lottery icon, draw date, two arrows icon and amount of money won                                        | Lottery icon, draw date, two arrows icon and number of lines                          |


    Scenario Outline: Options for past prepaid autoplay tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning ticket and one no winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the winning prepaid autoplay tickets have the following options: <winningTicketOptions>
        And the no winning prepaid autoplay tickets have the following options: <noWinningTicketOptions>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketOptions                                                         | noWinningTicketOptions                                                      |
            | mobile       | vertically        | Three golden dots with the following options: View ticket and renew autoplay | Three black dots with the following options: View ticket and renew autoplay |
            | desktop      | horizontally      | One golden icon to renew autoplay                                            | One standard iconto renew autoplay                                          |

    Scenario Outline: Clicking the mobile options for past prepaid autoplay tickets
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        And the user has at least one winning ticket and one no winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        When the user clicks on the three dots of a <kindOfPastPrepaidAutoplayTicket> ticket
        Then the modal with the ticket options is displayed
        When the user clicks on <ticketOption> option
        Then <resultOfClickedOption>
        Examples:
            | kindOfPastPrepaidAutoplayTicket | ticketOption   | resultOfClickedOption                                                                         |
            | winning                         | View ticket    | past prepaid autoplay ticket details are displayed                                            |
            | winning                         | Renew autoplay | purchase modal is open with the same numbers, lines and autoplay duration the past ticket has |
            | no winning                      | View ticket    | past prepaid autoplay ticket details are displayed                                            |
            | no winning                      | Renew autoplay | purchase modal is open with the same numbers, lines and autoplay duration the past ticket has |

    Scenario Outline: Clicking the desktop options for past prepaid autoplay tickets
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has at least one winning ticket and one no winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        When the user clicks on <ticketOption> option of a <kindOfPastPrepaidAutoplayTicket> ticket
        Then <resultOfClickedOption>
        Examples:
            | ticketOption   | kindOfPastPrepaidAutoplayTicket | resultOfClickedOption                                                                         |
            | Renew autoplay | winning                         | purchase modal is open with the same numbers, lines and autoplay duration the past ticket has |
            | Renew autoplay | no winning                      | purchase modal is open with the same numbers, lines and autoplay duration the past ticket has |
