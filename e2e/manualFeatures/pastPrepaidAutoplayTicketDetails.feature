Feature: Past prepaid autoplay ticket details

    Past prepaid autoplay ticket details

    Scenario Outline: Accessing no winning prepaid autoplay ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a no winning ticket on <platformSection>
        Then a modal with the following elements is displayed: Lottery icon, Results text, Draw date and winning numbers
        And the following information will be also displayed above the lines: Lottery icon and jackpot, Draw date, number of lines and price per line
        And ticket's lines are displayed, winning numbers will be highlighted and no winning numbers will be disabled
        And the following information will be also displayed below the lines: Autoplay icon and text, winning numbers, purchase date, autoplay duration, ticket price, autoplay cost, winnings, ticket number, button to renew autoplay.
        Examples:
            | kindOfDevice | platformSection |
            | mobile       | homepage        |
            | desktop      | homepage        |
            | mobile       | user profile    |
            | desktop      | user profile    |

    Scenario Outline: Accessing winning prepaid autoplay ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a winning ticket on <platformSection>
        Then a modal with the following elements is displayed: Lottery icon, Results text, Draw date and winning numbers
        And the following information will be also displayed above the lines: Lottery icon and jackpot, Draw date, number of lines and price per line
        And the winning section will be displayed, containing the following information: Megalotto icon, You won message, Amount of money won and congratulations message
        And ticket's lines are displayed, winning numbers will be highlighted and no winning numbers will be disabled
        And the following information will be also displayed below the lines: Autoplay icon and text, winning numbers, purchase date, autoplay duration, ticket price, autoplay cost, winnings, ticket number, button to renew autoplay.
        Examples:
            | kindOfDevice | platformSection |
            | mobile       | homepage        |
            | desktop      | homepage        |
            | mobile       | user profile    |
            | desktop      | user profile    |

    Scenario Outline: Clicking the button
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a <kindOfTicket> prepaid autoplay ticket on <platformSection>
        When the user clicks on renew autoplay button
        Then the user is redirected to purchase confirmation page for that specific lottery with the same lines, numbers and autoplay duration the past ticket has.
        Examples:
            | kindOfDevice | kindOfTicket | platformSection            |
            | mobile       | winning      | homepage                   |
            | desktop      | no winning   | homepage                   |
            | mobile       | winning      | user profile > past tab    |
            | desktop      | no winning   | user profile > past tab    |
            | mobile       | winning      | user profile > winning tab |
            | desktop      | winning      | user profile > winning tab |