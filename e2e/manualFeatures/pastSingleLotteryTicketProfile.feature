Feature: Past single lottery tickets on the profile page

    Past single lottery tickets on the profile page

    Scenario Outline: Accesing past lottery tickets section in the profile page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses profile page
        Then past single lottery tickets section <statusOfTheSection>
        Examples:
            | statusOfTheUser | kindOfDevice | statusOfTheSection                                                     |
            | logged in       | mobile       | is displayed and selected by default                                   |
            | logged in       | desktop      | is displayed and selected by default                                   |
            | logged out      | mobile       | is not displayed as profile page is only available for logged in users |
            | logged out      | desktop      | is not displayed as profile page is only available for logged in users |

    Scenario Outline: User who does not have any single past lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any single past lottery ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                                              |
            | mobile       | descriptive text, a powerball lottery ticket and a link to explore other lotteries |
            | desktop      | descriptive text and a link to explore other lotteries                             |

    Scenario Outline: User who has past single lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning ticket and one no winning ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the following elements are displayed inside each winning ticket: <winningTicketElements>
        And the following elements are displayed inside each no winning ticket: <noWinningTicketElements>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketElements                                                                   | noWinningTicketElements                                              |
            | mobile       | vertically        | Golden styles, lottery icon, draw date, amount of money won and three dots on the right | Lottery icon, draw date, number of lines and three dots on the right |
            | desktop      | horizontally      | Lottery icon, draw date and amount of money won                                         | Lottery icon, draw date and number of lines                          |


    Scenario Outline: Loading more tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has more than 3 single past tickets
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        Then a button to load more tickets is displayed
        When the user clicks the load more button
        Then <numberOfTickets> are loaded
        Examples:
            | kindOfDevice | numberOfTickets                                   |
            | mobile       | another 3 past lottery tickets                    |
            | desktop      | two more rows (6 tickets) of past lottery tickets |


    Scenario Outline: Options for past tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning ticket and one no winning ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the winning tickets have the following options: <winningTicketOptions>
        And the no winning tickets have the following options: <noWinningTicketOptions>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketOptions                                                                                           | noWinningTicketOptions                                                                                       |
            | mobile       | vertically        | Three golden dots with the following options: View ticket, Autoplay these numbers and Play again these numbers | Three black dots with the following options: View ticket, Autoplay these numbers and Play again these number |
            | desktop      | horizontally      | Two golden icons, one to autoplay these numbers and other to Play again these numbers                          | Two standard icons, one to autoplay these numbers and other to Play again these numbers                      |

    Scenario Outline: Clicking the mobile options
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        And the user has at least one winning ticket and one no winning ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        When the user clicks on the three dots of a <kindOfPastTicket> ticket
        Then the modal with the ticket options is displayed
        When the user clicks on <ticketOption> option
        Then <resultOfClickedOption>
        Examples:
            | kindOfPastTicket | ticketOption             | resultOfClickedOption                                                                |
            | winning          | View ticket              | past ticket details are displayed                                                    |
            | winning          | Autoplay these numbers   | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | winning          | Play again these numbers | purchase modal is open with autoplay option disabled, same lines and numbers are set |
            | no winning       | View ticket              | past ticket details are displayed                                                    |
            | no winning       | Autoplay these numbers   | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | no winning       | Play again these numbers | purchase modal is open with autoplay option disabled, same lines and numbers are set |

    Scenario Outline: Clicking the desktop options
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has at least one winning ticket and one no winning ticket
        When the user accesses profile page
        And the user clicks on past games tab
        Then the past lottery tickets section is displayed
        When the user clicks on <ticketOption> option of a <kindOfPastTicket> ticket
        Then <resultOfClickedOption>
        Examples:
            | ticketOption             | kindOfPastTicket | resultOfClickedOption                                                                |
            | Autoplay these numbers   | winning          | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | Play again these numbers | winning          | purchase modal is open with autoplay option disabled, same lines and numbers are set |
            | Autoplay these numbers   | no winning       | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | Play again these numbers | no winning       | purchase modal is open with autoplay option disabled, same lines and numbers are set |