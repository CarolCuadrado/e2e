Feature: Past single ticket details

    Past single ticket details

    Scenario Outline: Accessing no winning ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a no winning ticket on <platformSection>
        Then a modal with the following elements is displayed: Lottery icon, Results text, Draw date and winning numbers
        And the following information will be also displayed above the lines: Lottery icon and jackpot, Draw date, number of lines and price per line
        And ticket's lines are displayed, winning numbers will be highlighted and no winning numbers will be disabled
        And the following information will be also displayed below the lines: Winning numbers, purchase date, Total price, Winnings, Ticket number, button to autoplay these numbers and button to play these numbers again.
        Examples:
            | platformSection | kindOfDevice |
            | homepage        | mobile       |
            | homepage        | desktop      |
            | user profile    | mobile       |
            | user profile    | desktop      |

    Scenario Outline: Accessing winning ticket details
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a winning ticket on <platformSection>
        Then a modal with the following elements is displayed: Lottery icon, Results text, Draw date and winning numbers
        And the following information will be also displayed above the lines: Lottery icon and jackpot, Draw date, number of lines and price per line
        And the winning section will be displayed, containing the following information: Megalotto icon, You won message, Amount of money won and congratulations message
        And ticket's lines are displayed, winning numbers will be highlighted and no winning numbers will be disabled
        And the following information will be also displayed below the lines: Winning numbers, purchase date, Total price, Winnings, Ticket number, button to autoplay these numbers and button to play these numbers again.
        Examples:
            | platformSection | kindOfDevice |
            | homepage        | mobile       |
            | homepage        | desktop      |
            | user profile    | mobile       |
            | user profile    | desktop      |

    Scenario Outline: Clicking the buttons
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a past ticket on <platformSection>
        Then the past ticket details are displayed
        When the user clicks on <buttonToClick>
        Then the user is redirected to purchase confirmation page for that specific lottery with the same lines and numbers the past ticket had, autoplay option will be <statusOfAutoplay>
        Examples:
            | platformSection | kindOfDevice | buttonToClick          | statusOfAutoplay |
            | homepage        | mobile       | autoplay these numbers | enabled          |
            | homepage        | desktop      | play again             | disabled         |
            | user profile    | mobile       | play again             | disabled         |
            | user profile    | desktop      | autoplay these numbers | enabled          |