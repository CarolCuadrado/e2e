Feature: Past unchecked lottery tickets in the homepage

    Past unchecked tickets in the homepage

    Scenario Outline: Accesing past unchecked lottery tickets section in the homepage
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses home page
        Then past unchecked lottery tickets section <statusOfTheSection>
        Examples:
            | statusOfTheUser | kindOfDevice | statusOfTheSection                             |
            | logged in       | mobile       | is displayed after the user clicks on Past tab |
            | logged in       | desktop      | is displayed after the user clicks on Past tab |
            | logged out      | mobile       | is not displayed                               |
            | logged out      | desktop      | is not displayed                               |

    Scenario Outline: User who does not have any past unchecked lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any active ticket
        When the user accesses home page
        And the user clicks on past tab
        Then past unchecked lottery tickets section is displayed
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                           |
            | mobile       | descriptive text, 0 active lines and a powerball lottery ticket |
            | desktop      | descriptive text, 0 active lines                                |


    Scenario Outline: User who has past unchecked lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user <numberOfPastUncheckedTickets> past unchecked tickets
        When the user accesses home page
        And the user clicks on past tab
        Then only past unchecked lottery tickets are displayed
        And the following elements are also displayed: Number of active lines and time until next draw
        When the user tries to <wayToMoveTickets> horizontally the tickets
        Then the past unchecked lottery tickets are <wayToMoveTickets>
        Examples:
            | kindOfDevice | numberOfPastUncheckedTickets | wayToMoveTickets |
            | mobile       | 4 or more                    | scroll           |
            | desktop      | 10 or more                   | slide            |


    Scenario Outline: Ticket is no longer in the section after the user checks it
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has, at least, one past unchecked lottery ticket
        When the user accesses home page
        And the user clicks on past tab
        Then past unchecked lottery tickets section is displayed
        And the section displays the past unchecked lottery ticket
        When the user clicks on the lottery ticket
        And ther user goes back to homepage and past unchecked lotteries section
        Then the lottery ticket the user just checked is no longer present
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |



    -------------------------ML-321---------------------------
    Scenario Outline: Checking orange icon on past tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has, at least, one past unchecked single lottery ticket
        And the user has, at least, one past unchecked prepaid autoplay lottery ticket
        And the user has, at least, one past unchecked subscription lottery ticket
        When the user accesses home page
        And the user clicks on past tab
        Then past unchecked lottery tickets section is displayed
        And all the past unchecked lottery tickets have an orange icon on the top right
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |