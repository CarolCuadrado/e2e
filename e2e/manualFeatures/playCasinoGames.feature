Feature: Play casino games feature

    Play casino games

    Scenario Outline: Play casino games in mobile devices
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a mobile device
        When the user visits casino lobby
        And the user clicks on any casino game
        Then the game is automatically opened in fullscreen mode
        And the user can only play the game with <kindOfMoney> money
        When the user clicks on the home button in the game
        Then the user is redirected back to games lobby
        Examples:
            | statusOfTheUser | kindOfMoney |
            | logged in       | real        |
            | logged out      | no real     |

    Scenario Outline: Play casino games in desktop devices
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a desktop device
        When the user visits casino lobby
        And the user clicks on any casino game
        Then the game is not opened in fullscreen mode
        And the user can only play the game with <kindOfMoney> money
        When the user clicks the fullscreen mode button
        Then the game is displayed in fullscreen mode
        When the user clicks on the close button
        Then the user is redirected back to games lobby
        Examples:
            | statusOfTheUser | kindOfMoney |
            | logged in       | real        |
            | logged out      | no real     |
