Feature: Popular lotteries section on the homepage

    Popular lotteries section on the homepage

    Scenario Outline: Checking popular lotteries section is displayed
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are more than 5 popular lotteries configured on the CMS
        When the user accesses to homepage
        Then popular lotteries section is displayed on the homepage
        And popular lotteries section displays <elementsDisplayed>
        And popular lotteries section is placed below biggest jackpots section
        Examples:
            | statusOfTheUser | kindOfDevice | elementsDisplayed                                 |
            | logged in       | mobile       | all popular lotteries configured on the CMS       |
            | logged in       | desktop      | only the first 5 popular lotteries in a carrousel |
            | logged out      | mobile       | all popular lotteries configured on the CMS       |
            | logged out      | desktop      | only the first 5 popular lotteries in a carrousel |

    Scenario Outline: Checking popular lotteries section is not displayed when there are no lotteries configured
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are not any popular lotteries configured on the CMS
        When the user accesses to homepage
        Then popular lotteries section is not displayed on the homepage
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Loading more lotteries on desktop
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a desktop device
        And there are more than 5 popular lotteries configured on the CMS
        When the user accesses to homepage
        Then popular lotteries section is displayed on the homepage
        When the user clicks on the right arrow of the carrousel
        Then another 5 lotteries are displayed
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |