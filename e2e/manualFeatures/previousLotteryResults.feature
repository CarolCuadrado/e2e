Feature: Previous lottery results from lottery details

    Previous lottery results

    Scenario: More than three/six lottery results for a lottery ticket
        Given a logged in user in Megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is in a lottery ticket details page which has more than <numberOfLotteryResults> previous results
        When the user clicks on Results tab
        Then the last <numberOfLotteryResults> lottery results are displayed
        And each item contains the date and the winning combination of numbers
        And a load more button is shown
        When the user clicks on the load more button
        Then another <numberOfLotteryResults> lottery results are loaded
        Examples:
            | kindOfDevice | numberOfLotteryResults |
            | mobile       | 3                      |
            | desktop      | 6                      |

    Scenario: Less than three/six lottery results for a lottery ticket
        Given a logged in user in Megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is in a lottery ticket details page which has less than <numberOfLotteryResults> previous results
        When the user clicks on Results tab
        Then the last lottery results are displayed
        And each item contains the date and the winning combination of numbers
        And the load more button is not shown
        Examples:
            | kindOfDevice | numberOfLotteryResults |
            | mobile       | 3                      |
            | desktop      | 6                      |

    Scenario: No previous results for a lottery ticket
        Given a logged in user in Megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is in a lottery ticket details page which does not have any previous result
        When the user clicks on Results tab
        Then a descriptive text is displayed
        And the load more button is not shown
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |
