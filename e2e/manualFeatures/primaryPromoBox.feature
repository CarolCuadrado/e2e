Feature: Primary promo box

    Primary promo box

    Scenario Outline: Checking primary promo box elements and clicking on the button
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        Then the primary promo box is displayed with the following items: Headline, Background image, Lottery icon, Lottery jackpot, Time until draw and a play button
        When the user clicks on the button
        Then the user is redirected to purchase confirmation page for that specific lottery
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |