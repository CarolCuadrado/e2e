Feature: Privacy and cookie policy

    Privacy and cookie policy static page

    Scenario Outline: Accesing privacy and cookie policy page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on privacy and cookie policy option in the footer
        Then the privacy and cookie policy page is displayed as a <kindOfPage>
        Examples:
            | statusOfTheUser | kindOfDevice | kindOfPAge  |
            | logged in       | mobile       | normal page |
            | logged in       | desktop      | modal       |
            | logged out      | mobile       | normal page |
            | logged out      | desktop      | modal       |

    Scenario Outline: Closing Privacy and cookie policy in desktop
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a desktop device
        When the user clicks on privacy and cookie policy option in the footer
        Then the privacy and cookie policy page is displayed as a modal page
        And the page contains a button to close the modal
        When the user clicks on the close button
        Then the modal window is closed
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |