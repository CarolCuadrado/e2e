Feature: Profile overlay menu

    Profile overlay menu for logged in users

    Scenario: Logged in user accesses profile overlay menu
        Given a logged in user in Megalotto platform
        When the user clicks on profile user icon
        Then the profile overlay menu is displayed with the following elements: Current balance of the user, Deposit button, bonus balance of the user, Wallet option, Profile option, Inbox option, Promotions option, Settings option and log out option.

    Scenario: Logged out user cannot access profile overlay menu
        Given a logged out user in Megalotto platform
        When the user clicks on profile user icon
        Then the profile overlay menu is not displayed
        And the login page is shown instead

    Scenario Outline: Clicking elements of the profile overlay menu
        Given a logged in user in Megalotto platform
        When the user clicks on profile user icon
        Then the profile overlay menu is displayed
        When the user click on <profileOverlayMenuOption>
        Then the user is redirected to <redirectedOption>
        Examples:
            | profileOverlayMenuOption | redirectedOption       |
            | Deposit button           | Deposit page in wallet |
            | Wallet option            | Wallet default page    |
            | Profile option           | User profile page      |
            | Inbox option             | User's messages        |
            | Promotions option        | Promotions page        |
            | Settings option          | Settings page          |
            | Log out option           | logged out homepage    |

    Scenario Outline: Closing the menu depending on the kind of device used
        Given a logged out user in Megalotto platform using a <kindOfDevice> device
        When the user clicks on profile user icon
        Then the profile overlay menu is displayed
        When the user clicks <closeOption>
        Then the profile overlay menu is closed
        Examples:
            | kindOfDevice | closeOption                |
            | mobile       | on the close button        |
            | desktop      | elsewhere outside the menu |
