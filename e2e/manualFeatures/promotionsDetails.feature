Feature: Promotions Details

    Promotions Details

    Scenario Outline: Accessing one promotions and checking elements in the page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a promotion
        Then the user is redirected detailed promotion page with the following elements: Image of the promotion, Title of the promotion, Description,<kindOfSteps> help steps, terms and conditions and button to claim the offer
        Examples:
            | statusOfTheUser | kindOfDevice | kindOfSteps   |
            | logged in       | mobile       | scrollable    |
            | logged in       | desktop      | no scrollable |
            | logged out      | mobile       | scrollable    |
            | logged out      | desktop      | no scrollable |

    Scenario Outline: Scrolling steps on mobile devices
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a promotion
        Then the user is redirected detailed promotion page
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Claiming the promotion
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a promotion
        Then the user is redirected detailed promotion page
        When the user clicks on claim promotion button
        Then the user is redirected to <promotionRedirection>
        Examples:
            | statusOfTheUser | kindOfDevice | promotionRedirection                   |
            | logged in       | mobile       | the page where the promotion redirects |
            | logged in       | desktop      | the page where the promotion redirects |
            | logged out      | mobile       | login page                             |
            | logged out      | desktop      | login page                             |
    
    Scenario Outline: Redirection after claiming the promotion
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user clicks on a promotion that requires a deposit
        And the user is redirected detailed promotion page
        And the user clicks on claim promotion button
        And the user is redirected to wallet
        When the user completes a deposit
        Then the user is redirected to the page where the promotion redirects
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |