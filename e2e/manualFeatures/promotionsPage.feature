Feature: Promotions Page

    Promotions Page

    Scenario: Accessing promotions page and checking content
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on promotions link in <location>
        Then the promotions page is displayed with <promotionsPerRow> promotion/s per row
        And each promotion item contains: Image, Title, Play button, Short descriptive text and Read more link
        Examples:
            | statusOfTheUser | kindOfDevice | location             | promotionsPerRow |
            | logged in       | mobile       | hamburger menu       | 1                |
            | logged in       | mobile       | profile overlay menu | 1                |
            | logged in       | desktop      | top navigation menu  | 2                |
            | logged in       | desktop      | profile overlay      | 2                |
            | logged out      | mobile       | hamburger menu       | 1                |
            | logged out      | desktop      | top navigation menu  | 2                |

    Scenario: Checking redirections
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses promotions page
        And clicks on <elementToClick> in one promotion
        Then the user is redirected to detailed promotion page
        Examples:
            | statusOfTheUser | kindOfDevice | elementToClick |
            | logged in       | mobile       | play button    |
            | logged in       | desktop      | read more link |
            | logged out      | mobile       | read more link |
            | logged out      | desktop      | play button    |

    Scenario Outline: Accessing promotions page when is empty
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are no promos configured on the CMS
        When the user clicks on promotions link in <location>
        Then the promotions page doesn't show any promotions
        And there is an icon, descriptive text and a button
        When the use clicks on the button
        Then the user is redirected to the homepage
        Examples:
            | statusOfTheUser | kindOfDevice | location             |
            | logged in       | mobile       | hamburger menu       |
            | logged in       | mobile       | profile overlay menu |
            | logged in       | desktop      | top navigation menu  |
            | logged in       | desktop      | profile overlay      |
            | logged out      | mobile       | hamburger menu       |
            | logged out      | desktop      | top navigation menu  |