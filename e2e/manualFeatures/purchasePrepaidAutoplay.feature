Feature: Purchase prepaid autoplay ticket

    Purchase prepaid autoplay ticket


    Scenario Outline: Purchase a prepaid autoplay ticket with a logged out user
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on a play button on <pageToAccessModal>
        Then the purchase modal for that specific lottery is displayed
        And the ticket has three randomly generated lines following the rules for that specific lottery
        And payment method section is not displayed
        And autoplay section is displayed and disabled by default
        And autoplay section contains the following: Autoplay icon, title, days of the week the lottery has draws and a toggle button
        And the purchase button is not displayed
        And there is a login button redirecting to login window
        When the user enables the autoplay button
        Then the following options are displayed below: 4 and 8 weeks
        And 8 weeks option is selected by default
        And there is information about how many draws the user is going to play, date of the first draw and price per draw
        And the login button is still present
        When the user clicks on the login button
        Then the user is redirected to login window
        When the user logs in
        Then the purchase modal for the lottery the user was purchasing is shown again
        And the same number of lines with the same numbers are displayed
        And autoplay option is enabled with 8 weeks selected
        Examples:
            | kindOfDevice | pageToAccessModal    |
            | mobile       | lotteries page       |
            | mobile       | lottery details      |
            | mobile       | drawing soon section |
            | mobile       | ending soon widget   |
            | desktop      | lotteries page       |
            | desktop      | lottery details      |
            | desktop      | drawing soon section |
            | desktop      | ending soon widget   |

    Scenario Outline: Purchase a 4 weeks prepaid autoplay ticket with a logged in user with sufficient balance
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has enough money on wallet to buy a lottery ticket
        When the user clicks on a play button on <pageToAccessModal>
        Then the purchase modal for that specific lottery is displayed
        And the ticket has three randomly generated lines following the rules for that specific lottery
        And user's wallet balance is displayed
        And wallet option on payment method section is shown as selected
        And the purchase button is enabled with the amount to be paid
        And autoplay section is displayed and disabled by default
        And autoplay section contains the following: Autoplay icon, title, days of the week the lottery has draws and a toggle button
        When the user enables the autoplay button
        Then the following options are displayed below: 4 and 8 weeks
        And 8 weeks option is selected by default
        And there is information about how many draws the user is going to play, date of the first draw and price per draw
        And the purchase button is updated with the correct price
        And wallet is displayed as payment option
        When the user selects 4 weeks option
        Then the purchase button with the price is updated properly
        When the user clicks on purchase button
        Then then prepaid autoplay lottery ticket is purchased
        Examples:
            | kindOfDevice | pageToAccessModal                 |
            | mobile       | lotteries page                    |
            | mobile       | lottery details                   |
            | mobile       | suggested lottery on user profile |
            | mobile       | drawing soon section              |
            | mobile       | recently played section           |
            | mobile       | ending soon widget                |
            | desktop      | lotteries page                    |
            | desktop      | lottery details                   |
            | desktop      | drawing soon section              |
            | desktop      | recently played section           |
            | desktop      | ending soon widget                |

    Scenario Outline: Purchase a 8 weeks prepaid autoplay ticket with a logged in user with sufficient balance
        Given a logged in user in megalotto platform
        And the user is on purchase lottery ticket modal
        And the user is using a <kindOfDevice> device
        And the user has enough money on wallet to buy a lottery ticket
        When the user enables the autoplay button
        Then the following options are displayed below: 4 and 8 weeks
        And 8 weeks option is selected by default
        And there is information about how many draws the user is going to play, date of the first draw and price per draw
        And the purchase button is updated with the correct price
        And wallet is displayed as payment option
        When the user clicks on purchase button
        Then the prepaid autoplay lottery ticket is purchased
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Purchase a prepaid autoplay ticket with a logged in user who does not have sufficient balance but a card registered
        Given a logged in user in megalotto platform
        And the user is on purchase lottery ticket modal
        And the user is using a <kindOfDevice> device
        And the user does not have sufficient balance to buy the ticket
        And the user has a card registered
        When the user enables the autoplay button
        Then the following options are displayed below: 4 and 8 weeks
        And 8 weeks option is selected by default
        And there is information about how many draws the user is going to play, date of the first draw and price per draw
        And the purchase button is updated with the correct price
        And card is displayed as payment option
        When the user enters the card CVC
        And the user clicks on purchase button
        Then the prepaid autoplay lottery ticket is purchased
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Purchase a prepaid autoplay ticket with a logged in user who does not have neither sufficient balance nor a card registered
        Given a logged in user in megalotto platform
        And the user is on purchase lottery ticket modal
        And the user is using a <kindOfDevice> device
        And the user does not have sufficient balance to buy the ticket
        And the user does not have a card registered
        And deposit button is displayed
        When the user enables the autoplay button
        Then the following options are displayed below: 4 and 8 weeks
        And 8 weeks option is selected by default
        And there is information about how many draws the user is going to play, date of the first draw and price per draw
        And deposit button is still present
        And payment section is not displayed
        When the user clicks on deposit button
        And the user deposits the sufficient amount to buy the lottery ticket
        Then the user is redirected back to purchase modal with the same lines and numbers
        And wallet is displayed as payment option
        And purchase button is displayed with the correct price
        And deposit button is no longer present
        When the user clicks on purchase button
        Then the prepaid autoplay lottery ticket is purchased
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Disable autoplay toggle button with a logged out user
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the autoplay section is enabled
        When the user disables the autoplay option
        Then options below the autoplay option dissapear
        And payment section is not displayed
        And the login button is displayed without price
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Disable autoplay toggle button with a logged in user
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the autoplay section is enabled
        And the user <conditionOfTheUser>
        When the user disables the autoplay option
        Then options below the autoplay option dissapear
        And payment section <statusOfPaymentSection>
        And purchase button <statusOfPurchaseButton>
        Examples:
            | kindOfDevice | conditionOfTheUser                                             | statusOfPaymentSection           | statusOfPurchaseButton                                                  |
            | mobile       | has sufficient balance to buy the ticket                       | shows wallet as payment option   | purchase button is displayed and updated properly with the ticket price |
            | desktop      | does not have sufficient balance but a card registered         | shows the card as payment option | purchase button is displayed and updated properly with the ticket price |
            | mobile       | does not have neither sufficient balance nor a card registered | is not displayed                 | is not displayed and instead a deposit button is shown                  |
