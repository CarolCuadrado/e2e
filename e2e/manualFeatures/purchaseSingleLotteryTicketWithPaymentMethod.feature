Feature: Purchase singe lottery ticket with credit/debit cards

    Purchase singe lottery ticket with credit/debit cards

    Scenario Outline: Accessing purchase lottery modal with a logged in user without enough money to buy the ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        When the user clicks on a play button on <pageToAccessModal>
        Then the purchase modal for that specific lottery is displayed with the following elements: Lottery icon, Lottery Jackpot, Next lottery draw date, Lottery description with a learn more link and tooltip on the top right
        And the ticket has three randomly generated lines following the rules for that specific lottery
        And user's wallet balance is displayed
        And last active card is displayed and selected on payment options
        And there is a field next to the card to enter card CVC
        And a tooltip icon next to CVC field is displayed
        And a message with the minumum deposit amount that the cards allows is displayed
        And a field with the exact amount to deposit to purchase the ticket selected by default is displayed
        And a field to allow the user to enter the desired amount to be deposited disabled by default is displayed
        And a descriptive text explaining to the user the amount to be deposited and the amount taken from the wallet
        And the purchase button is enabled with the amount to be paid
        When the user enters the correct CVC code
        And the user clicks on the purchase button
        Then the lottery ticket is purchased
        Examples:
            | kindOfDevice | pageToAccessModal                 |
            | mobile       | lotteries page                    |
            | mobile       | lottery details                   |
            | mobile       | suggested lottery on user profile |
            | mobile       | drawing soon section              |
            | mobile       | recently played section           |
            | mobile       | ending soon widget                |
            | desktop      | lotteries page                    |
            | desktop      | lottery details                   |
            | desktop      | drawing soon section              |
            | desktop      | recently played section           |
            | desktop      | ending soon widget                |

    Scenario Outline: Setting different amount for the purchase
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        When the user selects the field to enter another amount
        Then the descriptive text is not displayed
        When the user enters a bigger amount than the needed to just buy the ticket
        And the user enters the correct CVC code
        Then the purchase button changes to Desposit & Purchase
        When the user clicks on the button
        Then the lottery ticket is purchased
        And the spare amount of money is added to user's wallet
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Setting a bigger amount than allowed to be deposited
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        When the user selects the field to enter another amount
        And the user enters a bigger amount than the card allows
        Then purchase button is disabled
        And there is an error message explaining that the set amount is bigger than allowed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Setting a smaller amount than allowed to be deposited
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        When the user selects the field to enter another amount
        And the user enters a smaller amount than the needed to perform the purchased
        Then purchase button is disabled
        And there is an error message explaining that the set amount is smaller than allowed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Needed amount to buy the ticket is less than the minimum the card allows
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        And the remaining amount needed to purchase the lottery ticket is less than the mininum that the card allows
        Then the field selected by default with the exact amount to purchase the lottery ticket shows the minimum amount that the card allows
        When the user selects the other field to enter a different amount
        And the user enters the exact amount to perform the purchase that is less than the minium than the card allows
        Then the purchase button is disabled
        And there is an error message explaining that the set amount is less than the card allows
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Enter a wrong CVC
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        When the user enters a wrong CVC code
        And the user clicks on purchase
        Then the transaction is not performed
        And an error message is displayed above/below the button
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Open CVC tooltip
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have enough money on wallet to buy a lottery ticket
        And the user has registered previously a credit or debit card
        And the user is already in purchase lottery ticket modal
        When the user clicks on the tooltip icon next to CVC field
        Then a tooltip with info about CVC code is open
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |