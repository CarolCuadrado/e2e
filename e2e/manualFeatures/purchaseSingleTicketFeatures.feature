Feature: Differents functionalities when purchasing a single lottery ticket

    Differents functionalities when purchasing a single lottery ticket

    Scenario Outline: Randomize line
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        When the user clicks on <wayToAccessRandomizeOption>
        Then the line numbers are changed randomly
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessRandomizeOption                |
            | logged out      | mobile       | three dots icon and then randomize option |
            | logged out      | desktop      | randomize icon next to each line          |
            | logged in       | desktop      | randomize icon next to each line          |
            | logged in       | mobile       | three dots icon and then randomize option |


    Scenario Outline: Remove line when there are at least two lines
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has more than two lines
        When the user clicks on <wayToAccessDeleteOption>
        Then the lottery line is removed
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessDeleteOption                     |
            | logged out      | mobile       | three dots icon and then delete line option |
            | logged out      | desktop      | delete icon next to each line               |
            | logged in       | desktop      | delete icon next to each line               |
            | logged in       | mobile       | three dots icon and then delete line option |

    Scenario Outline: Remove all the lines
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has only one line
        When the user clicks on <wayToAccessDeleteOption>
        Then lines section is not displayed
        And the button at the end of the ticket <purchaseButton>
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessDeleteOption                     | purchaseButton                                    |
            | logged out      | mobile       | three dots icon and then delete line option | is enabled as it is a button redirecting to login |
            | logged out      | desktop      | delete icon next to each line               | is enabled as it is a button redirecting to login |
            | logged in       | desktop      | delete icon next to each line               | is disabled                                       |
            | logged in       | mobile       | three dots icon and then delete line option | is disabled                                       |

    Scenario Outline: Removing lines and checking the payment option is changed properly with a user who does not have any card registered
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has 3 lines
        And the user only has balance to buy a 2 lines ticket
        And the user does not have any card registered before
        When the user clicks on remove line
        Then the line is removed
        And the deposit button changes to purchase
        And wallet option as payment method is displayed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Removing lines and checking the payment option is changed properly with a user who has a card registered
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has 3 lines
        And the user only has balance to buy a 2 lines ticket
        And the user has a card registered before
        When the user clicks on remove line
        Then the line is removed
        And the deposit & purchase button changes to purchase
        And the card is no longer shown as payment method
        And wallet is shown as payment option
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Add one line
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has less lines than the maximum allowed
        When the user clicks on add one more line button
        Then a random line is added to the lottery ticket
        And the price on the purchase button <lotteryPrice>
        Examples:
            | statusOfTheUser | kindOfDevice | lotteryPrice                           |
            | logged out      | mobile       | is not shown as the user is logged out |
            | logged out      | desktop      | is not shown as the user is logged out |
            | logged in       | desktop      | is updated after adding a new line     |
            | logged in       | mobile       | is updated after adding a new line     |

    Scenario Outline: Add all the remaining lines until the maximum allowed
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has less lines than the maximum allowed
        When the user clicks on add the rest number of lines until the maximum allowed
        Then the lines are added with random numbers
        And the price on the purchase button <lotteryPrice>
        And both buttons to add lines are disabled
        Examples:
            | statusOfTheUser | kindOfDevice | lotteryPrice                           |
            | logged out      | mobile       | is not shown as the user is logged out |
            | logged out      | desktop      | is not shown as the user is logged out |
            | logged in       | desktop      | is updated after adding the new lines  |
            | logged in       | mobile       | is updated after adding the new lines  |



    Scenario Outline: Adding lines and checking the payment option is changed properly with a user who does not have any card registered
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has 3 lines
        And the user only has balance to buy a 3 lines ticket
        And the user does not have any card registered before
        When the user clicks on add one more line button
        Then a random line is added to the lottery ticket
        And purchase button changes to Deposit
        And wallet option in payment method is not displayed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Adding lines and checking the payment option is changed properly with a user who has a card registered
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has 3 lines
        And the user only has balance to buy a 3 lines ticket
        And the user has a card registered before
        When the user clicks on add one more line button
        Then a random line is added to the lottery ticket
        And purchase button changes to Deposit & purchase
        And wallet option in payment method is not displayed
        And the card registered is shown instead
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Edit numbers
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has at least one line
        When the user clicks on <wayToAccessEditNumbersOption>
        Then the number picker is open
        And the section with the numbers selected is empty
        And done button is disabled
        And there is a cancel option
        When the user selects all the main and special numbers required
        Then the rest of numbers are disabled
        And the order of the numbers is the same in which they were picked
        And the done button is enabled
        When the user clicks on the done button
        Then the user is redirected back to purchase modal
        And the line numbers are changed with the selected ones
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessEditNumbersOption                 |
            | logged out      | mobile       | three dots icon and then edit numbers option |
            | logged out      | desktop      | edit numbers icon next to each line          |
            | logged in       | desktop      | edit numbers icon next to each line          |
            | logged in       | mobile       | three dots icon and then edit numbers option |


    Scenario Outline: Change selected numbers
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has at least one line
        When the user clicks on <wayToAccessEditNumbersOption>
        Then the number picker is open
        And the section with the numbers selected is empty
        And done button is disabled
        And there is a cancel option
        When the user selects all the main and special numbers required
        Then the rest of numbers are disabled
        And the done button is enabled
        When the user clicks on an already selected number
        Then the number is removed from the selected ones
        And done button is disabled again
        When the user clicks on a new number
        Then the number is selected
        And the done button is enabled again
        When the user clicks on done button
        Then the user is redirected back to purchase modal
        And the line numbers are changed with the selected ones
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessEditNumbersOption                 |
            | logged out      | mobile       | three dots icon and then edit numbers option |
            | logged out      | desktop      | edit numbers icon next to each line          |
            | logged in       | desktop      | edit numbers icon next to each line          |
            | logged in       | mobile       | three dots icon and then edit numbers option |

    Scenario Outline: Go back when editing numbers without saving any changes
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is on purchase lottery ticket modal
        And the lottery ticket has at least one line
        When the user clicks on <wayToAccessEditNumbersOption>
        Then the number picker is open
        And the section with the numbers selected is empty
        And done button is disabled
        And there is a cancel option
        When the user clicks on cancel button
        Then the user is redirected back to purchase modal
        And the line numbers are not changed
        Examples:
            | statusOfTheUser | kindOfDevice | wayToAccessEditNumbersOption                 |
            | logged out      | mobile       | three dots icon and then edit numbers option |
            | logged out      | desktop      | edit numbers icon next to each line          |
            | logged in       | desktop      | edit numbers icon next to each line          |
            | logged in       | mobile       | three dots icon and then edit numbers option |