Feature: Recently played lotteries

    Recently played lotteries

    Scenario Outline: Checking last played lotteries with a user who has not purchased lotteries before
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses home page
        Then last played lotteries section is not displayed
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Checking last played lotteries with a user who has purchased lotteries before
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has purchased <numberOfLotteriesPurchased> lotteries before
        When the user accesses home page
        Then last played lotteries section <statusOfTheSection>
        And the each lottery ticket contains the following: Lottery icon, Jackpot amount, Time until next draw and button to play
        Examples:
            | statusOfTheUser | kindOfDevice | numberOfLotteriesPurchased | statusOfTheSection                                                                |
            | logged in       | mobile       | 1                          | displayed with only one ticket                                                    |
            | logged in       | mobile       | 3 or more                  | vertically displayed with the last 3 kind of lottery tickets the user purchased   |
            | logged in       | desktop      | 1                          | displayed with only one ticket                                                    |
            | logged in       | desktop      | 3 or more                  | horizontally displayed with the last 3 kind of lottery tickets the user purchased |
            | logged out      | mobile       | 1                          | not displayed                                                                     |
            | logged out      | desktop      | 3                          | not displayed                                                                     |

    Scenario Outline: Purchasing a new ticket and check if the section is properly updated
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has purchased 3 or more lotteries before
        When the user accesses home page
        And check last played lotteries section
        Then the section displayed the last 3 lotteries the user purchased
        When the user purchases a new lottery ticket
        And the user checks the recently played lotteries section in the homepage
        Then the section is properly updated after the purchase showing the last 3 lotteries the user purchased
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Clicking elements inside a lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has purchased 1 or more lotteries before
        And the user is in home page
        When the user clicks on <elementToBeClicked>
        Then the user is redirected to <redirectedPage>
        Examples:
            | kindOfDevice | elementToBeClicked                | redirectedPage                                       |
            | mobile       | play button in the lottery ticket | purchase confirmation page for that specific lottery |
            | mobile       | elsewhere on the ticket           | lottery details                                      |
            | desktop      | elsewhere on the ticket           | lottery details                                      |
            | desktop      | play button in the lottery ticket | purchase confirmation page for that specific lottery |
