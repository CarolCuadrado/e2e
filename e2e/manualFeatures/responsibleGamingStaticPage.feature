Feature: Responsible gaming static page

    Responsible gaming static page

    Scenario Outline: Accesing Responsible gaming static page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on responsible gaming option in the footer
        Then the responsible gaming static page is displayed as a normal page
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |
