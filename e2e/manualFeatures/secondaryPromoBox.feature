Feature: Secondary promo boxes

    Secondary promo boxes

    Scenario Outline: Checking scratchcards promo box section on the homepage
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are three scratchcards promo boxes configured in the CMS for the homepage
        When the user accesses the homepage
        Then three scratchcards promo boxes are displayed with the following items: Image, title, subtitle and CTA
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Checking casino promo box section on the homepage
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        And there are three casino promo boxes configured in the CMS for the homepage
        When the user accesses the homepage
        Then three casino promo boxes are displayed with the following items: Image, title, subtitle and CTA
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |