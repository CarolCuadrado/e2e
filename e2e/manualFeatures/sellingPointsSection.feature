Feature: Selling points section on the homepage

    Selling points section on the homepage

    Scenario Outline: Checking selling points section on the homepage for logged out users
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        Then the selling points section is displayed between secondary promos section and suggestion box component
        And there is a title
        And <numberOfRows> is/are displayed with 4 elements
        And each element has an icon and descriptive information <orientation>
        And payment methods are displayed within this section
        Examples:
            | kindOfDevice | numberOfRows | orientation              |
            | mobile       | 1            | to the right of the icon |
            | desktop      | 2            | below the icon           |

    Scenario Outline: Checking selling points section on the homepage for logged in users
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        Then the selling points section is not displayed on the homepage
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |