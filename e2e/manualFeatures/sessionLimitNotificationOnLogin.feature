Feature: Session limmit notification on login

    Session limmit notification on login

    Scenario: Trying to access to the platform with a user who has reached his session limit
        Given a logged out user in Megalotto platform
        And the user has reached the daily session limit set
        When the user tries to access the platform
        Then an error message is displayed "Your limit session has been reached"
        And the user is not able to log in again until 24 hours after the limit was set.