Feature: Sign up promo (Hero banner) on the homepage

    Sign up promo (Hero banner) on the homepage

    Scenario Outline: Checking sign up promo (Hero banner) elements and clicking on the button
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        Then the sign up promo is displayed at the top of the page
        And the sign up promo has the following items: Title, description, primary CTA, subtitle and <imageDisplayed>
        When the user clicks on the button
        Then sign up window is open
        Examples:
            | statusOfTheUser | kindOfDevice | imageDisplayed |
            | logged out      | mobile       | no image       |
            | logged out      | desktop      | an image       |

    Scenario Outline: Checking sign up promo box is not displayed for logged in users
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the homepage
        Then the sign up promo is not displayed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |