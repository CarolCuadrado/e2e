Feature: Sign up with btag parameter

    Sign up with btag parameter

    Scenario Outline: Sign up flow from affiliate site
        Given a logged out user in Megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the sign up window from an affiliate site
        Then the btag code is part of registation data in the URL
        When the user finishes the sign up flow
        Then the btag code is sent in the request payload
        Examples:
            | kindOfDevice |
            | desktop      |
            | mobile       |

    Scenario Outline: Sign up flow without having any btag code
        Given a logged out user in Megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses the sign up window directly from megalotto
        Then there is no btag code on the sign up URL
        When the user finishes the sign up flow
        Then no btag code is sent in the request payload
        Examples:
            | kindOfDevice |
            | desktop      |
            | mobile       |