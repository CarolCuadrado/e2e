Feature: Single lottery widget

    Single lottery widget

    Scenario Outline: Location and content of the single lottery ticket
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses to <platformPage>
        Then the single lottery widget is displayed with the following elements: Lottery icon, Jackpot amount, Countdown until draw and play button
        Examples:
            | statusOfTheUser | kindOfDevice | platformPage   |
            | logged in       | mobile       | lotteries page |
            | logged in       | desktop      | lotteries page |
            | logged out      | mobile       | lotteries page |
            | logged out      | desktop      | lotteries page |

    Scenario Outline: Clicking on single lottery ticket
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses to <platformPage>
        And clicks on <element>
        Then the user is redirected to <redirectedPage>
        Examples:
            | statusOfTheUser | kindOfDevice | platformPage   | element                           | redirectedPage                                       |
            | logged in       | mobile       | lotteries page | play button in the lottery ticket | purchase confirmation page for that specific lottery |
            | logged in       | desktop      | lotteries page | elsewhere on the ticket           | lottery details                                      |
            | logged out      | mobile       | lotteries page | elsewhere on the ticket           | lottery details                                      |
            | logged out      | desktop      | lotteries page | play button in the lottery ticket | purchase confirmation page for that specific lottery |