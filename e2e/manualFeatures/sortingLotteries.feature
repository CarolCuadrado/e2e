Feature: Sorting lotteries

    Sorting lotteries

    Scenario: Sorting lotteries
        Given a logged in user in Megalotto platform
        When he clicks on the hamburger menu
        And in lotteries option
        Then lotteries page is displayed
        When the user clicks on the sorting button
        Then the following options to sort are displayed: Name, Jackpot Size, Cut-off timer, Popularity and Draw date
        And a button to close the menu
        When the user selects <sortingOption> option
        Then the lotteries are refreshed and displayed as per the option selected
        Examples:
            | sortingOption |
            | Name          |
            | Jackpot Size  |
            | Cut-off timer |
            | Popularity    |
            | Draw date     |

    Scenario: Closing sorting menu without applying any filter
        Given a logged in user in Megalotto platform
        When he clicks on the hamburger menu
        And in lotteries option
        Then lotteries page is displayed
        When the user clicks on the sorting button
        Then the following options to sort are displayed: Name, Jackpot Size, Cut-off timer, Popularity and Draw date
        And a button to close the menu
        When the user clicks on the close button
        Then sorting menu dissapears
        And no filter is applied

