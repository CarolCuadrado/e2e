Feature: Suggestion box on the homepage

    Suggestion box on the homepage

    Scenario Outline: Checking element is on the homepage and links are redirecting properly
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses home page
        Then the suggestion box is displayed at the bottom of the page
        When the user clicks on <elementToClick>
        Then the user is redirected to <redirectedPage>
        Examples:
            | statusOfTheUser | kindOfDevice | elementToClick          | redirectedPage  |
            | logged in       | mobile       | Browse all lotteries    | Lotteries page  |
            | logged in       | desktop      | Browse all scratchcards | Scratcards page |
            | logged out      | mobile       | Questions               | Help page       |
            | logged out      | desktop      | Browse all lotteries    | Lotteries page  |
            | logged in       | mobile       | Browse all scratchcards | Scratcards page |
            | logged in       | desktop      | Questions               | Help page       |
            | logged out      | mobile       | Browse all lotteries    | Lotteries page  |
            | logged out      | desktop      | Browse all scratchcards | Scratcards page |
            | logged in       | desktop      | Browse all casino games | Casino lobby    |
            | logged out      | mobile       | Browse all casino games | Casino lobby    |