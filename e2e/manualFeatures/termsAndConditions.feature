Feature: Terms and conditions

    Terms and conditions static page

    Scenario Outline: Accesing terms and conditions page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user clicks on terms and conditions option in the footer
        Then the terms and conditions page is displayed as a <kindOfPage>
        Examples:
            | statusOfTheUser | kindOfDevice | kindOfPAge  |
            | logged in       | mobile       | normal page |
            | logged in       | desktop      | modal       |
            | logged out      | mobile       | normal page |
            | logged out      | desktop      | modal       |

    Scenario Outline: Closing terms and conditions modal in desktop
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a desktop device
        When the user clicks on terms and conditions option in the footer
        Then the terms and conditions page is displayed as a modal page
        And the page contains a button to close the modal
        When the user clicks on the close button
        Then the modal window is closed
        Examples:
            | statusOfTheUser |
            | logged in       |
            | logged out      |