Feature: Top navigation

    Header menu for logged in and out users

    Scenario Outline: Header menu (top navigation) is always present even when scrolling
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user is visiting any page on the platform
        And the user scrolls up and down
        Then the header menu is still visible on the top
        Examples:
            | statusOfTheUser | kindOfDevice |
            | logged in       | mobile       |
            | logged in       | desktop      |
            | logged out      | mobile       |
            | logged out      | desktop      |

    Scenario Outline: Elements in header menu (top navigation) for mobile devices
        Given a <statusOfTheUser> user in megalotto platform using a mobile device
        When the user visits any page on the platform
        Then the header menu (top navigation) is displayed with the following elements: Megalotto icon, user icon with <balanceOption> and hamburger icon
        Examples:
            | statusOfTheUser | balanceOption       |
            | logged in       | the current balance |
            | logged out      | no balance          |

    Scenario Outline: Clicking elements in header menu (top navigation) for mobile devices
        Given a <statusOfTheUser> user in megalotto platform using a mobile device
        When the user visits any page on the platform
        Then the header menu (top navigation) is displayed
        When the user clicks on <topNavigationOption>
        Then the user is redirected to <redirectedOption>
        Examples:
            | statusOfTheUser | topNavigationOption            | redirectedOption     |
            | logged in       | Megalotto icon                 | homepage             |
            | logged in       | user icon with current balance | profile overlay menu |
            | logged in       | hamburger icon                 | hamburger menu      |
            | logged out      | Megalotto icon                 | homepage             |
            | logged out      | user icon with no balance      | login page           |
            | logged out      | hamburger icon                 | hamburger menu       |

    Scenario Outline: Elements in header menu (top navigation) for desktop devices
        Given a <statusOfTheUser> user in megalotto platform using a desktop device
        When the user visits any page on the platform
        Then the header menu (top navigation) is displayed with the following elements: Megalotto icon, Lotteries option, Scratch cards option, Instant wins option, Games option,Promotions option, News option, Mega millions icon, Euro millions icon and user icon with <balanceOption>
        Examples:
            | statusOfTheUser | balanceOption       |
            | logged in       | the current balance |
            | logged out      | no balance          |

    Scenario Outline: Clicking elements in header menu (top navigation) for desktop devices
        Given a <statusOfTheUser> user in megalotto platform using a desktop device
        When the user visits any page on the platform
        Then the header menu (top navigation) is displayed
        When the user clicks on <topNavigationOption>
        Then the user is redirected to <redirectedOption>
        Examples:
            | statusOfTheUser | topNavigationOption            | redirectedOption                         |
            | logged in       | Megalotto icon                 | homepage                                 |
            | logged in       | Lotteries option               | /lotteries URL                           |
            | logged in       | Scratch cards option           | /scratchcards URL                        |
            | logged in       | Instant wins option            | /instantwins URL                         |
            | logged in       | Games option                   | /casino URL                               |
            | logged in       | Promotions option              | /promotions URL                          |
            | logged in       | News option                    | /news URL                                |
            | logged in       | Mega millions icon             | purchase modal for Mega Millions lottery |
            | logged in       | Euro millions icon             | purchase modal for Euro Millions lottery |
            | logged in       | user icon with current balance | profile overlay menu                     |
            | logged out      | Megalotto icon                 | homepage                                 |
            | logged out      | Lotteries option               | /lotteries URL                           |
            | logged out      | Scratch cards option           | /scratchcards URL                        |
            | logged out      | Instant wins option            | /instantwins URL                         |
            | logged out      | Games option                   | /casino URL                               |
            | logged out      | Promotions option              | /promotions URL                          |
            | logged out      | News option                    | /news URL                                |
            | logged out      | Mega millions icon             | purchase modal for Mega Millions lottery |
            | logged out      | Euro millions icon             | purchase modal for Euro Millions lottery |
            | logged out      | user icon with no balance      | login page                               |