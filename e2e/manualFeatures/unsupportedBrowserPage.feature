Feature: Unsupported browser page

    Unsupported browser page

    Scenario Outline: Check unsupported browser page is displayed
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is using a browse which does not support shadow dom
        When the user accesses megalotto platform
        Then unsupported browser page is displayed
        Examples:
            | kindOfDevice |
            | mobile       |
            | desktop      |

    Scenario Outline: Check up to the last 3 versions of browsers
        Given a logged out user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user is using the last 3 versions of <browser>
        When the user accesses megalotto platform
        Then megalotto platform is working as expected
        Examples:
            | kindOfDevice | browser           |
            | mobile       | chrome on android |
            | mobile       | safari on iphone  |
            | desktop      | safari on mac     |
            | desktop      | chrome on mac     |
            | desktop      | chrome on windows |
