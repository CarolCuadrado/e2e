Feature: Winning single lottery tickets on the profile page

    Winning single lottery tickets on the profile page

    Scenario Outline: Accesing winning lottery tickets section in the profile page
        Given a <statusOfTheUser> user in megalotto platform
        And the user is using a <kindOfDevice> device
        When the user accesses profile page
        Then winning single lottery tickets section <statusOfTheSection>
        Examples:
            | statusOfTheUser | kindOfDevice | statusOfTheSection                                                     |
            | logged in       | mobile       | is displayed and selected by default                                   |
            | logged in       | desktop      | is displayed and selected by default                                   |
            | logged out      | mobile       | is not displayed as profile page is only available for logged in users |
            | logged out      | desktop      | is not displayed as profile page is only available for logged in users |

    Scenario Outline: User who does not have any winning lottery ticket
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user does not have any winning single lottery ticket
        When the user accesses profile page
        And the user clicks on winnings games tab
        Then the winning lottery tickets section is displayed
        And the section does not have any lottery ticket
        And the following elements are displayed: <elementsToBeDisplayed>
        Examples:
            | kindOfDevice | elementsToBeDisplayed                                                              |
            | mobile       | descriptive text, a powerball lottery ticket and a link to explore other lotteries |
            | desktop      | descriptive text and a link to explore other lotteries                             |

    Scenario Outline: User who has winning single lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only winning lottery tickets are shown
        And the following elements are displayed inside each winning ticket: Golden styles, lottery icon, draw date and amount of money won
        Examples:
            | kindOfDevice | ticketOrientation |
            | mobile       | vertically        |
            | desktop      | horizontally      |


    Scenario Outline: Loading more tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has more than 3 winning single tickets
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        Then a button to load more tickets is displayed
        When the user clicks the load more button
        Then <numberOfTickets> are loaded
        Examples:
            | kindOfDevice | numberOfTickets                                      |
            | mobile       | another 3 winning lottery tickets                    |
            | desktop      | two more rows (6 tickets) of winnnig lottery tickets |

    Scenario Outline: Options for winning tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the winning tickets have the following options: <winningTicketOptions>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketOptions                                                                                           |
            | mobile       | vertically        | Three golden dots with the following options: View ticket, Autoplay these numbers and Play again these numbers |
            | desktop      | horizontally      | Two golden icons, one to autoplay these numbers and other to Play again these numbers                          |

    Scenario Outline: Clicking the mobile options
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        And the user has at least one winning ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        When the user clicks on the three dots of a winning ticket
        Then the modal with the ticket options is displayed
        When the user clicks on <ticketOption> option
        Then <resultOfClickedOption>
        Examples:
            | ticketOption             | resultOfClickedOption                                                                |
            | View ticket              | past ticket details are displayed                                                    |
            | Autoplay these numbers   | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | Play again these numbers | purchase modal is open with autoplay option disabled, same lines and numbers are set |

    Scenario Outline: Clicking the desktop options
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has at least one winning ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        When the user clicks on <ticketOption> option of a winning ticket
        Then <resultOfClickedOption>
        Examples:
            | ticketOption             | resultOfClickedOption                                                                |
            | Autoplay these numbers   | purchase modal is open with autoplay option enabled,  same lines and numbers are set |
            | Play again these numbers | purchase modal is open with autoplay option disabled, same lines and numbers are set |