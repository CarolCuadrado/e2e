Feature: Winnings lottery subscription and prepaid tickets on the user profile

    Winnings lottery subscription and prepaid tickets on the user profile


    Scenario Outline: User who has winning subscription lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning subscription ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only winning lottery tickets are shown
        And the following elements are displayed inside each winning subscription ticket: Golden styles, lottery icon, draw date, two arrows icon and amount of money won
        Examples:
            | kindOfDevice | ticketOrientation |
            | mobile       | vertically        |
            | desktop      | horizontally      |

    Scenario Outline: User who has winning prepaid autoplay lottery tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only winning lottery tickets are shown
        And the following elements are displayed inside each winning prepaid autoplay ticket: Golden styles, lottery icon, draw date, two arrows icon and amount of money won
        Examples:
            | kindOfDevice | ticketOrientation |
            | mobile       | vertically        |
            | desktop      | horizontally      |

    Scenario Outline: Options for winning prepaid autoplay tickets
        Given a logged in user in megalotto platform
        And the user is using a <kindOfDevice> device
        And the user has at least one winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        And lottery tickets are displayed <ticketOrientation>
        And only lottery tickets whose draw date has taken place are shown
        And the winning prepaid autoplay tickets have the following options: <winningTicketOptions>
        Examples:
            | kindOfDevice | ticketOrientation | winningTicketOptions                                                         |
            | mobile       | vertically        | Three golden dots with the following options: View ticket and renew autoplay |
            | desktop      | horizontally      | One golden icon to renew autoplay                                            |

    Scenario Outline: Clicking the mobile options
        Given a logged in user in megalotto platform
        And the user is using a mobile device
        And the user has at least one winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        When the user clicks on the three dots of a winning prepaid autoplay ticket
        Then the modal with the ticket options is displayed
        When the user clicks on <ticketOption> option of a winning prepaid autoplay ticket
        Then <resultOfClickedOption>
        Examples:
            | ticketOption   | resultOfClickedOption                                                                         |
            | View ticket    | past ticket details are displayed                                                             |
            | Renew autoplay | purchase modal is open with the same numbers, lines and autoplay duration the past ticket has |

    Scenario: Clicking the desktop options
        Given a logged in user in megalotto platform
        And the user is using a desktop device
        And the user has at least one winning prepaid autoplay ticket
        When the user accesses profile page
        And the user clicks on winning games tab
        Then the winning lottery tickets section is displayed
        When the user clicks on renew autoplay option of a winning ticket
        Then purchase modal is open with the same numbers, lines and autoplay duration the past ticket has