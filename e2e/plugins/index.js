/* eslint-disable @typescript-eslint/no-var-requires */
const globalProperties = require('../config.js');
const cucumber = require('cypress-cucumber-preprocessor').default;
const browserify = require('@cypress/browserify-preprocessor');

module.exports = (on, config) => {
    // Add a task to allow printing logs in terminal.
    // cy.task('log', 'message to be printed')
    on('task', {
        log(message) {
            // eslint-disable-next-line no-console
            console.log(message);
            return null;
        },
    });

    const options = browserify.defaultOptions;
    const babelOptions = options.browserifyOptions.transform[1][1];

    babelOptions.global = true;

    // Transpile certain node_modules to es5
    babelOptions.ignore = [
        /node_modules\/(?!query-selector-shadow-dom\/|@megalotto\/)/,
    ];

    on('file:preprocessor', cucumber(options));

    const configOverride = {};

    if (config.env.url) {
        configOverride.baseUrl =
            `${globalProperties.megalottoEnvironmentURLPrefix}`
            + `${config.env.url}${globalProperties.megalottoEnvironmentURLSuffix}`;
    } else {
        configOverride.baseUrl = globalProperties.localURL;
    }

    if (config.env.TAGS && config.env.TAGS.includes('mobile')) {
        configOverride.userAgent = globalProperties.iphoneUserAgent;
        configOverride.viewportWidth = globalProperties.iphoneViewportWidht;
        configOverride.viewportHeight = globalProperties.iphoneViewportHeight;
    }

    return Object.assign({}, config, configOverride);
};
/* eslint-enable @typescript-eslint/no-var-requires */
