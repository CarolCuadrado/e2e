

const scratchcardsRegExp = /scratchcards/;
const instantWinsRegExp = /live-casino/;
const casinoGamesRegExp = /my-new-lobby/;
const header = '[data-v-test-id="games-header"]';
const search = '[data-v-test-id="games-search"]';
const promoLink = '[data-v-test-id="games-header-promo-link"]';
const mainUrl = '/casino';
const categoriesMenuMobile = '[data-v-test-id="games-categories-menu-mobile"]';

const casinoLobbyPom = {
    // gameCardContainer: getSelectorFunction('game-card'),

    goToMainLobby: (() => {
        cy.authvisit(mainUrl);
    }),

    checkIsScratchCardsPage: (() => {
        casinoLobbyPom.checkIsCorrectGamesLobbyPage(scratchcardsRegExp);
    }),

    checkIsInstantWinsPage: (() => {
        casinoLobbyPom.checkIsCorrectGamesLobbyPage(instantWinsRegExp);
    }),

    checkIsCasinoGamesPage: (() => {
        casinoLobbyPom.checkIsCorrectGamesLobbyPage(casinoGamesRegExp);
    }),

    checkIsCorrectGamesLobbyPage: ((gamesCategory) => {
        Games.categoriesMenu().should('have.attr', 'active-item').and('match', gamesCategory);
    }),

    checkHeaderIsDisplayed: (() => {
        cy.get(header).should('be.visible');
    }),

    checkSearchIsDisplayed: (() => {
        cy.get(search).should('be.visible');
    }),
    
    checkPromoLinkIsDisplayed: (() => {
        cy.get(promoLink).should('be.visible');
    }),

    checkDesktopCategoriesMenuIsDisplayed: (() => {
        Games.categoriesMenu().should('be.visible');
    }),
    
    checkMobileCategoriesMenuIsDisplayed: (() => {
        cy.get(categoriesMenuMobile).should('be.visible');
    }),
};

export default casinoLobbyPom;
