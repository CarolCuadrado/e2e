const footer = '.footer__content .footer__navigation';

const footerMenuLinks = [
    { key: 'aboutUs', link: 'a[href*="about-us"]', name: 'about-us' },
    { key: 'help', link: 'a[href*="faq"]', name: 'faq' },
    { key: 'contactUs', link: 'a[href*="contact-us"]', name: 'contact-us' },
    { key: 'responsibleGaming', link: 'a[href*="responsible-gaming"]', name: 'responsible-gaming' },
    { key: 'termsAndConditions', link: 'a[href$="tnc"]', name: 'tnc' },
    { key: 'privacyPolicy', link: 'a[href*="privacy"]', name: 'privacy-and-cookie-policy' },
];
const paymentfooter = 'div.footer__payment > ul';
const PaymentMenuLinksfooter = [
    { key: 'Visa', link: 'a[href*="visa"]' ,name : 'visa'},
    { key: 'Mastercard', link: 'a[href*="mastercard"]', name: 'mastercard' },
    { key: 'skrill', link: 'a[href*="skrill"]',name: 'skrill' },
    { key: 'neteller', link: 'a[href*="neteller"]',name: 'neteller' },
    { key: 'paysafecard', link: 'a[href*="paysafecard"]',name: 'paysafecard' },
    { key: 'Trustly', link: 'a[href*="trustly"]',name: 'trustly' },
    { key: 'Klarna', link: 'a[href*="klarna"]',name: 'klarna' },
    { key: 'Euteller', link: 'a[href*="euteller"]',name: 'euteller' },
    { key: 'Interacetransfer', link: 'a[href*="interac"]',name: 'interacetransfer' },
];

const footerPom = {
    clickOption: ((option) => {
        cy.get(footer).find(footerMenuLinks.find((item) => item.name === option).link).click();
    }),

    scrollToFooter: (() => {
        cy.get(footer).scrollIntoView();
    }),

    checkFooterElements: (() => {
        footerMenuLinks.forEach((menuLink) => {
            cy.get(footer).find(menuLink.link).should('be.visible');
        });
    }),
    checkFooterPaymentElements: (() => {
        PaymentMenuLinksfooter.forEach((menuLink) => {
            cy.get(paymentfooter).find(menuLink.link).should('be.visible');
        });
    })
};

export default footerPom;
