/* eslint-disable @typescript-eslint/no-var-requires */
import { getCypressAdaptor } from '/Users/carolina/new_pro/e2e/magic-test-helpers/dist/cypress/adaptor';
import 'cypress-shadow-dom';
import languages from "../fixtures/languages.json";

const globalProperties = require('../config.js');
const forgotPasswordLink = '.login__content>magic-ui-link';
const forgotPasswordModal = '.password-reset-request.page';
const forgotPasswordInput = 'magic-reset-password-request';
const forgotPasswordSendButton = 'magic-reset-password-request';
const forgotPasswordEmailSentConfirmationModal = '.password-reset-confirmation.page';
const enterNewPasswordModal = '.password-reset-form.page';
const firstPasswordField = '.form';
const secondPasswordField = 'form.reset-password-form magic-ui-peek-password:nth-of-type(2) input';
const changePasswordButton = 'magic-reset-password-form';
const passwordSuccesfullyChangeModal = '.password-reset-success.page';
const loginRedirectButton = 'magic-reset-password-success';

const forgottenPasswordPom = {

    clickForgotPasswordLink: (() => {
        //cy.get(forgotPasswordLink).shadow().find('a').click({force:true});
        cy.get(forgotPasswordLink).find('a',{includeShadowDom : true}).click({force:true});
       
    }),

    checkForgotPasswordModalIsDisplayed: (() => {
        cy.get(forgotPasswordModal).should('be.visible');
    }),

    enterEmailInForgotPassword: ((email) => {
        cy.get(forgotPasswordInput).shadow().find('form>magic-ui-form-field').shadow().find('.form-field>magic-ui-input').shadow().find('input').type(email,{force:true});
  

    }),

    checkSendButtonIsEnabled: (() => {
        cy.get(forgotPasswordSendButton).shadow().find('button').should("not.have.property", "disabled");
    }),

    clickOnSendButton: (() => {
        cy.get(forgotPasswordSendButton).shadow().find('form>div>magic-ui-button').shadow().find('button').click({force:true});
    }),

    checkEmailSentModalIsDisplayed: (() => {
        cy.get(forgotPasswordEmailSentConfirmationModal).should("be.visible");
    }),

    mockSuccessfulPasswordChange: (() => {
        cy.fixture('forgottenPasswordResponses').then((forgottenPasswordResponses) => {
            cy.addRoute(globalProperties.resetPasswordAPIURL,
                forgottenPasswordResponses.passwordSuccessfullyChanged.method,
                forgottenPasswordResponses.passwordSuccessfullyChanged.status,
                forgottenPasswordResponses.passwordSuccessfullyChanged.response)
                .as('passwordSucessfullyChanged');
        });
    }),

    mockUnsuccessfulPasswordChange: (() => {
        cy.fixture('forgottenPasswordResponses').then((forgottenPasswordResponses) => {
            cy.addRoute(globalProperties.resetPasswordAPIURL,
                forgottenPasswordResponses.passwordUnsuccessfullyChanged.method,
                forgottenPasswordResponses.passwordUnsuccessfullyChanged.status,
                forgottenPasswordResponses.passwordUnsuccessfullyChanged.response)
                .as('passwordUnsucessfullyChanged');
        });
    }),

    goToEnterNewPasswordModal: (() => {
        const enterNewPasswordURL = `${languages.English}${globalProperties.resetPasswordURL}`
            + `${globalProperties.expiredForgottenPasswordToken}`;
        cy.authvisit(enterNewPasswordURL);
    }),

    checkEnterNewPasswordModalIsDisplayed: (() => {
        cy.get(enterNewPasswordModal).should("be.visible");
    }),

    enterNewPassword: ((password) => {
        cy.get(firstPasswordField).shadow().find('form>magic-ui-peek-password:nth-of-type(1)').shadow().find('div>magic-ui-form-field').shadow().find('div>magic-ui-input').shadow().find('input').type(password,{force:true});
      
    }),

    enterConfirmPassword: ((password) => {
        cy.get(firstPasswordField).shadow().find('form>magic-ui-peek-password:nth-of-type(2)').shadow().find('div>magic-ui-form-field').shadow().find('div>magic-ui-input').shadow().find('input').type(password,{force:true});
      
    }),

    checkChangePasswordButtonIsEnabled: (() => {

        cy.get(changePasswordButton).shadow().find('form>div>magic-ui-button').shadow().find('button').should('be.enabled');
    }),

    clickOnChangePasswordButton: (() => {
         cy.get(changePasswordButton).shadow().find('form>div>magic-ui-button').shadow().find('button').click({force:true});
    }),

    checkSucessPasswordChangeModalIsDisplayed: (() => {
        cy.get(passwordSuccesfullyChangeModal).should("be.visible");
    }),

    checkLoginButtonIsDisplayed: (() => {
        cy.get(loginRedirectButton).shadow().find('.reset-success>div>magic-ui-link').shadow().find('magic-ui-button').shadow().find('a').should('exist');

    })

};

export default forgottenPasswordPom;
