const hamburgerMenu = 'aside.main-navigation';
const loginMobileButton = '[data-v-test-id="top-navigation-login-mobile"]';
const hamburgerMenuLinks = [
    { key: 'lotteries', link: 'a[href*="lotteries"]', name: 'lotteries' },
    { key: 'scratchcards', link: 'a[href*="scratchcards"]', name: 'scratchcards' },
    { key: 'liveCasino', link: 'a[href*="/casino/live-casino"]', name: 'live-casino' },
    { key: 'casino', link: 'a[href$="/casino"]', name: 'casino' },
    { key: 'promotions', link: 'a[href*="promo"]', name: 'promo' },
    { key: 'news', link: 'a[href*="news"]', name: 'news' },
    { key: 'faq', link: 'a[href*="faq"]', name: 'faq' },
    { key: 'aboutUs', link: 'a[href*="about-us"]', name: 'about-us' },
    { key: 'contactUs', link: 'a[href*="contact-us"]', name: 'contact-us' },
];

const hamburgerMenuPom = {

    isHamburgerMenuOpen: (() => {
        cy.get(hamburgerMenu).should('be.visible');
    }),

    isHamburgerMenuClosed: (() => {
        cy.get(hamburgerMenu).should('not.be.visible');
    }),

    clickSignup: (() => {
        cy.get(loginMobileButton).click();
    }),

    checkHamburgerMenuElements: (() => {
        hamburgerMenuLinks.forEach((menuLink) => {
            cy.get(hamburgerMenu).find(menuLink.link).should('be.visible');
        });
    }),

    clickOption: ((option) => {
        cy.get(hamburgerMenu).find(hamburgerMenuLinks.find((item) => item.name === option).link).click();
    }),
};

export default hamburgerMenuPom;
