
import 'cypress-shadow-dom';
const homepage = 'div.home.page';
const homepageLink = 'div.router-link-active:nth(1)';
const dropdown='magic-ui-dropdown';
const homepageSections = [
    { key: 'carousel', selector: '.promotion-hero', name: 'carousel' },
    { key: 'biggestJackpotsSection', selector: '[data-v-test-id="biggest-jackpots"]', name : 'biggest jackpots section'},
    { key: 'drawingSoonSection', selector: '[data-v-test-id="drawing-soon"]', name: 'drawing soon section' },
    { key: 'casinoGamesSection', selector: '[data-v-test-id="home-casino-games"]', name: 'casino games section' },
    { key: 'endingSoonWidget', selector: '[data-v-test-id="home-ending-soon"]', name: 'ending soon widget' },
    { key: 'scratchcardsPromoBoxSection', selector: '[data-v-test-id="home-promo-scratchcards"]', name: 'scratchcards promo box section' },
    { key: 'casinoPromoBoxSection', selector: '[data-v-test-id="home-promo-casino"]', name: 'casino promo box section' },
    { key: 'popularLotteriesSection', selector: '[data-v-test-id="popular-lotteries"]', name: 'popular lotteries section' },
    { key: 'sellingPoints', selector: '[data-v-test-id="home-selling-points"]', name: 'selling points'},
    { key: 'suggestionBox', selector: '[data-v-test-id="home-suggestion-box"]', name: 'suggestion box' },
    { key: 'accumulatedLotteryJackpots', selector: '[data-v-test-id="home-accumulated-jackpot"]', name: 'accumulated lottery jackpots' },
    { key: 'activePastLotteryTicketsSection', selector: '[data-v-test-id="player-tickets"]', name: 'active and past lottery tickets section' },
    { key: 'recentlyPlayedSection', selector: '[data-v-test-id="recently-played"]', name: 'recently played section' }
];
const payment = 'div.payment-methods > ul';
const PaymentMenuLinks = [
    { key: 'Visa', link: 'a[href*="visa"]',class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--visa',name : 'visa'},
    { key: 'Mastercard', link: 'a[href*="mastercard"]', class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--mastercard', name: 'mastercard' },
    { key: 'skrill', link: 'a[href*="skrill"]', class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--skrill',name: 'skrill' },
    { key: 'neteller', link: 'a[href*="neteller"]', class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--neteller',name: 'neteller' },
    { key: 'paysafecard', link: 'a[href$="paysafecard"]', class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--paysafecard',name: 'paysafecard' },
    { key: 'Trustly', link: 'a[href*="trustly"]', class : 'div > magic-ui-icon.payment-method-icon.payment-method-icon--trustly',name: 'trustly' },
];

const game_providers = 'div.game-providers > ul';
const gameProvidersIcon = [
    { key: 'playAndGo', class : 'div > img.game-provider-image' ,alt: 'playAndGo'  ,name : 'playAndGo'},
    { key: 'evolutionGaming',class : 'div > img.game-provider-image', alt: 'evolutionGaming',name: 'evolutionGaming' },
    { key: 'pragmaticPlay', class : 'div > img.game-provider-image',alt: 'pragmaticPlay',name: 'pragmaticPlay' },
    { key: 'bigTimeGaming', class : 'div > img.game-provider-image',alt: 'bigTimeGaming',name: 'bigTimeGaming' },
    { key: 'quickSpin', class : 'div > img.game-provider-image', alt: 'quickSpin',name: 'quickSpin' },
    { key: 'microGaming', class : 'div > img.game-provider-image',alt: 'microGaming',name: 'microGaming' },
    { key: 'pushGaming', class : 'div > img.game-provider-image',alt: 'pushGaming',name: 'pushGaming' },
    { key: 'noLimitCity', class : 'div > img.game-provider-image',alt: 'noLimitCity',name: 'noLimitCity' },
    { key: 'gamMat', class : 'div > img.game-provider-image',alt: 'gamMat',name: 'gamMat' },
];
const gameProvidersIconCanada = [
    { key: 'playAndGo', class: 'div > img.game-provider-image' ,alt : 'playAndGo'},
    { key: 'pragmaticPlay', class : 'div > img.game-provider-image',alt: 'pragmaticPlay' },
    { key: 'quickSpin', class : 'div > img.game-provider-image',alt: 'quickSpin' },
    { key: 'microGaming', class : 'div > img.game-provider-image',alt: 'microGaming' },
    { key: 'pushGaming', class : 'div > img.game-provider-image',alt: 'pushGaming' },
    { key: 'noLimitCity', class : 'div > img.game-provider-image',alt: 'noLimitCity' },
];
const homepagePom = {
    // paymentMethods: getSelectorFunction('payment-methods'),

    goToHomepage: (() => {
        cy.get(homepageLink).click();
    }),

    checkHomepageIsDisplayed: (() => {
        cy.get(homepage).should('be.visible');
    }),

    changeLanguage: (() => {
        cy.get(dropdown).scrollIntoView();
        cy.get(dropdown).click();
        cy.get(dropdown).shadowFind('div');
    }),
    checkHomepageSectionIsDisplayed: ((homepageSection, visible) => {
        cy.get(homepageSections.find((section) => section.name === homepageSection).selector).should(`be.${!visible ? 'not.' : ''}visible`);
    }),
    checkPaymentElements: (() => {
        PaymentMenuLinks.forEach((menuLink) => {
            cy.get(payment).children('.payment-method-item').get(menuLink.class).should('be.visible');
        });
    }),
    checkGameProvidersElements: (() => {
        gameProvidersIcon.forEach((menuLink) => {
            cy.get(game_providers).children('.game-provider-item').get(menuLink.class).should('be.visible');
        });
    }),
    checkGameProvidersElementsCA: (() => {
        gameProvidersIconCanada.forEach((menuLink) => {
            cy.get(game_providers).children('.game-provider-item').get(menuLink.class).should('be.visible');
        });
    })

};

export default homepagePom;