// eslint-disable-next-line @typescript-eslint/no-var-requires
const globalProperties = require('../config.js');
import languages from "../fixtures/languages.json";
import commonUtils from "../utils/commonUtils.js";
const landingPage = 'div.offer.page';
const topNavigation = '[data-v-test-id="top-navigation"]';
const offer = 'div.landing-offer';
const help = 'div.landing-offer__content > ul';
const desc = 'magic-ui-text';
const buttonCTA = 'magic-ui-button.landing-offer__button';
const buttonCTA_one = 'magic-ui-button.landing-offer__cover-cta';
const Tnc = 'magic-ui-accordion.landing-offer__accordion';
const banner = 'div.landing-offer__cover-background';
const titlebanner = 'magic-ui-text.landing-offer__cover-content-title';
const descBanner = 'magic-ui-text.landing-offer__cover-description';
const imageBanner = 'div.landing-offer__cover-content-image';
const hamburgerMenu = '[data-v-test-id="top-navigation-hamburger-menu"]';
const helpSteps = [
    { class: '.landing-offer__step', name: 'Step1' },
    { class: '.landing-offer__step', name: 'Step2' },
    { class: '.landing-offer__step', name: 'Step3' },
];

const landingPom = {
    goToLandingPage: (() => {
        const visitProfileURL = `${languages.English}${globalProperties.viewProfileURL}`;
        cy.authvisit(visitProfileURL);
        commonUtils.waitUntilLoadingScreenDisappears();
    }),

    checkIsnotLandingPage: (() => {
        cy.get(landingPage).should('not.exist');
    }),

    checkIsLandingPage: (() => {
        cy.get(landingPage).should('exist');
    }),
    checkTopNavigationPresent: (() => {
        cy.get(topNavigation).should('not.exist');
    }),
    checkIsOffer: (() => {
        cy.get(offer).should('exist');
    }),

    checkDescriptionExists: (() => {
        cy.get(desc).should('exist');
    }),
    checkHamburgerMenuPresent: (() => {
        cy.get(hamburgerMenu).should('not.exist');
    }),
    checkIsCTA: (() => {
        cy.get(buttonCTA).should('be.visible');
    }),
    checkIsCTA_one: (() => {
        cy.get(buttonCTA_one).should('be.visible');
    }),
    checkIsTnc: (() => {
        cy.get(Tnc).should('be.visible');
    }),
    checklandingBanner: (() => {
        cy.get(banner).should('be.visible');
    }),
    checktitlelandingBanner: (() => {
        cy.get(titlebanner).should('be.visible');
    }),
    checkdescriptionlandingBanner: (() => {
        cy.get(descBanner).should('be.visible');
    }),
    checkImagelandingBanner: (() => {
        cy.get(imageBanner).should('be.visible');
    }),

    checkIshelpSteps: (() => {
        helpSteps.forEach((element) => {
            cy.get(help).children(element.class).should('be.visible');

        });
    }),
};

export default landingPom;
