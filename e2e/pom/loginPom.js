// eslint-disable-next-line @typescript-eslint/no-var-requires
const globalProperties = require('../config.js');
import languages from "../fixtures/languages.json";
import commonUtils from "../utils/commonUtils.js";
const fetch = require('node-fetch');



const loginWindow = 'div.button.button--user-out';
const loggedInElement = '.top-navigation__balance';
const profilePage = '.profile.page';
const loginError = '[data-v-test-id=\'login-failed-message\']';
const animatedBallsHomepage = 'div.home-header__background';
const loginModal = 'div.login.page';
const loginButton = 'button[data-v-test-id="login-button"]';
const loginPom = {
    goToMegalottoPlatform: (() => {
        cy.authvisit('');
        commonUtils.waitUntilLoadingScreenDisappears();
    }),

    loginRequest: ((username, password) => {
        return cy.loginRequest(username, password);
    }),

    visitProfile: (() => {
        const visitProfileURL = `${languages.English}${globalProperties.viewProfileURL}`;
        cy.authvisit(visitProfileURL);
        commonUtils.waitUntilLoadingScreenDisappears();
    }),

    isUserLoggedIn: ((userSessionId) => {
        let isUserLoggedIn = false;
        const magicAPIURL = globalProperties.magicAPIURL;
        const magicUsersAPIURL = globalProperties.magicUsersAPIURL;
        const isUserLoggedInAPIURL = globalProperties.isUserLoggedInAPIURL;
        cy.request({
            method: 'POST',
            url: `${magicAPIURL}${magicUsersAPIURL}${isUserLoggedInAPIURL}`,
            headers: {
                'x-api-key': globalProperties.xApiKey,
                'sid': userSessionId
            }
        }).then((resp) => {
            isUserLoggedIn = resp.body.res;
        });
        return isUserLoggedIn;
    }),

    openLoginWindow: (() => {
        commonUtils.removeElement(animatedBallsHomepage);
        cy.get(loginWindow).click();
    }),

    checkUserIsLoggedIn: (() => {
        cy.get(loggedInElement).should('exist');
    }),

    checkIsProfilePage: (() => {
        cy.get(profilePage).should('exist');
    }),

    checkLoginFailed: (() => {
        getCypressAdaptor().getElement(loginError).should('exist');
    }),

    logOutUser: ((userSessionId) => {
        const magicAPIURL = globalProperties.magicAPIURL;
        const magicUsersAPIURL = globalProperties.magicUsersAPIURL;
        const logOutUserAPIURL = globalProperties.logOutUserAPIURL;
        cy.request({
            method: 'POST',
            url: `${magicAPIURL}${magicUsersAPIURL}${logOutUserAPIURL}`,
            headers: {
                'x-api-key': globalProperties.xApiKey,
                'sid': userSessionId
            }
        }).then((resp) => {
            expect(resp.status).to.eq(200);
        });
    }),

    checkLoginModalIsDisplayed: (() => {
        cy.get(loginModal).should('be.visible');
    }),

    clickLoginButton: (() => {
        getCypressAdaptor().getElement(loginButton).click();
    })

};

export default loginPom;
