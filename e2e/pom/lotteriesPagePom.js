const lotteriesPage = '.lotteries.page';
const lotteriesGrid = '.lotteries__grid';
const lotteryCard = '.lottery-card';
const lotteryDetailPage = '.lottery-detail.page';
const loadMoreLotteriesButton = '[data-v-test-id="lotteries-load-more-button"]';


const lotteriesPom = {
    checkIsLotteriesPage: (() => {
        cy.url().should('include', 'lotteries');
        cy.get(lotteriesPage).should('be.visible');
    }),

    clickLottery: ((lottery) => {
        const lotteryLink = `a[href*=${lottery}]`;
        cy.get(lotteriesPage).find(lotteriesGrid).find(lotteryCard).find(lotteryLink).first().click();
    }),

    checkIsLotteriesDetailsPage: ((lottery) => {
        cy.url().should('include', lottery);
        cy.get(lotteryDetailPage).should('exist');
    }),

    clickLoadMoreLotteriesButton: (() => {
        cy.get(loadMoreLotteriesButton).should('exist');
        cy.get(loadMoreLotteriesButton).scrollIntoView().click();
        cy.get(loadMoreLotteriesButton).should('not.exist');
    }),

    checkMoreLotteriesWereLoaded: (() => {
        cy.get(lotteriesGrid).find(lotteryCard).its('length').should('be.gt', 10);
    })
};

export default lotteriesPom;
