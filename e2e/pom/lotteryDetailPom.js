

const lotteryDetailPom = {
    // header: getSelectorFunction('lottery-detail-header'),
    // playButton: getSelectorFunction('lottery-detail-play-button', 'button'),
    // tabs: getSelectorFunction('lottery-detail-tabs', 'li'),
    // informationContent: getSelectorFunction('lottery-detail-information'),
    // resultsContent: getSelectorFunction('lottery-detail-results'),

    getInformationTab: () => lotteryDetailPom.tabs().eq(0),

    getResultsTab: () => lotteryDetailPom.tabs().eq(1),
};

export default lotteryDetailPom;
