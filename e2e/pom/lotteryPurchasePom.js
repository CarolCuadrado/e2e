import languages from "../fixtures/languages.json";

import commonUtils from "../utils/commonUtils.js";
/* eslint-disable @typescript-eslint/no-var-requires */
const globalProperties = require('../config.js');
/* eslint-enable @typescript-eslint/no-var-requires */
const purchaseModal = 'div.modal-ticket__ticket';
const userBalance = 'div.lottery-purchase__payment-method > magic-ui-text:nth-of-type(2)';
const purchaseButton = 'div.lottery-purchase__action > div magic-ui-button';
const GoHomeButton = '.lottery-purchased__back-button > magic-ui-button';
const registerButton = 'div.lottery-purchase__action-wrapper magic-ui-button:nth-child(1)';
const lotteryLinesWrapper = 'div.lottery-lines__number-wrapper ul';
const lotteryLinesWrapperdetails = 'div.lottery-line__number-wrapper ul';
const lotteryconfirmationPage = 'div.lottery-purchased.page';
const lotterypurchasedlink = '.lottery-purchased__link';
const lotterypurchasedjackpot = 'magic-ui-text.lottery-purchased__jackpot';
const lotterypurchasedtime = '.time-until-wrapper';
const lotterypurchasedshow = '.lottery-purchased__show-all-wrapper';
const lotterypurchasedback = '.lottery-purchased__back-button';
const lotterypurchasedline = '.lottery-line__number-wrapper';
const lotteryline = 'li.lottery-lines__number-item magic-ui-text';
const lotterylinedetails = 'li.lottery-line__number-item magic-ui-text';
const lotteryerrormessage = 'magic-ui-text';
const purchaseLotteryMap = [];
const paymentSection = 'div.lottery-purchase__selected-payment-method';
const loginLink = 'div.lottery-purchase__action-wrapper magic-ui-button:nth-child(2)';
const cardCVC = 'magic-ui-number.payment-cc__cvc-input input';
const pagesToAccessLotteryModal = [
    { url: '/lotteries', button: 'div.lotteries__grid > div:nth-of-type(1) .lottery-card__action magic-ui-button', name: 'lotteries page' },
    { url: '/lotteries/euromillions', button: 'div.lottery-card__logo-wrapper.lottery-card__logo-wrapper--clipped', name: 'lottery details' },
    { url: '/', button: 'div.home.page > div.home-section:nth-of-type(3) div.vueperslide.vueperslide--visible:nth-of-type(1) magic-ui-button.lottery-card__button', name: 'drawing soon section' },
    { url: '/', button: 'div.home.page > div.home-section:nth-of-type(4) div.vueperslide.vueperslide--visible:nth-of-type(1) magic-ui-button.lottery-card__button', name: 'recently played section' },
    { url: '/404', button: 'div.ending-soon magic-ui-button', name: 'ending soon widget' }
];
const purchaseLotteryPom = {
    mockUserBalance: ((amount) => {
        cy.fixture('userBalanceResponses').then((userBalanceResponses) => {
            userBalanceResponses.userBalance.response.res.accounts[0].amount = amount;
            cy.addRoute(globalProperties.gamingAccountsAPIURL,
                userBalanceResponses.userBalance.method,
                userBalanceResponses.userBalance.status,
                userBalanceResponses.userBalance.response,
                globalProperties.magicWalletAPIURL)
                .as('userBalance');
        });
    }),

    mockPaymentMethods: (() => {
        cy.fixture('userPaymentMethods').then((userPaymentMethods) => {
            cy.addRoute(globalProperties.paymentMethodWalletAPIURL,
                userPaymentMethods.paymentMethods.method,
                userPaymentMethods.paymentMethods.status,
                userPaymentMethods.paymentMethods.response,
                '',
                globalProperties.walletAPIURL)
                .as('userPaymentMethods');
        });
    }),

    mockUserWithCards: (() => {
        cy.fixture('userPaymentMethods').then((userPaymentOptions) => {
            cy.addRoute(globalProperties.accountWalletAPIURL,
                userPaymentOptions.userWithCards.method,
                userPaymentOptions.userWithCards.status,
                userPaymentOptions.userWithCards.response,
                '',
                globalProperties.walletAPIURL)
                .as('userCards');
        });
    }),

    mockUserWithNoCards: (() => {
        cy.fixture('userPaymentMethods').then((userPaymentOptions) => {
            cy.addRoute(globalProperties.accountWalletAPIURL,
                userPaymentOptions.userWithNoCards.method,
                userPaymentOptions.userWithNoCards.status,
                userPaymentOptions.userWithNoCards.response,
                '',
                globalProperties.walletAPIURL)
                .as('userCards');
        });
    }),

    mockSuccessfulLotteryPurchase: (() => {
        cy.fixture('lotteryResponses').then((lotteryResponses) => {
            cy.addRoute(globalProperties.createBetAPIURL,
                lotteryResponses.purchaseSuccessful.method,
                lotteryResponses.purchaseSuccessful.status,
                lotteryResponses.purchaseSuccessful.response,
                globalProperties.lotteriesAPIURL)
                .as('purchaseResponse');
        });
    }),
    registerGetOpenDrawsAPI: (() => {
        cy.fixture('lotteryResponses').then((lotteryResponses) => {
            cy.addRoute(globalProperties.getOpenDrawsAPIURL,
                lotteryResponses.getOpenDraws.method,
                lotteryResponses.getOpenDraws.status,
                lotteryResponses.getOpenDraws.response,
                '',
                globalProperties.lotteriesAPIURL).as('openDrawsAPI');
        });
    }),
    confirmationGetOpenDrawsAPI: (() => {
        cy.fixture('confirmationResponses').then((confirmationResponses) => {
            cy.addRoute(globalProperties.confirmationAPIURL,
                confirmationResponses.getUserBets.method,
                confirmationResponses.getUserBets.status,
                confirmationResponses.getUserBets.response,
                '',
                globalProperties.confirmationAPIURL).as('openDrawsAPI');
        });
    }),

    mockSucessfulDepositWithCard: (() => {
        cy.fixture('cardTransactions').then((cardTransactions) => {
            cy.addRoute(globalProperties.cardDepositWalletAPIURL,
                cardTransactions.successfulDeposit.method,
                cardTransactions.successfulDeposit.status,
                cardTransactions.successfulDeposit.response,
                '',
                globalProperties.walletAPIURL).as('successfulDepositWithCard');
        });
    }),

    accessLotteryModalFromPage: ((pageToAccessModal) => {
        const pageFound = pagesToAccessLotteryModal.find((page) => page.name === pageToAccessModal);
        const urlToNavigate = `${languages.English}${pageFound.url}`;
        cy.url().then((url) => {
            if (!url.endsWith(urlToNavigate)) {
                cy.authvisit(urlToNavigate);
            }
        });
        if (pageToAccessModal != 'lottery details') {
            cy.get(pageFound.button).click();
        }
    }),

    checkPurchaseModalIsDisplayed: (() => {
        cy.get(purchaseModal).should('be.visible');
    }),

    checkUserBalanceIsDisplayed: (() => {
        cy.get(userBalance).should('be.visible');
    }),
    checkIsconfirmationPage: ((lottery) => {
        cy.url().should('include', lottery);
    }),
    checkIsconfirmationticket: (() => {
        cy.get(lotteryconfirmationPage).should('exist');
    }),

    visitConfirmation: (() => {
        const visitProfileURL = `${languages.English}${globalProperties.confirmationAPIURL}`;
        cy.authvisit(visitProfileURL);
        commonUtils.waitUntilLoadingScreenDisappears();
    }),
    checkPurchaseButton: ((statusOfButton, contentOfButton) => {
        cy.get(purchaseButton).should('contain', contentOfButton);
    }),

    checkjackpotIsDisplayed: (() => {
        cy.get(lotterypurchasedjackpot).should('exist');
    }),

    clickPurchaseButton: (() => {
        cy.get(purchaseButton).click();
        cy.get(purchaseButton).click();
        cy.get(lotteryerrormessage).then(($btn) => {
            if ($btn.hasClass('lottery-purchase__error-message')) {
                cy.get(purchaseButton).click();
            }
        });
    }),

    clickRegisterButton: (() => {
        cy.get(registerButton).click();
    }),

    checkLotteryTicketWasPurchased: (() => {
        cy.get(purchaseModal).should('not.exist');
    }),

    saveGeneratedLines: (() => {
        purchaseLotteryMap.length = 0;
        cy.get(lotteryLinesWrapper).each((line) => {
            const lineWithNumbers = [];
            cy.get(line).find(lotteryline).each((number) => {
                cy.wrap(number).should('have.attr', 'content').then((content) => {
                    lineWithNumbers.push(content);
                });
            });
            purchaseLotteryMap.push(lineWithNumbers);
        });
    }),


    saveGeneratedLinesdetails: (() => {
        purchaseLotteryMap.length = 0;
        cy.get(lotteryLinesWrapperdetails).each((line) => {
            const lineWithNumbers = [];
            cy.get(line).find(lotterylinedetails).each((number) => {
                cy.wrap(number).should('have.attr', 'content').then((content) => {
                    lineWithNumbers.push(content);
                });
            });
            purchaseLotteryMap.push(lineWithNumbers);
        });
    }),
    checkNumberOfLotteryLines: ((numberOfLines) => {
        cy.get(lotteryLinesWrapper).should('have.length', numberOfLines);
    }),
    checkNumberOfLotteryLinesdetails: ((numberOfLines) => {
        cy.get(lotteryLinesWrapperdetails).should('have.length', numberOfLines);
    }),

    checkPaymentSectionIsNotDisplayed: (() => {
        cy.get(paymentSection).should('not.exist');
    }),

    checkLoginLinkIsDisplayed: (() => {
        cy.get(loginLink).should('exist');
    }),

    clickLoginLink: (() => {
        cy.get(loginLink).click();
    }),
    checklinkIsDisplayed: (() => {
        cy.get(lotterypurchasedlink).should('exist');
    }),

    checkShowAllIsDisplayed: (() => {
        cy.get(lotterypurchasedshow).should('exist');
    }),
    checklineIsDisplayed: (() => {
        cy.get(lotterypurchasedline).should('exist');
    }),

    checkRemainingIsDisplayed: (() => {
        cy.get(lotterypurchasedtime).should('exist');
    }),

    checkLotteryTicketIsTheSame: (() => {
        purchaseLotteryPom.checkNumberOfLotteryLines(purchaseLotteryMap.length);
        cy.get(lotteryLinesWrapper).each((line, lineIndex) => {
            cy.get(line).find(lotteryline).each((number, numberIndex) => {
                cy.wrap(number).should('have.attr', 'content', purchaseLotteryMap[lineIndex][numberIndex]);
            });
        });
    }),

    checkPaymentSectionWithCardIsDisplayed: (() => {
        cy.get(paymentSection).should('exist');
    }),
    checkPurchasedGoHome: (() => {
        cy.get(lotterypurchasedback).should('be.visible');
    }),
    clickGoHome: (() => {
        cy.get(GoHomeButton).click();

    }),
    chekisGoHome: (() => {
        cy.get('.home.page').should('be.visible');
    }),

    enterCardCVC: ((CVCCode) => {
        getCypressAdaptor().getElement(cardCVC).type(CVCCode, { force: true });;
    })

};
export default purchaseLotteryPom;
