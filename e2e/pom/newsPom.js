const newsPage = '.news.page';
const newsGrid = '.news-grid';
const newsCard = '.news-item';
const newsImg = '.news-item__cover>div>magic-ui-link>img';
const newsDescription = '.news-item__description';
const loadMoreNewsButton = 'magic-ui-button.news-grid__action-button';
const newsPom = {
    checkIsnewsPage: (() => {
        cy.url().should('include', 'news');
        cy.get(newsPage).should('be.visible');
    }),
    checkLoadMoreNewsButton: (() => {
        cy.get(loadMoreNewsButton).should('exist');
        cy.get(loadMoreNewsButton).scrollIntoView();
    }),
    clickLoadMoreNewsButton: (() => {
        cy.get(loadMoreNewsButton).click();
    }),
    checkMoreNewsWereLoaded: ((num) => {
        cy.get(newsGrid).get(newsCard).should('have.length', num);
    }),
    checkEachImageNews: (() => {
        cy.get(newsGrid).find(newsCard).find(newsImg).should('be.visible');
    }),
    checkEachDescNews: (() => {
        cy.get(newsGrid).find(newsCard).find(newsDescription).should('be.visible');
    }),
    clickNews: (() => {
        cy.get(newsGrid).find(newsCard).find('.news-item__cover>div>magic-ui-link').first().click();
    })
};

export default newsPom;
