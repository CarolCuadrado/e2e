import commonUtils from "../utils/commonUtils.js";
import loginPom from "./loginPom.js";

const profileOverlayMenu = '.profile-overlay';
const profileOverlay = '.profile-overlay';
const wallet = 'magic-wallet';
const profileOverlayLinks = {
    depositLink: '.balance magic-ui-button',
    walletLink: 'a[href*="wallet"]',
    profileLink: 'a[href*="profile"]',
    messagesLink: 'a[href*="messages"]',
    promoLink: 'a[href*="promo"]',
    accountLink: 'a[href$="account/"]',
    logoutLink: 'a[href*="logout"]',
};
const messageNumber = '[data-v-test-id="profile-overlay-message-number"]';
const closure ='[data-v-tet-id="account-title"]';

const profileOverlayPom = {
    isProfileOverlayMenuOpen: (() => {
        cy.get(profileOverlayMenu).should('be.visible');
    }),

    clickOption: ((option) => {
        cy.get(profileOverlay).find(profileOverlayLinks[`${option}Link`]).click();
    }),

    isWalletOpen: (() => {
        cy.get(wallet).should('be.visible');
    }),

    isUserLoggedOut: (() => {
        let isUserLoggedOut = false;
        cy.get(profileOverlay).should('not.be.visible').then(() => {
            if (!loginPom.isUserLoggedIn(commonUtils.getGlobalProperty('currentUserSessionId'))) {
                isUserLoggedOut = true;
            }
            expect(isUserLoggedOut).to.be.true;
        });
    }),

    checkProfileOverlayElements: (() => {
        Object.values(profileOverlayLinks).forEach((menuLink) => {
            cy.get(profileOverlay).within(() => {
                cy.get(menuLink).should('be.visible');
            });
        });
    }),

    isClosureAccount: (() => {
        cy.get(closure).should('not.exist');
    }),


    isMessageNotificationVisible: (() => {
        cy.get(messageNumber).should('be.visible');
    }),

    isMessageNotificationNotPresent: (() => {
        cy.get(messageNumber).should('not.exist');
    })
};

export default profileOverlayPom;
