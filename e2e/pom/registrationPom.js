
import signUpPayload from "../fixtures/signUpPayload.json";
import commonUtils from '../utils/commonUtils.js';


const registrationLink = '.sign-up__button';
const registrationWindow = '.regily-module';
const registrationPom = {
    openRegistrationWindow: (() => {
        cy.clickForce(registrationLink);
    }),

    checkRegistationWindowIsDisplayed: (() => {
        cy.get(registrationWindow).should('be.visible');
    }),

    registerRandomUser: (() => {
        const userDataObject = commonUtils.getRandomUserData();
        signUpPayload.authModel = { ...signUpPayload.authModel, ...userDataObject };
        cy.registerUser(signUpPayload);
    }),

    // form: getSelectorFunction('sign-up-form'),
    // cover: getSelectorFunction('sign-up-cover'),
    // progressBar: getSelectorFunction('sign-up-progress-bar'),
    // // TODO: move this selector to Modal COM, as it is a generic component, not sign-up exclusive
    // closeButton: getSelectorFunction('modal-close-button', 'magic-ui-icon'),
    // message: getSelectorFunction('sign-up-message', 'h4'),
    // step1: getSelectorFunction('sign-up-progress-bar-step-1'),
    // step2: getSelectorFunction('sign-up-progress-bar-step-2'),
    // step3: getSelectorFunction('sign-up-progress-bar-step-3'),
    // confirmationContainer: getSelectorFunction('confirmation'),
    // confirmationQuestion: getSelectorFunction('confirmation-question'),
    // confirmationInfo: getSelectorFunction('confirmation-info'),
    // notificationError: getSelectorFunction('notification-text'),
    // confirmationContinueButtonContainer: getSelectorFunction('confirmation-continue-button'),
    // confirmationCancelButtonContainer: getSelectorFunction('confirmation-cancel-button'),
    // confirmationContinueButton: getSelectorFunction('confirmation-continue-button', 'button'),
    // confirmationCancelButton: getSelectorFunction('confirmation-cancel-button', 'button'),
    
    completeStep1(userData, chain) {
        within(($subject) => {
            SignUpStep1.email($subject).type(userData.email,{ force: true }); 
            SignUpStep1.password($subject).type(userData.password,{ force: true });

        }, chain);
    },

    completeStep2(userData, chain) {
        within(($subject) => {
            SignUpStep2.firstName($subject).type(userData.firstName,{ force: true });
            SignUpStep2.lastName($subject).type(userData.lastName,{ force: true });

            UiDate.day($subject).type(userData.birthday.day, {force: true });
            UiDate.month($subject).type(userData.birthday.month, {force: true });
            UiDate.year($subject).type(userData.birthday.year, {force: true });
           
                SignUpStep2.birthday($subject),
            

            SignUpStep2.address($subject).type(userData.address,{ force: true });
            SignUpStep2.city($subject).type(userData.city,{ force: true });
            SignUpStep2.country($subject).select(userData.country,{ force: true });
            SignUpStep2.postcode($subject).type(userData.postcode,{ force: true });
            SignUpStep2.phonePrefix($subject).select(userData.phonePrefix,{ force: true });
            SignUpStep2.phoneNumber($subject).type(userData.phoneNumber,{ force: true });

            if (userData.agreeAllTerms) {
                SignUpStep2.agreeAllTerms($subject).check({ force: true });
            }

            if (userData.agreeAllMarketing) {
                SignUpStep2.agreeAllMarketing($subject).check({ force: true });
            }
        }, chain);
    },
};

export default registrationPom;
