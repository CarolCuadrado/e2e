
const container ='div.rg-questionnaire__wrapper';
const questionnaire='magic-rg-questionnaire';
const title='magic-ui-text.title';
const text='magic-ui-text.text';
const resultext='div.action.action--info';
const closeButton='magic-ui-icon.icon';

const rgquestionnairePom = {
    CheckisQuestionnaire: (() => {
        cy.get(container).should('be.visible');
    }),
    CheckisTitleQuestionnaire: (() => {
        cy.get(questionnaire).shadowFind(title).scrollIntoView().should('exist');
    }),
    CheckisTextQuestionnaire: (() => {
        cy.get(questionnaire).shadowFind(text).scrollIntoView().should('exist');
    }),
    CheckisResultTextQuestionnaire: (() => {
        cy.get(questionnaire).shadowFind(resultext).scrollIntoView().should('exist');
    }),
    closeButtonQuestionnaire: (() => {
        cy.get(closeButton).should('be.visible');
    }),
   
};

export default rgquestionnairePom;