
import commonUtils from "../utils/commonUtils.js";

const topNavigation = 'nav.navigation';

const topNavigationMenuLinks = [
    { key: 'home', link: '.brand', name: 'home' },
    { key: 'lotteries', link: 'a[href*="lotteries"]', name: 'lotteries' },
    { key: 'scratchcards', link: 'a[href*="scratchcards"]', name: 'scratchcards' },
    { key: 'liveCasino', link: 'a[href*="/casino/live-casino"]', name: 'live-casino' },
    { key: 'casino', link: 'a[href$="casino"]', name: 'casino' },
    { key: 'promotions', link: 'a[href*="promo"]', name: 'promo' },
    { key: 'news', link: 'a[href*="news"]', name: 'news' }
];

const loginDesktopButton = 'div.authentication>magic-ui-button:nth-child(2)';
const signupDesktopButton = '[data-v-test-id="top-navigation-signup-desktop"]';
const loginMobileButton = '[data-v-test-id="top-navigation-login-mobile"]';
const signupMobileButton = '[data-v-test-id="top-navigation-signup-mobile"]';
const hamburgerMenuIcon = '[data-v-test-id="top-navigation-hamburger-menu"]';
const unreadMessagesNotification = '[data-v-test-id="top-navigation-unread-messages"]';
const buttonoverlay = '[data-v-test-id="top-navigation-profile-button"]';
const loginbuttonNavigation = 'div.top-navigation__buttons > magic-ui-button:nth-child(1)';
const depositbuttonNavigation = 'div.top-navigation__buttons > div:nth-child(1) > magic-ui-button';
const loginModal='magic-ui-modal';

const topNavigationPom = {

    // profileOverlayButton: getSelectorFunction('top-navigation-profile-button', 'button'),

    clickOption: ((option) => {
        cy.get(topNavigation).find(topNavigationMenuLinks.find((item) => item.name === option).link).click();
    }),

    checkTopNavigationElements: (() => {
        topNavigationMenuLinks.forEach((menuLink) => {
            cy.get(topNavigation).find(menuLink.link).should('be.visible');
        });
    }),

    isTopNavigationMenuPresent: (() => {
        cy.get(topNavigation).should('be.visible');
    }),

    isloginPresent: (() => {
        cy.get(loginbuttonNavigation).should('be.visible');
    }),
    isdepositPresent: (() => {
        cy.get(depositbuttonNavigation).should('be.visible');
    }),

    loginModal: (() => {
        cy.get(loginbuttonNavigation).click();
    }),
    loginOnDesktop: () => cy.get(loginDesktopButton).click(),
    signupOnDesktop: () => cy.get(signupDesktopButton).click(),
    loginOnMobile: () => cy.get(loginMobileButton).click(),
    signupOnMobile: () => cy.get(signupMobileButton).click(),
    hamburgerMenu: () => cy.get(hamburgerMenuIcon).click(),

    openProfileOverlayMenu: (() => {
        cy.get(buttonoverlay).click({ force: true });
        commonUtils.waitUntilLoadingScreenDisappears();
    }),

    isMessageNotificationVisible: (() => {
        cy.get(unreadMessagesNotification).should('be.visible');
    }),

    isMessageNotificationNotPresent: (() => {
        cy.get(unreadMessagesNotification).should('not.exist');
    }),
    isloginModalPresent: (() => {
        cy.get(loginModal).should('be.visible');
    }), 

};

export default topNavigationPom;
