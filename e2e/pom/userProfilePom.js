import commonUtils from "../utils/commonUtils.js";


/* eslint-disable @typescript-eslint/no-var-requires */
const globalProperties = require('../config.js');
/* eslint-enable @typescript-eslint/no-var-requires */
const profilePage = '.profile.page';
const expandProfileButton = 'magic-ui-accordion.accordion--profile';
const profileAccordionContent = '[data-v-test-id=\'accordion-content\']';
const luckyNumber = 'div.lucky-number h3';
const editUserInfoIcon = 'magic-ui-box.card magic-ui-button';
const editLuckyNumberIcon = 'magic-ui-button.lucky-number__button';
const luckyNumberInput = 'magic-ui-number.lucky-number-input input';
const saveLuckyNumberButton = 'div.lucky-number div.action magic-ui-button';
const editPasswordButton = 'div.form  magic-ui-form-field:nth-of-type(7) button';
const changePasswordPage = 'div.change-password';
const confirmationPasswordField = 'magic-ui-peek-password input';
const savePasswordButton = 'div.action magic-ui-button';
const errorField = 'magic-ui-text[data-v-test-id="message-test"] div';
const userProfileSections = [
    { key: 'luckyNumber', selector: 'div.lucky-number', name: 'lucky number' },
    { key: 'userName', selector: 'magic-ui-text.name', name: 'user name' },
    { key: 'userAddress', selector: 'magic-ui-accordion.accordion--profile div.item:nth-of-type(1)', name: 'user address' },
    { key: 'userMail', selector: 'magic-ui-accordion.accordion--profile div.item:nth-of-type(2) magic-ui-text:nth-of-type(2)', name: 'user mail' },
    { key: 'userPhoneNumber', selector: 'magic-ui-accordion.accordion--profile div.item:nth-of-type(2) magic-ui-text:nth-of-type(3)', name: 'user phone number' }
];
const passwordFields = [
    { selector: 'form.form magic-ui-peek-password:nth-of-type(1) input', name: 'oldPasswordField' },
    { selector: 'form.form magic-ui-peek-password:nth-of-type(2) input', name: 'newPasswordField' },
    { selector: 'form.form magic-ui-peek-password:nth-of-type(3) input', name: 'repeatedPasswordField' }
];
const userProfilePom = {

    checkProfilePageIsDisplayed: (() => {
        cy.get(profilePage).should('be.visible');
    }),

    checkUserProfileSectionIsDisplayed: ((userProfileSection, visible) => {
        cy.get(userProfileSections.find((section) => section.name === userProfileSection).selector).should(`be.${!visible ? 'not.' : ''}visible`);
    }),

    checkUserInformationIsCollapsed: (() => {
        cy.get(profileAccordionContent).should('not.exist');
    }),

    expandUserDetails: (() => {
        cy.get(expandProfileButton).click();
    }),

    setLuckyNumberWithRequest: ((luckyNumber) => {
        const userSessionId = commonUtils.getGlobalProperty('currentUserSessionId');
        const magicAPIURL = globalProperties.magicAPIURL;
        const magicUsersAPIURL = globalProperties.magicUsersAPIURL;
        const userPreferencesURL = globalProperties.userPreferencesURL;
        cy.request({
            method: 'POST',
            url: `${magicAPIURL}${magicUsersAPIURL}${userPreferencesURL}`,
            headers: {
                'x-api-key': globalProperties.xApiKey,
                'sid': userSessionId
            },
            body: {
                preference: { luckyNumber }
            }
        }).then((resp) => {
            expect(resp.status).to.eq(200);
            expect(resp.body.res).to.eq(null);
        });
    }),

    checkValueLuckyNumber: ((value) => {
        getCypressAdaptor().getElement(luckyNumber).should(`${value === null ? 'not.' : ''}have.attr`, 'content', `${value}`);
    }),

    clickEditDetails: (() => {
        cy.get(editUserInfoIcon).click();
    }),

    clickEditLuckyNumber: (() => {
        cy.get(editLuckyNumberIcon).click();
    }),

    checkEditLuckyNumberPageIsDisplayed: (() => {
        getCypressAdaptor().getElement(luckyNumberInput).should('be.visible');
        commonUtils.checkUserWasProperlyRedirected(globalProperties.editLuckyNumberURL);
    }),

    clearLuckyNumberInput: (() => {
        getCypressAdaptor().getElement(luckyNumberInput).clear({force: true});
    }),

    setLuckyNumber: ((luckyNumber) => {
        cy.typeForce(luckyNumberInput,luckyNumber);
    }),

    clickSaveLuckyNumberButton: (() => {
        cy.clickForce(saveLuckyNumberButton);
    }),

    checkEditProfilePageIsDisplayed: (() => {
        getCypressAdaptor().getElement(editLuckyNumberIcon).should('be.visible');
        commonUtils.checkUserWasProperlyRedirected(globalProperties.editProfileURL);
    }),

    checkLuckyNumberWasProperlySaved: ((savedLuckyNumber) => {
        userProfilePom.checkValueLuckyNumber(savedLuckyNumber);
    }),

    enterConfirmationPassword: ((password) => {
        cy.typeForce(confirmationPasswordField,password);
    }),

    mockEmailChange: (() => {
        cy.fixture('editProfileResponses').then((editProfileResponses) => {
            cy.addRoute(globalProperties.changeEmailAPIURL,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.method,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.status,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.response)
                .as('emailSucessfullyChanged');
        });
    }),

    mockIsUserLoggedIn: (() => {
        cy.fixture('loggedInUserResponses').then((loggedInUserResponses) => {
            cy.addRoute(globalProperties.isUserLoggedInAPIURL,
                loggedInUserResponses.userIsNotLoggedIn.method,
                loggedInUserResponses.userIsNotLoggedIn.status,
                loggedInUserResponses.userIsNotLoggedIn.response)
                .as('userIsNotLoggedIn');
        });
    }),

    clickEditPasswordButton: (() => {
        cy.clickForce(editPasswordButton);
    }),

    checkEditPasswordPageIsDisplayed: (() => {
        getCypressAdaptor().getElement(changePasswordPage).should('be.visible');
    }),

    enterPassword: ((field, password) => {
        getCypressAdaptor()
            .getElement(passwordFields.find((element) => element.name === `${field}PasswordField`).selector)
            .type(password,{force: true});
    }),

    clickSavePasswordButton: (() => {
        cy.clickForce(savePasswordButton);
    }),

    mockPasswordChange: (() => {
        cy.fixture('editProfileResponses').then((editProfileResponses) => {
            cy.addRoute(globalProperties.changePasswordAPIURL,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.method,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.status,
                editProfileResponses.emailOrPasswordSuccessfullyChanged.response)
                .as('passwordSucessfullyChanged');
        });
    }),

    checkSavePasswordButtonIsDisabled: (() => {
        getCypressAdaptor().getElement(savePasswordButton).should('have.attr', 'disabled');
    }),

    checkErrorMessageIsDisplayed: ((errorMessage) => {
        cy.focused().blur();
        getCypressAdaptor().getElement(errorField).should('contain', errorMessage);
    })

};

export default userProfilePom;
