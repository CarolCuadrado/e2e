import { Then } from "cypress-cucumber-preprocessor/steps";
import profileOverlayPom from "../pom/profileOverlayPom.js";

Then('the closure account section is not displayed', () => {
     profileOverlayPom.isClosureAccount();
});