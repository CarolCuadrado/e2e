import { Then } from "cypress-cucumber-preprocessor/steps";
import casinoLobbyPom from "../pom/casinoLobbyPom.js";


Then('scratchcards page on casino lobby is displayed', () => {
    casinoLobbyPom.checkIsScratchCardsPage();
});

Then('live casino page on casino lobby is displayed', () => {
    casinoLobbyPom.checkIsInstantWinsPage();
});

Then('casino games page on casino lobby is displayed', () => {
    casinoLobbyPom.checkIsCasinoGamesPage();
});

Then('Casino lobby header is displayed', () => {
    casinoLobbyPom.checkHeaderIsDisplayed();
});

Then('Games search field is displayed', () => {
    casinoLobbyPom.checkSearchIsDisplayed();
});

Then('Link to promotions is displayed', () => {
    casinoLobbyPom.checkPromoLinkIsDisplayed();
});

Then('Game categories menu is displayed on desktop', () => {
    casinoLobbyPom.checkDesktopCategoriesMenuIsDisplayed();
});

Then('Game categories menu is displayed on mobile', () => {
    casinoLobbyPom.checkMobileCategoriesMenuIsDisplayed();
});

Then('Last played games are {string}', (status) => {
    if (status === 'displayed') {
        RecentGames.container().should('be.visible');
    } else if (status === 'not displayed') {
        RecentGames.container().should('not.exist');
    } else {
        throw new Error('Invalid status');
    }
});
Then('Games are displayed', () => {
    Game.container().should('have.length.greaterThan', 0);
});

Then('Scratchcard games are displayed', () => {
    casinoLobbyPom.gameCardContainer().should('have.length.greaterThan', 0);
});
