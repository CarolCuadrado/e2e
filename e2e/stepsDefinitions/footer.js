import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import footerPom from "../pom/footerPom.js";
import commonUtils from "../utils/commonUtils.js";

When('the user scrolls down to the footer', () => {
    footerPom.scrollToFooter();
});

Then('footer elements are displayed', () => {
    footerPom.checkFooterElements();
});

And('the user clicks {string} option in the footer', (footerOption) => {
    footerPom.clickOption(footerOption);
});

Then('the user was properly redirected to {string} page', (footerOption) => {
    commonUtils.checkUserWasProperlyRedirected(footerOption);
});

Then('Payment elements footer are displayed', () => {
    footerPom.checkFooterPaymentElements();
    });
    