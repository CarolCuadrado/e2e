import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import forgottenPasswordPom from "../pom/forgottenPasswordPom.js";
import mockUtils from "../utils/mockUtils.js";
import commonUtils from "../utils/commonUtils.js";

And('the user clicks on the forgot password link', () => {
    forgottenPasswordPom.clickForgotPasswordLink();
});

Then('the reset password window is displayed', () => {
    forgottenPasswordPom.checkForgotPasswordModalIsDisplayed();
});

When('the user enters a valid registered email {string} in the field', (email) => {
    forgottenPasswordPom.enterEmailInForgotPassword(email);
});

Then('the send button is enabled', () => {
    forgottenPasswordPom.checkSendButtonIsEnabled();
});

When('the user clicks on the send button', () => {
    forgottenPasswordPom.clickOnSendButton();
});

Then('an email is sent to the account entered', () => {
    //It is not necessary to implement, just to clarify
});

And('a success window with a link to home page is displayed', () => {
    forgottenPasswordPom.checkEmailSentModalIsDisplayed();
});

When('the user clicks on the link received in the email', () => {
    mockUtils.startServer();
    forgottenPasswordPom.mockSuccessfulPasswordChange();
});

Then('the user is redirected to the platform', () => {
    forgottenPasswordPom.goToEnterNewPasswordModal();
});

Then('the change password window is displayed', () => {
    forgottenPasswordPom.checkEnterNewPasswordModalIsDisplayed();
});

When('the user enters a new password {string} twice in the fields', (password) => {
    forgottenPasswordPom.enterNewPassword(password);
    forgottenPasswordPom.enterConfirmPassword(password);
});

Then('the change password button is enabled', () => {
    forgottenPasswordPom.checkChangePasswordButtonIsEnabled();
});

When('the user clicks on change password button', () => {
    forgottenPasswordPom.clickOnChangePasswordButton();
});

Then('the new password is properly set', () => {
    //It is not necessary to implement, just to clarify
});

Then('a success window with a link to login page is displayed', () => {
    forgottenPasswordPom.checkSucessPasswordChangeModalIsDisplayed();
});

And('there is a button redirecting to login modal', () => {
    forgottenPasswordPom.checkLoginButtonIsDisplayed();
});

Given('a user who already received the reset password link but has not been used', () => {
    //It is not necessary to implement, just to clarify
});

When('the user opens the link after 24 hours', () => {
    mockUtils.startServer();
    forgottenPasswordPom.mockUnsuccessfulPasswordChange();
});

Then('an error message is shown {string}', (message) => {
    commonUtils.checkNotification(message);
});

