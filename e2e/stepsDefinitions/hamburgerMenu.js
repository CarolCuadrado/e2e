import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import hamburgerMenuPom from "../pom/hamburgerMenuPom.js";

Then('the hamburger menu is open', () => {
    hamburgerMenuPom.isHamburgerMenuOpen();
});

And('the hamburger menu constains all the correct links', () => {
    hamburgerMenuPom.checkHamburgerMenuElements();
});

And('the hamburger menu is no longer visible', () => {
    hamburgerMenuPom.isHamburgerMenuClosed();
});

When('the user clicks {string} on the hamburger menu', (hamburgerMenuOption) => {
    hamburgerMenuPom.clickOption(hamburgerMenuOption);
});

When('the user clicks on registration button', () => {
    hamburgerMenuPom.clickSignup();
});