import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import homepagePom from "../pom/homepagePom.js";
import { getUrl, URLS } from "../utils/urls";
When('the user accesses the homepage', () => {
   homepagePom.goToHomepage();
});

Then('the user enter as {string}', (language) => {
    switch (language) {
        case 'Germany':
            homepagePom.changeLanguage();
            cy.visit(getUrl(URLS.GERMAN));
            break;
        case 'Canada':
            homepagePom.changeLanguage();
            cy.authvisit(getUrl(URLS.CANADA));
            break;
        case 'Norway':
            homepagePom.changeLanguage();
            cy.authvisit(getUrl(URLS.NORWAY));
            break;
        case 'NewZeland':
            homepagePom.changeLanguage();
            cy.authvisit(getUrl(URLS.NEWZELAND));
            break;
            case 'Finland':
                homepagePom.changeLanguage();
            cy.authvisit(getUrl(URLS.FINLAND));
            break;
        default:
            break;
    }

   
});

Then('the homepage is displayed', () => {
    homepagePom.checkHomepageIsDisplayed();
});

And('the homepage contains the {string}', (homepageOption) => {
    homepagePom.checkHomepageSectionIsDisplayed(homepageOption, true);
});


And('the homepage does not contain the {string}', (homepageOption) => {
    homepagePom.checkHomepageSectionIsDisplayed(homepageOption, false);
});

Then('Payment methods section {string} displayed', (status) => {
    if (status === 'is') {
        homepagePom.paymentMethods().should('be.visible');
    } else if (status === 'is not') {
        homepagePom.paymentMethods().should('not.exist');
    } else {
        throw new Error('Invalid status');
    }
});

Then('Payment elements are displayed', () => {
    homepagePom.checkPaymentElements();
});


Then('the game providers icons are displayed for {string}', (language) => {
    if(language === 'Canada'){
      homepagePom.checkGameProvidersElementsCA();
    } else {
    homepagePom.checkGameProvidersElements();
    }
});