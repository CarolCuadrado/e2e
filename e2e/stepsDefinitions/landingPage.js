import { And, Then } from "cypress-cucumber-preprocessor/steps";
import landingPom from "../pom/landingPom.js";

And('user is redirected to {string} page', (page) => {
    if (page === 'landing') {
        landingPom.checkIsLandingPage();
    } else if (page === 'home') {
        landingPom.checkIsnotLandingPage();
    }
});

Then('navigation options are not displayed', () => {
    landingPom.checkTopNavigationPresent();
});

Then('hamburger menu icon is not displayed', () => {
    landingPom.checkHamburgerMenuPresent();
});

Then('landing page offer is displayed', () => {
    landingPom.checkIsOffer();
});

Then('landing page description is displayed', () => {
    landingPom.checkDescriptionExists();
});

Then('landing page banner is displayed', () => {
    landingPom.checklandingBanner();
});

Then('title of landing page banner is displayed', () => {
    landingPom.checktitlelandingBanner();
});
Then('description of landing page banner is displayed', () => {
    landingPom.checkdescriptionlandingBanner();
});

Then('image of landing page banner is displayed', () => {
    landingPom.checkImagelandingBanner();
});

Then('landing page help steps are displayed', () => {
    landingPom.checkIshelpSteps();
});
Then('landing page second CTA is displayed', () => {
    landingPom.checkIsCTA();
});
Then('first CTA of landing page banner is displayed', () => {
    landingPom.checkIsCTA_one();
});

Then('landing page TnC is displayed', () => {
    landingPom.checkIsTnc();
});

