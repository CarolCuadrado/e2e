import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";
import loginPom from "../pom/loginPom.js";
import users from "../fixtures/users.json";
import signUpPayload from "../fixtures/signUpPayload.json";
import commonUtils from "../utils/commonUtils.js";
import userProfilePom from "../pom/userProfilePom.js";
const fetch = require('node-fetch');

Given('a logged {string} user in megalotto platform', (statusOfTheUser) => {
    if (statusOfTheUser === 'in') {
        loginPom.loginRequest(users.Qatar.email, users.Qatar.password);
    }
    loginPom.goToMegalottoPlatform();
});

Then('User is logged out', () => {
    // It is not necessary to implement, just to clarify
});

When('the user enters his credentials', () => {
    LoginForm.emailInput().type(users.Qatar.email);
    LoginForm.passwordInput().type(users.Qatar.password);
});
When('the user enters his credentials to purchase', () => {
    LoginForm.emailInput().type(users.Withmoney.email);
    LoginForm.passwordInput().type(users.Withmoney.password);
});

And('the user clicks on login button', () => {
    loginPom.clickLoginButton();
});

Then('the user is logged in properly', () => {
    loginPom.checkUserIsLoggedIn();
});

When('the user visits his profile', () => {
    loginPom.visitProfile();
});

Then('the profile page is displayed properly', () => {
    loginPom.checkIsProfilePage();
});

When('the user enters invalid credentials', () => {
    LoginForm.emailInput().type(users.Fake.email);
    LoginForm.passwordInput().type(users.Fake.password);
});

Then('the login failed', () => {
    LoginForm.emailInput().should('exist');
    loginPom.checkLoginFailed();
});

When('enters his email {string} and password {string}', (userEmail, userPassword) => {
    LoginForm.emailInput().type(userEmail);
    LoginForm.passwordInput().type(userPassword);
});

When('enters his credentias for the just created account', () => {
    LoginForm.emailInput().type(commonUtils.getGlobalProperty("registeredUser"));
    LoginForm.passwordInput().type(signUpPayload.authModel.password);
});

When('the user who is {string} enters his credentials', (statusOfUser) => {
    LoginForm.emailInput().type(users[statusOfUser].email);
    LoginForm.passwordInput().type(users[statusOfUser].password);
});

Given('a logged in user in megalotto platform who has {string} a lottery ticket', (purchased) => {
    const user = purchased === 'purchased' ? users.PurchasedLotteries : users.NoPurchasedLotteries;
    loginPom.loginRequest(user.email, user.password);
    loginPom.goToMegalottoPlatform();
});
Given('a logged in user in megalotto platform who has money', () => {

    loginPom.loginRequest(user.Withmoney.email, user.Withmoney.password);
    loginPom.goToMegalottoPlatform();
});

Given('a logged in user in megalotto platform who has {string} the lucky number previously', (set) => {
    const luckyNumber = set === 'set' ? 99 : null;
    loginPom.loginRequest(users.Qatar.email, users.Qatar.password).then(() => {
        userProfilePom.setLuckyNumberWithRequest(luckyNumber);
    });
    loginPom.goToMegalottoPlatform();
});

Then('the login modal is displayed', () => {
    loginPom.checkLoginModalIsDisplayed();
});

Given('User logs in with account {string} unread messages', (condition) => {
    const user = condition === 'with' ? users.WithUnreadMessages : users.WithoutUnreadMessages;
    loginPom.loginRequest(user.email, user.password);
    loginPom.goToMegalottoPlatform();
});
