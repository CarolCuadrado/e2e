import { When, Then } from "cypress-cucumber-preprocessor/steps";
import lotteriesPagePom from "../pom/lotteriesPagePom.js";

Then('lotteries page is displayed', () => {
    lotteriesPagePom.checkIsLotteriesPage();
});

When('the user clicks on {string} lottery', (lottery) => {
    lotteriesPagePom.clickLottery(lottery);
});

When('the user is redirected to {string} lottery details', (lottery) => {
    lotteriesPagePom.checkIsLotteriesDetailsPage(lottery);
});

When('the user clicks on load more lotteries button', () => {
    lotteriesPagePom.clickLoadMoreLotteriesButton();
});

When('the remaining amount of lotteries are loaded', () => {
    lotteriesPagePom.checkMoreLotteriesWereLoaded();
});
