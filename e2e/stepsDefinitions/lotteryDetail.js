import lotteryDetailPom from "../pom/lotteryDetailPom.js";
import { Then } from "cypress-cucumber-preprocessor/steps";

Then('Header is displayed in lottery details page', () => {
    lotteryDetailPom.header().should('be.visible');
});

Then('Play button is displayed', () => {
    lotteryDetailPom.playButton().should('be.visible');
});

Then('Tabs are displayed', () => {
    lotteryDetailPom.tabs().should('be.visible');
});

Then('User clicks on information tab', () => {
    lotteryDetailPom.getInformationTab().click();
});

Then('User clicks on results tab', () => {
    lotteryDetailPom.getResultsTab().click();
});

Then('Information section is displayed', () => {
    lotteryDetailPom.informationContent().should('be.visible');
});

Then('Results section is displayed', () => {
    lotteryDetailPom.resultsContent().should('be.visible');
});
