import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";
import lotteryPurchasePom from "../pom/lotteryPurchasePom.js";
import mockUtils from "../utils/mockUtils.js";

const correctCardCVC = 123;

Given('a user who does not have enough money on wallet to buy a lottery ticket', () => {
    lotteryPurchasePom.mockUserBalance(1);
    lotteryPurchasePom.mockSuccessfulLotteryPurchase();
});

And('the user does not have any card registered', () => {
    lotteryPurchasePom.mockPaymentMethods();
    lotteryPurchasePom.mockUserWithNoCards();
});

When('the user clicks on a play button on {string}', (pageToAccessModal) => {
    lotteryPurchasePom.registerGetOpenDrawsAPI();
    lotteryPurchasePom.accessLotteryModalFromPage(pageToAccessModal);
});

When('confirmation page appears', () => {
    lotteryPurchasePom.confirmationGetOpenDrawsAPI();
    lotteryPurchasePom.checkIsconfirmationPage('lottery-purchased');
    lotteryPurchasePom.checkIsconfirmationticket();
});

Then('it shows the remaining time until next draw on the right', () => {
    lotteryPurchasePom.checkRemainingIsDisplayed();
});

Then('it shows the back button', () => {
    lotteryPurchasePom.checkPurchasedGoHome();
});

Then('the user is redirect to home', () => {
    lotteryPurchasePom.chekisGoHome();
});

Then('it displays the next jackpot on the left', () => {
    lotteryPurchasePom.checkjackpotIsDisplayed();
});
Then('it displays the link to see the tickets', () => {
    lotteryPurchasePom.checklinkIsDisplayed();
});

Then('click on back home', () => {
    lotteryPurchasePom.clickGoHome();
});
Then('it displays lottery lines and a button to expand and see all', () => {
    lotteryPurchasePom.checkShowAllIsDisplayed();
    lotteryPurchasePom.checklineIsDisplayed();

});

Then('the purchase modal is displayed', () => {
    lotteryPurchasePom.checkPurchaseModalIsDisplayed();
});

And('user wallet balance is displayed', () => {
    lotteryPurchasePom.checkUserBalanceIsDisplayed();
});

And('the purchase button is {string} with the {string} option', (statusOfButton, contentOfButton) => {
    lotteryPurchasePom.checkPurchaseButton(statusOfButton, contentOfButton);
});

Given('a user who has enough money on wallet to buy a lottery ticket', () => {
    lotteryPurchasePom.mockUserBalance(50);
    lotteryPurchasePom.mockSuccessfulLotteryPurchase();
});

When('the user clicks on the purchase button', () => {
    lotteryPurchasePom.clickPurchaseButton();
});

Then('lottery ticket is purchased', () => {
    lotteryPurchasePom.checkLotteryTicketWasPurchased();
});

And('the ticket has {int} randomly generated lines', (numberOfLines) => {
    lotteryPurchasePom.checkNumberOfLotteryLines(numberOfLines);
    lotteryPurchasePom.saveGeneratedLines();
});
And('the ticket has {int} randomly generated lines in lottery details', (numberOfLines) => {
    lotteryPurchasePom.checkNumberOfLotteryLinesdetails(numberOfLines);
    lotteryPurchasePom.saveGeneratedLinesdetails();
});

And('payment method section is not displayed', () => {
    lotteryPurchasePom.checkPaymentSectionIsNotDisplayed();
});

And('there is a login link', () => {
    lotteryPurchasePom.checkLoginLinkIsDisplayed();
});

When('the user clicks on the register button', () => {
    lotteryPurchasePom.clickRegisterButton();
});

When('the user clicks on the login link', () => {
    lotteryPurchasePom.clickLoginLink();
});

Then('the purchase modal for the lottery the user was purchasing is shown again', () => {
    lotteryPurchasePom.checkPurchaseModalIsDisplayed();
});

Then('the same number of lines with the same numbers are displayed', () => {
    lotteryPurchasePom.checkLotteryTicketIsTheSame();
});

And('the user has registered previously a credit or debit card', () => {
    lotteryPurchasePom.mockPaymentMethods();
    lotteryPurchasePom.mockUserWithCards();
    lotteryPurchasePom.mockSucessfulDepositWithCard();
});

And('last active card is displayed and selected on payment options', () => {
    lotteryPurchasePom.checkPaymentSectionWithCardIsDisplayed();
});

When('the user enters the correct CVC code', () => {
    lotteryPurchasePom.enterCardCVC(correctCardCVC);
});

Given('Use mocked APIs', () => {
    mockUtils.startServer();
});
