import { When, Given, Then } from "cypress-cucumber-preprocessor/steps";
import landingPom from "../pom/landingPom.js";
import casinoLobbyPom from "../pom/casinoLobbyPom.js";
import { getUrl, URLS } from "../utils/urls";

When('the user is using a desktop device', () => {
    // It is not necessary to implement, just to clarify
});

When('the user is using a mobile device', () => {
    // let's run tests dedicated to device type instead of `commonUtils.useMobileDevice();`
});

Given('User navigates to landing page', () => {
    landingPom.goToLandingPage();
});

Given('User navigates to Casino lobby site', () => {
    casinoLobbyPom.goToMainLobby();
});

Given('User navigates to Scratchcard site on desktop', () => {
    cy.authvisit(getUrl(URLS.CASINO.SCRATCHARDS));
});

Given('User navigates to Scratchcard site on mobile', () => {
    cy.authvisit(getUrl(URLS.CASINO.SCRATCHARDS_MOBILE));
});

Given('User navigates to sign-up site', () => {
    cy.authvisit(getUrl(URLS.REGISTRATION));
});

Given('User navigates to Megamillions lottery details page', () => {
    cy.authvisit(getUrl(URLS.LOTTERIES.MEGAMILLIONS));
});

Then('User is on main page', () => {
    cy.url().should('eq', getUrl(URLS.MAIN));
});

Then('User is on Scratchcards page on mobile', () => {
    cy.url().should('eq', getUrl(URLS.CASINO.SCRATCHARDS_MOBILE));
});

Then('User is on live casino page on mobile', () => {
    cy.url().should('eq', getUrl(URLS.CASINO.LIVE_CASINO_MOBILE));
});

Then('User is on Casino page', () => {
    cy.url().should('eq', getUrl(URLS.CASINO.MAIN));
});
