import { When, Then } from "cypress-cucumber-preprocessor/steps";
import newsPom from "../pom/newsPom.js";
Then('news section is displayed', () => {
    newsPom.checkIsnewsPage();
});
Then('there is {string} news displayed', (num) => {
    newsPom.checkMoreNewsWereLoaded(num);
});
Then('each new contains image', () => {
    newsPom.checkEachImageNews();
});
Then('each new contains description', () => {
    newsPom.checkEachDescNews();
});
Then('there is a load more button at the bottom of the page', () => {
    newsPom.checkLoadMoreNewsButton();
});
When('the user clicks on the load more button', () => {
    newsPom.clickLoadMoreNewsButton();
});
When('the user clicks the image of a new', () => {
    newsPom.clickNews();
});

