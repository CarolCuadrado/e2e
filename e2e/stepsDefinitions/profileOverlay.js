import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import profileOverlayPom from "../pom/profileOverlayPom.js";

Then('the profile overlay menu is open', () => {
    profileOverlayPom.isProfileOverlayMenuOpen();
});

Then('the wallet is open', () => {
    profileOverlayPom.isWalletOpen();
});

And('the profile overlay menu contains all the correct links', () => {
    profileOverlayPom.checkProfileOverlayElements();
});

When('the user clicks {string} on the profile overlay menu', (menuOption) => {
    profileOverlayPom.clickOption(menuOption);
});

Then('the user is logged out', () => {
     profileOverlayPom.isUserLoggedOut();
});

Then('Notification about unread messages {string} displayed in profile overlay', (status) => {
    if (status === 'is') {
        profileOverlayPom.isMessageNotificationVisible();
    } else if (status === 'is not') {
        profileOverlayPom.isMessageNotificationNotPresent();
    } else {
        throw new Error('Invalid status');
    }
});
