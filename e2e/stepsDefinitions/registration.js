import { When, Then } from "cypress-cucumber-preprocessor/steps";
import registrationPom from "../pom/registrationPom.js";
import { getUrl, URLS } from "../utils/urls";
// import {
//     SignUpStep1,
//     SignUpStep2,
// } from '@megalotto/magic-account/dist/index.com';
// import { PasswordIndicator } from '@megalotto/magic-ui-kit/dist/index.com';
import {
    getUserData,
    getRandomLettersString,
    getRandomNumberString,
    getRandomSymbolString,
} from '../util/datagenerator';
import commonUtils from "../utils/commonUtils.js";

const data = getUserData();

When('the user clicks on registration link', () => {
    registrationPom.openRegistrationWindow();
});

Then('the registration window appears', () => {
    registrationPom.checkRegistationWindowIsDisplayed();
});

Then('a user who just completed the registration flow', () => {
    registrationPom.registerRandomUser();
});

When('Password input field of sign-up is empty', () => {
    SignUpStep1.password().should('be.value', '');
});

Then('Password requirements are displayed', () => {
    SignUpStep1.passwordRequirementsContainer().should('be.visible');
    PasswordIndicator.items().should('have.length', 4);
});

When(
    'User types {string} lowercase letters in password input field of sign-up',
    (amount) => {
        SignUpStep1.password().type(getRandomLettersString(amount),{ force: true });
    },
);

When(
    'User types {string} uppercase letters in password input field of sign-up',
    (amount) => {
        SignUpStep1.password().type(getRandomLettersString(amount).toUpperCase(),{ force: true });
    },
);

When(
    'User types {string} digits in password input field of sign-up',
    (amount) => {
        SignUpStep1.password().type(getRandomNumberString(amount),{ force: true });
    },
);

When(
    'User types {string} symbols in password input field of sign-up',
    (amount) => {
        SignUpStep1.password().type(getRandomSymbolString(amount),{ force: true });
    },
);

Then('{string} password requirement is {string}', (requirement, status) => {
    const requirements = [
        'Characters long',
        'Uppercase',
        'Lowercase',
        'DigitOrSymbol',
        
    ];
    let assertion = '';
    const pos = requirements.indexOf(requirement);

    if (pos === -1) {
        throw new Error('not valid password requirement');
    }

    if (status === 'successful') {
        assertion = 'have.class';
    } else if (status === 'inactive') {
        assertion = 'not.have.class';
    } else {
        throw new Error('not valid status');
    }

    PasswordIndicator.items()
        .eq(pos)
        .should(assertion, 'item--active');
});

When('Complete step 1 of sign-up with valid data', () => {
    registrationPom.completeStep1({
        email: data.email,
        password: data.password,
    });
});
When('Complete step 1 of sign-up with invalid data', () => {
    registrationPom.completeStep1({
        email:data.emailexisting,
        password: data.password,
    });
});

When('Entered e-mail in step 1 is validated by backend', () => {
    SignUpStep1.email().should('have.attr', 'has-success', 'true');
});

When('Submit step 1 of sign-up', () => {
    SignUpStep1.submit().click({ force: true });
});

When('Complete step 2 of sign-up with valid data', () => {
    registrationPom.completeStep2({
        firstName: data.firstName,
        lastName: data.lastName,
        gender: 'Mr',
        birthday: { day: data.day, month: data.month, year: data.year },
        address: data.address,
        city: data.town,
        country: data.country,
        postcode: data.zipcode,
        phonePrefix: '+47',
        phoneNumber: data.phone,
        agreeAllTerms: true,
        agreeAllMarketing: false,
    });
});

When('Complete step 2 of sign-up with invalid data', () => {
    registrationPom.completeStep2({
        firstName: data.firstName,
        lastName: data.lastName,
        gender: 'Mr',
        birthday: { day: data.day, month: data.month, year: data.year },
        address: data.address,
        city: data.town,
        country: data.country,
        postcode: data.zipcode,
        phonePrefix: '+47',
        phoneNumber: '90474747',
        agreeAllTerms: true,
        agreeAllMarketing: false,
    });
});

When('Complete step 2 of sign-up with invalid data in Prefix and mobile', () => {
    registrationPom.completeStep2({
        firstName: data.firstName,
        lastName: data.lastName,
        gender: 'Mr',
        birthday: { day: data.day, month: data.month, year: data.year },
        address: data.address,
        city: data.town,
        country: data.country,
        postcode: data.zipcode,
        phonePrefix: '+356',
        phoneNumber: '5665',
        agreeAllTerms: true,
        agreeAllMarketing: false,
    });
});

When('Submit step 2 of sign-up', () => {
    SignUpStep2.submit().click({ force: true });
});

Then('User is on step {string} of sign-up', (stepNumber) => {
    registrationPom.form().should('have.attr', 'current-step', stepNumber);
});

Then('Sign-up progress bar is displayed', () => {
    registrationPom.progressBar().should('be.visible');
});

Then('{string} is highlighted on sign-up progress bar', (step) => {
    const stepsStatus = {
        step1: 'not.have.class',
        step2: 'not.have.class',
        step3: 'not.have.class',
    };

    if (step === 'Step 1') {
        stepsStatus.step1 = 'have.class';
    } else if (step === 'Step 2') {
        stepsStatus.step2 = 'have.class';
    } else if (step === 'Verification') {
        stepsStatus.step3 = 'have.class';
    } else {
        throw new Error('not valid step');
    }
    registrationPom.step1().should(stepsStatus.step1, 'is-active');
    registrationPom.step2().should(stepsStatus.step2, 'is-active');
    registrationPom.step3().should(stepsStatus.step3, 'is-active');
});

// TODO: this step definition to be moved to navigation.js once selector is correctly coming from Modal
When('User clicks on close button of modal', () => {
    registrationPom.closeButton().click();
});

// TODO: this step definition to be moved to navigation.js once selector is correctly coming from Modal
Then('Close button of modal is not displayed', () => {
    registrationPom.closeButton().should('not.exist');
});

When('Message {string} displayed on sign-up modal', (status) => {
    if (status === 'is') {
        registrationPom.message().should('be.visible');
    } else if (status === 'is not') {
        registrationPom.message().should('not.exist');
    } else {
        throw new Error('not valid status');
    }
});

When('Image {string} displayed on sign-up modal', (status) => {
    if (status === 'is') {
        registrationPom.cover().should('be.visible');
    } else if (status === 'is not') {
        registrationPom.cover().should('not.exist');
    } else {
        throw new Error('not valid status');
    }
});

When('Close confirmation is displayed', () => {
    registrationPom.confirmationContainer().should('be.visible');
});

Then('A message is displayed in close confirmation modal', () => {
    registrationPom.confirmationQuestion().should('be.visible');
    registrationPom.confirmationInfo().should('be.visible');
});

Then('Continue button is displayed in close confirmation modal', () => {
    registrationPom.confirmationContinueButtonContainer().should('be.visible');
});

Then('Cancel button is displayed in close confirmation modal', () => {
    registrationPom.confirmationCancelButtonContainer().should('be.visible');
});

Then('User clicks on Continue button in close confirmation modal', () => {
    registrationPom.confirmationContinueButton().click({ force: true });
});
Then('error notification is displayed', () => {
    registrationPom.notificationError().should('be.visible');
});

Then('User clicks on Cancel button in close confirmation modal', () => {
    registrationPom.confirmationCancelButton().click({ force: true });
});

Then('user navigates to landing offer page', () => {
    cy.visit(getUrl(URLS.LANDPAGE));
});
Then('an error message is displayed', () => {
    commonUtils.checkMessage();
});