import { Then } from 'cypress-cucumber-preprocessor/steps';
import { getUrl, URLS } from "../utils/urls";
import {
    questionnaireResults,
    questionnaireQuestions,
    questionnaireAnswers,
} from '../fixtures/responsibleGamingQuestionnaire';
import rgquestionnairePom from '../pom/rgquestionnairePom';

Given('User navigates to Responsible Gaming site', () => {
    cy.visit(getUrl(URLS.QUESTIONNAIRE));
});

Given('User navigates to Responsible Gaming questionnaire', () => {
    cy.visit(getUrl(URLS.QUESTIONNAIRE));
});

// TODO: this step definition to be moved to navigation.js once selector is correctly coming from Modal
Then('Close button in the modal is displayed', () => {
    rgquestionnairePom.closeButtonQuestionnaire();
});

Then('Responsible gaming questionnarie modal is displayed', () => {
   // RGQuestionnaire.container().should('be.visible');
    rgquestionnairePom.CheckisQuestionnaire();
});
Then('Question title is displayed in Responsible Gaming questionnaire', () => {
    rgquestionnairePom.CheckisTitleQuestionnaire();
});
Then('Question text is displayed in Responsible Gaming questionnaire', () => {
    rgquestionnairePom.CheckisTextQuestionnaire();
});
Then('Set of answers are displayed in Responsible Gaming questionnaire', () => {
    RGQuestionnaire.answers().should(
        'have.length',
        questionnaireAnswers.length,
    );
    questionnaireAnswers.forEach((answer, pos) => {
        RGQuestionnaire.answers()
            .eq(pos)
            .should(($elem) => {
                expect($elem.text()).to.include(answer);
            });
    });
});
Then('User answers all the questions with {string}', (answer) => {
    const amountOfQuestions = questionnaireQuestions.length;
    for (let i = 1; i <= amountOfQuestions; i++) {
        RGQuestionnaire.title().should(
            'have.text',
            `Question ${i} / ${amountOfQuestions}`,
        );
        RGQuestionnaire.text().should(
            'have.text',
            questionnaireQuestions[i - 1],
        );
        RGQuestionnaire.answers()
            .contains(answer)
            .click({force:true});
    }
});
Then('Result title is displayed in Responsible Gaming questionnaire', () => {
    const resultTItle = 'Your test result';
    RGQuestionnaire.resultTitle().should('have.text', resultTItle);
});
Then(
    'Responsible gaming result for {string} level is displayed',
    (levelValue) => {
        RGQuestionnaire.resultText().should(
            'have.text',
            questionnaireResults.find((element) => element.level === levelValue)
                .text,
        );
    },
);
Then('Info is displayed in Responsible Gaming questionnaire', () => {
    rgquestionnairePom.CheckisResultTextQuestionnaire();
  
});