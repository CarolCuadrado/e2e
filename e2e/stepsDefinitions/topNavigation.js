import { When, Then } from "cypress-cucumber-preprocessor/steps";
import topNavigationPom from "../pom/topNavigationPom.js";

When('the user clicks {string} on top navigation menu', (menuOption) => {
    topNavigationPom.clickOption(menuOption);
});

When('the user opens the hamburger menu', () => {
    topNavigationPom.hamburgerMenu();
});

Then('the top navigation menu is present', () => {
    topNavigationPom.isTopNavigationMenuPresent();
});

Then('the top navigation contains login button', () => {
    topNavigationPom.isloginPresent();
});
Then('the top navigation contains deposit button', () => {
    topNavigationPom.isdepositPresent();
});
Then('the top navigation menu contains all the correct links', () => {
    topNavigationPom.checkTopNavigationElements();
});

When('the user opens the login window on {string} from top navigation', (kindOfDevice) => {
    if (kindOfDevice === 'mobile') {
        topNavigationPom.loginOnMobile();
    } else if (kindOfDevice === 'desktop') {
        topNavigationPom.loginOnDesktop();
    } else {
        throw new Error('Invalid device');
    }
});

When('the user opens the registration window on {string} from top navigation', (kindOfDevice) => {
    if (kindOfDevice === 'mobile') {
        topNavigationPom.signupOnMobile();
    } else if (kindOfDevice === 'desktop') {
        topNavigationPom.signupOnDesktop();
    } else {
        throw new Error('Invalid device');
    }
});

Then('Notification about unread messages {string} displayed in top navigation', (status) => {
    if (status === 'is') {
        topNavigationPom.isMessageNotificationVisible();
    } else if (status === 'is not') {
        topNavigationPom.isMessageNotificationNotPresent();
    } else {
        throw new Error('Invalid status');
    }
});

When('the user clicks on profile overlay icon', () => {
    topNavigationPom.openProfileOverlayMenu();
});
When('the user clicks in login button', () => {
    topNavigationPom.loginModal();
});

When('it should open a modal to sign-in', () => {
    topNavigationPom.isloginModalPresent();
});


