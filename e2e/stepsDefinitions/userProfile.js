import { When, And, Then } from "cypress-cucumber-preprocessor/steps";
import userProfilePom from "../pom/userProfilePom.js";
import profileOverlayPom from "../pom/profileOverlayPom.js";
import commonUtils from "../utils/commonUtils.js";
import mockUtils from "../utils/mockUtils.js";
import loginPom from "../pom/loginPom.js";
import topNavigationPom from "../pom/topNavigationPom.js";

Then('user profile page is displayed', () => {
    userProfilePom.checkProfilePageIsDisplayed();
});

And('the user profile contains the {string}', (profileOption) => {
    userProfilePom.checkUserProfileSectionIsDisplayed(profileOption, true);
});

And('the user profile does not contains the {string}', (profileOption) => {
    userProfilePom.checkUserProfileSectionIsDisplayed(profileOption, false);
});

And('user information is collapsed', () => {
    userProfilePom.checkUserInformationIsCollapsed();
});

When('the user clicks on the arrow to expand the details', () => {
    userProfilePom.expandUserDetails();
});

When('the user accesses the profile page', () => {
    topNavigationPom.openProfileOverlayMenu();
    profileOverlayPom.clickOption('profile');
});

Then('the lucky number field is empty', () => {
    userProfilePom.checkValueLuckyNumber(null);
});

When('the user clicks on edit profile', () => {
    userProfilePom.clickEditDetails();
});

When('the user clicks on edit lucky number', () => {
    userProfilePom.clickEditLuckyNumber();
});

Then('the page to lucky number is displayed', () => {
    userProfilePom.checkEditLuckyNumberPageIsDisplayed();
});

When('the user enters {string} as lucky number', (luckyNumber) => {
    userProfilePom.clearLuckyNumberInput();
    userProfilePom.setLuckyNumber(luckyNumber);
});

When('the user clicks on Wish me luck button', () => {
    userProfilePom.clickSaveLuckyNumberButton();
});

Then('the user is redirected back to account details page', () => {
    userProfilePom.checkEditProfilePageIsDisplayed();
});

And('the new lucky number {string} is set', (savedLuckyNumber) => {
    userProfilePom.checkLuckyNumberWasProperlySaved(savedLuckyNumber);
});

When('the user leaves empty the lucky number field', () => {
    userProfilePom.clearLuckyNumberInput();
});

Then('edit user profile page is displayed', () => {
    userProfilePom.checkEditProfilePageIsDisplayed();
});

And('the user is logged out from the platform', () => {
    //Doing manually the logout as the call to the API was mocked 
    //and we are not receiving any event from the websocket to logout the user.
    const currentUserSessionId = commonUtils.getGlobalProperty('currentUserSessionId');
    loginPom.logOutUser(currentUserSessionId);
    profileOverlayPom.isUserLoggedOut(currentUserSessionId);
});

When('the user clicks on the edit password button', () => {
    userProfilePom.clickEditPasswordButton();
});

Then('edit password page is displayed', () => {
    userProfilePom.checkEditPasswordPageIsDisplayed();
});

When('the user enters the {string} password {string}', (field, password) => {
    const passwordField = field.includes('repeated') ? 'repeated' : field;
    userProfilePom.enterPassword(passwordField, password);
});

And('the user clicks on save password button', () => {
    mockUtils.startServer();
    userProfilePom.mockPasswordChange();
    userProfilePom.mockIsUserLoggedIn();
    userProfilePom.clickSavePasswordButton();
});

Then('the password is properly changed', () => {
    //It is not necessary to implement as the call to the API was mocked, just to clarify
});

Then('the save password button is not enabled', () => {
    userProfilePom.checkSavePasswordButtonIsDisabled();
});

And('an error message {string} is shown', (errorMessage) => {
    userProfilePom.checkErrorMessageIsDisplayed(errorMessage);
});