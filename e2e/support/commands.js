import commonUtils from "../utils/commonUtils.js";
 import { getCypressAdaptor } from '/Users/carolina/new_pro/e2e/magic-test-helpers/dist/cypress/adaptor';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const globalProperties = require('../config.js');
const httpAuthObject = {
    username: globalProperties.httpUsername,
    password: globalProperties.httpPassword
};

// Perform login by API.
Cypress.Commands.add('loginRequest', (usernameOrEmail, password) => {
    cy.request({
        method: 'POST',
        url: `${globalProperties.magicAPIURL}${globalProperties.magicUsersAPIURL}${globalProperties.loginAPIURL}`,
        headers: {
            'x-api-key': globalProperties.xApiKey,
        },
        body: {
            usernameOrEmail,
            password,
        },
    }).then((resp) => {
        const sessionId = resp.body.res.sessionId;
        commonUtils.setGlobalProperty('sid', sessionId);
        commonUtils.setGlobalProperty('currentUserSessionId', sessionId);
        commonUtils.setGlobalProperty('user', JSON.stringify(resp.body.res));
    });
});

Cypress.Commands.add('authvisit', (URL) => {
    cy.visit(URL, {
        auth: httpAuthObject
    });
});

// Remove favourite from all games.
Cypress.Commands.add('removeAllFavourites', () => {
    const removeAllFavourites = async (window) => {
        const faves = await window.magic.methods.games.getFavourites();
        return Promise.all(
            faves.map(({ gameId }) => {
                return window.magic.methods.games.removeFavouriteGame(gameId);
            }),
        );
    };
    return cy.window().then((window) => {
        return removeAllFavourites(window);
    });
});

// Check current url is the same as provided by locale and url paramethers. "/" at the end is optional.
Cypress.Commands.add('checkUrl', (locale, url) => {
    const regex = new RegExp(`${getUrl(locale, url)}(|/)`);
    cy.url().should('match', regex);
});
Cypress.Commands.add('clickForce', (element) => {
   

    getCypressAdaptor().getElement(element).click({force:true});
});

Cypress.Commands.add('typeForce', (element,param) => {
   
    getCypressAdaptor().getElement(element).type(param,{force:true});
});

Cypress.Commands.add('registerUser', (payload) => {
    cy.request({
        method: 'POST',
        url: `${globalProperties.magicAPIURL}${globalProperties.magicUsersAPIURL}${globalProperties.signUpAPIURL}`,
        headers: {
            'x-api-key': globalProperties.xApiKey,
        },
        body: payload
    }).then((resp) => {
        expect(resp.status).to.eq(200);
        commonUtils.setGlobalProperty('registeredUser', resp.body.res.email);
    });

});

Cypress.Commands.add('addRoute', (url, method, statusCode, response, optionalURL = globalProperties.magicUsersAPIURL, rootURL = globalProperties.magicAPIURL) => {
    const routeObject = {
        method,
        status: statusCode,
        url: `${rootURL}${optionalURL}${url}`,
    };

    cy.route(response
        ? { ...routeObject, response }
        : routeObject);
});
