import './commands';
// import { registerShadowQuery } from '@megalotto/magic-test-helpers/dist/cypress/commands/shadow-query';
import './fetchToXhr';

// registerShadowQuery();

beforeEach(function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    cy.window().then((win) => {
        win.sessionStorage.clear();
    });
    cy.setCookie('cookie-policy-accepted', 'true');
});
