import faker from 'faker';

export function getUserData() {
    return {
        email: faker.internet.email().toLowerCase() + '.wa',
        emailexisting: 'carolina@megalotto.com',
        password: 'Megalotto2019!',
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        day: faker.random.number({ min: 1, max: 28 }),
        month: faker.random.number({ min: 1, max: 12 }),
        year: faker.random.number({ min: 1950, max: 1999 }),
        address: faker.address.streetAddress(),
        town: faker.address.city(),
        country: faker.random.arrayElement(['Malta', 'Norway', 'Germany']),
        zipcode: faker.address.zipCode(),
        currency: faker.random.arrayElement([
            'EUR - Euro',
            'NOK - Norwegian krone',
        ]),
        phone: faker.random.number(100000000, 999999999),
        lvPersonalId: '32' + faker.random.number(1000000000, 9999999999),
    };
}

export function getRandomLettersString(length) {
    return getRandomString('abcdefghijklmnopqrstuvwxyz', length);
}

export function getRandomNumberString(length) {
    return getRandomString('0123456789', length);
}

export function getRandomSymbolString(length) {
    return getRandomString('~{[]}=)(<?!@$%^&#+_>|-.,', length);
}

function getRandomString(characters, length) {
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength),
        );
    }
    return result;
}
