function pad(num) {
    const norm = Math.abs(Math.floor(num));
    return (norm < 10 ? '0' : '') + norm;
}

export function getPastDateISO(days) {
    const date = new Date();

    date.setDate(date.getDate() - days);

    return (
        date.getFullYear() +
        '-' +
        pad(date.getMonth() + 1) +
        '-' +
        pad(date.getDate())
    );
}

export function getPastDateDmy(days) {
    const date = new Date();

    date.setDate(date.getDate() - days);

    return (
        pad(date.getDate()) +
        '/' +
        pad(date.getMonth() + 1) +
        '/' +
        date.getFullYear()
    );
}

export function getPastDate(days) {
    const date = new Date();

    date.setDate(date.getDate() - days);

    return {
        day: pad(date.getDate()),
        month: pad(date.getMonth() + 1),
        year: pad(date.getFullYear()),
    };
}
