const fetch = require('node-fetch');
const LOCATION = {
    NZ: {
        LAT: -41.3,
        LONG: 174.8,
    },
};

function fakeLocation(latitude, longitude) {
    return {
        onBeforeLoad(win) {
            cy.stub(
                win.navigator.geolocation,
                'getCurrentPosition',
                (cb, err) => {
                    if (latitude && longitude) {
                        return cb({ coords: { latitude, longitude } });
                    }
                    throw err({ code: 1 }); // 1: rejected, 2: unable, 3: timeout
                },
            );
        },
    };
}

export { fakeLocation, LOCATION };
