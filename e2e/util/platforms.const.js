module.exports = {
    DESKTOP: {
        NAME: 'desktop',
        VIEWPORT_WIDTH: 1280,
        VIEWPORT_LENGHT: 900,
        USER_AGENT: null,
    },
    MOBILE: {
        NAME: 'mobile',
        VIEWPORT_WIDTH: 375,
        VIEWPORT_LENGHT: 812,
        USER_AGENT:
            'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1',
    },
};
