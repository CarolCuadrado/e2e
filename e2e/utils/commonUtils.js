// import { getCypressAdaptor } from '@megalotto/magic-test-helpers/dist/cypress/adaptor';

const userPrefix = "apitest";
const emailSuffix = "@gig.com";
const notificationComponent = "magic-notifications";
const messageText = 'magic-ui-text';
const cookiesPopUp = 'div.notification--message button';
const commonUtils = {
    waitUntilLoadingScreenDisappears: (() => {
        cy.get('.splash').should('not.be.visible');
    }),

    setGlobalProperty: ((propertyName, propertyValue) => {
        localStorage.setItem(propertyName, propertyValue);
    }),

    getGlobalProperty: ((propertyName) => {
        return localStorage.getItem(propertyName);
    }),

    checkUserWasProperlyRedirected: ((redirectedPage) => {
        cy.url().should('include', redirectedPage);
    }),

    getRandomUserData: (() => {
        const epoch = Math.round(Date.now() / 1000);
        const userDataObject = {
            email: `${userPrefix}${epoch}${emailSuffix}`,
            username: `${epoch}`,
            firstname: `${userPrefix}${epoch}`,
            surname: `${userPrefix}${epoch}`,
            phone: `${epoch}`,
            mobile: `${epoch}`
        };
        return userDataObject;
    }),

    checkNotification: ((message) => {
        cy.get(notificationComponent).shadow().find('div.notifications>magic-ui-notification').shadow().find('p').should('have.text', message);
    }),
    checkMessage: (() => {
        cy.get(messageText).should('be.exist');
    }),

    removeElement: ((element) => {
        cy.get(element).then((el) => {
            el.remove();
        });
    }),

    acceptCookies: (() => {
        getCypressAdaptor().getElement(cookiesPopUp).click();
    })
};

export default commonUtils;
