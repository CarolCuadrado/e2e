const mockUtils = {
    startServer: (() => {
        cy.server();
    })
};

export default mockUtils;