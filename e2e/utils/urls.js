const getUrl = (path) =>
    `${Cypress.config('baseUrl')}${path}`;

const URLS = {
    MAIN: 'en/',
    CASINO: {
        MAIN: 'en/casino',
        SCRATCHARDS: 'en/casino/scratchcards',
        SCRATCHARDS_MOBILE: 'en/casino/scratchcards-mobile',
        INSTANT_WINS: 'en/casino/instant-wins',
        LIVE_CASINO_MOBILE: 'en/casino/live-casino-mobile',
    },
    LOTTERIES: {
        MAIN: 'en/lotteries',
        MEGAMILLIONS: 'en/lotteries/megamillions',
    },
    REGISTRATION: 'en/register',
    QUESTIONNAIRE: 'en/rg-questionnaire',
    GERMAN : 'de-de/',
    CANADA : 'en-ca/',
    NORWAY : 'nb-no/',
    NEWZELAND : 'en-nz/',
    FINLAND : 'fi-fi/',
    LANDPAGE: 'en/promo/welcome-offer-games-bak',
};

export { getUrl, URLS };
